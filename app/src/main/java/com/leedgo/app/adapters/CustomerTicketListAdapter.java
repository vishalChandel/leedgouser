package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgo.app.R;
import com.leedgo.app.activities.ChatSuppotActivity;
import com.leedgo.app.models.ServicesDataModel;
import com.leedgo.app.models.TicketListModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerTicketListAdapter extends RecyclerView.Adapter<CustomerTicketListAdapter.ViewHolder> {
    private ArrayList<TicketListModel.Data> mArrayList;
    private Activity mActivity;
    String formattedDate;
    public CustomerTicketListAdapter(Activity mActivity, ArrayList<TicketListModel.Data> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public CustomerTicketListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ticket_list, parent, false);
        return new CustomerTicketListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerTicketListAdapter.ViewHolder holder, int position) {
        TicketListModel.Data model=mArrayList.get(position);
        holder.textMain.setText("Ticket issued regarding the task submitted for "+model.getSub_category_name()+" - "+model.getCategory_name());

        long coment_time = Long.parseLong(model.getCreation_time());
        timeChange(coment_time);
        holder.textDate.setText(formattedDate);
        if (Integer.parseInt(model.getTicket_status())==0)
        {
            holder.textStatus.setText("Status: Pending");
            holder.textStatus.setTextColor(ContextCompat.getColor(mActivity,R.color.colorOrange));

        }
        else if (Integer.parseInt(model.getTicket_status())==1)
        {
            holder.textStatus.setText("Status: Open");
            holder.textStatus.setTextColor(ContextCompat.getColor(mActivity,R.color.colorGreenLight));

        }
        else  if (Integer.parseInt(model.getTicket_status())==2)
        {
            holder.textStatus.setText("Status: Closed");
            holder.textStatus.setTextColor(ContextCompat.getColor(mActivity,R.color.colorBlack));

        }
        else {
            holder.textStatus.setText("Status: Closed");
            holder.textStatus.setTextColor(ContextCompat.getColor(mActivity,R.color.colorBlack));

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mActivity, ChatSuppotActivity.class);
                intent.putExtra("ticket_no",model.getTicket_no());
                intent.putExtra("serviceName",model.getCategory_name());
                intent.putExtra("serviceNameType",model.getSub_category_name());
                intent.putExtra("serviceImage",model.getIcon_img());
                mActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textMain)
        TextView textMain;
        @BindView(R.id.textDate)
        TextView textDate;
        @BindView(R.id.textStatus)
        TextView textStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);
        }
    }

    void timeChange(long coment_time)
    {
        Date date = new java.util.Date(coment_time * 1000L);

     // the format of your date
        SimpleDateFormat sdf2 = new java.text.SimpleDateFormat("hh:mm a ");
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("EE dd MMM yyyy");
     // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());
        sdf2.setTimeZone(TimeZone.getDefault());
String    formattedDate0=sdf2.format(date) ;
        formattedDate = sdf.format(date)+" at "+formattedDate0;
    }

}
