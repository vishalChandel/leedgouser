package com.leedgo.app.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgo.app.R;
import com.leedgo.app.models.PendingTaskModel;
import com.leedgo.app.models.RequestTaskModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestedHistoryAdapter2 extends RecyclerView.Adapter<RequestedHistoryAdapter2.ViewHolder> {
    private ArrayList<PendingTaskModel.Datum> mArrayList;
    private Activity mActivity;


    public RequestedHistoryAdapter2(Activity activity, ArrayList<PendingTaskModel.Datum> data) {
        this.mActivity = activity;
        this.mArrayList = data;


    }

    @NonNull
    @Override
    public RequestedHistoryAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request_history1, parent, false);
        return new RequestedHistoryAdapter2.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestedHistoryAdapter2.ViewHolder holder, int position) {
        final PendingTaskModel.Datum mModel = mArrayList.get(position);
        holder.txOrderName.setText(mModel.getServiceName()+" Service");
        holder.imCompletePending.setImageResource(R.drawable.ic_grey_clock);

        RequestHistorySubAdapter mAdapter = new RequestHistorySubAdapter(mActivity, mArrayList.get(position));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        holder.recyclerView.setLayoutManager(mLayoutManager);
        holder.recyclerView.setAdapter(mAdapter);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.recyclerView.getVisibility() == View.VISIBLE) {
                    holder.imageShoworNot.setImageResource(R.drawable.ic_arrow_right);
                    holder.recyclerView.setVisibility(View.GONE);
                } else {
                    holder.imageShoworNot.setImageResource(R.drawable.ic_arrow_bottom);
                    holder.recyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recyclerSubTask)
        RecyclerView recyclerView;
        @BindView(R.id.txOrderName)
        TextView txOrderName;
        @BindView(R.id.txOrderRating)
        RatingBar ratingBar;
        @BindView(R.id.imageCompletedorPending)
        ImageView imCompletePending;
        @BindView(R.id.imageOrdershownot)
        ImageView imageShoworNot;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
