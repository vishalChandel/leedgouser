package com.leedgo.app.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.leedgo.app.fragments.IntroScreen1Fragment;
import com.leedgo.app.fragments.IntroScreen2Fragment;
import com.leedgo.app.fragments.IntroScreen3Fragment;

public class SimpleFragmentPagerAdapter extends FragmentStateAdapter {


    public SimpleFragmentPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position)
        {
            case 0:
                return new IntroScreen1Fragment(); //ChildFragment1 at position 0
            case 1:
                return new IntroScreen2Fragment(); //ChildFragment2 at position 1
            case 2:
                return new IntroScreen3Fragment(); //ChildFragment3 at position 2
        }
        return null; //does not happen
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
