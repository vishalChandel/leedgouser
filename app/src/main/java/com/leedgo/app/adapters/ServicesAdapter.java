package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.leedgo.app.R;
import com.leedgo.app.activities.SeeAllActivity;
import com.leedgo.app.activities.SeeSingleService;
import com.leedgo.app.activities.ServiceRepair;
import com.leedgo.app.models.ServicesDataModel;
import com.leedgo.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     *
     */
    private ArrayList<ServicesDataModel> mArrayList;
    private Activity mActivity;

    public ServicesAdapter(Activity mActivity, ArrayList<ServicesDataModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_services, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ServicesDataModel mModel = mArrayList.get(position);
        holder.txtItemNameTV.setText(mModel.getName());

        if (!mModel.getImage().equals("")) {
            Glide.with(mActivity)
                    .load(mModel.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(holder.imgItemIV);
        } else {

            SharedPreferences preference=mActivity.getSharedPreferences("LeedGo", Context.MODE_PRIVATE);

            Set<String> set=preference.getStringSet("myKey",null);
            List<String> sample=new ArrayList<String>(set);
            Glide.with(mActivity)
                        .load(sample.get(new Random().nextInt(sample.size())))
                        .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                        .into(holder.imgItemIV);

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("Checkla",mModel.getSubCat().toString());
                Intent intent=new Intent(mActivity, SeeSingleService.class);
                intent.putExtra("serviceSingle","yes");
              //  intent.putExtra("serviceName",mModel.getName())
                intent.putExtra("serviceType",mModel.getName());
                intent.putExtra("serviceImage",mModel.getImage());
                intent.putExtra(Constants.LIST,mModel.getSubCat());
                mActivity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return mArrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgItemIV;
        public TextView txtItemNameTV;

        public ViewHolder(View itemView) {
            super(itemView);
            imgItemIV = itemView.findViewById(R.id.imgItemIV);
            txtItemNameTV = itemView.findViewById(R.id.txtItemNameTV);
        }
    }

}
