package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.leedgo.app.R;
import com.leedgo.app.activities.CustomerSupportActivity;
import com.leedgo.app.models.RequestTaskModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TicketListAdapter extends RecyclerView.Adapter<TicketListAdapter.ViewHolder> {
    private ArrayList<RequestTaskModel.Data> mArrayList;
    private Activity mActivity;

    public TicketListAdapter(Activity mActivity, ArrayList<RequestTaskModel.Data> data) {
        this.mActivity=mActivity;
        this.mArrayList=data;

    }

    @NonNull
    @Override
    public TicketListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_list_item, parent, false);
        return new TicketListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketListAdapter.ViewHolder holder, int position) {
        final RequestTaskModel.Data mModel = mArrayList.get(position);
        if (mModel.getCategory_image() != null) {
            Glide.with(mActivity)
                    .load(mModel.getCategory_image())
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(holder.imServiceImage);
        } else {
            holder.imServiceImage.setImageResource(R.drawable.ic_pp);
        }
        Glide.with(mActivity).load(mModel.getCategory_image()).into(holder.imServiceImage);
        holder.txServiceName.setText(mModel.getSub_category_name()+"("+mModel.getService_name()+")");
        holder.txServiceWorkDate.setText("Work Date "+mModel.getWork_date());

    ;

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mActivity, CustomerSupportActivity.class);
                intent.putExtra("servicename",mModel.getSub_category_name()+"("+mModel.getService_name()+")");
                intent.putExtra("task_id",mModel.getTask_id());

                mActivity.startActivity(intent);
                mActivity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.serviceImage)
        ImageView imServiceImage;
        @BindView(R.id.txServiceName)
        TextView txServiceName;
        @BindView(R.id.txServiceWorkDate)
        TextView txServiceWorkDate;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }



}
