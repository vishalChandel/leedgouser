package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.StringChange.StringFormatter;
import com.leedgo.app.activities.ChatActivity;
import com.leedgo.app.models.ChatModel;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatFAdapter extends RecyclerView.Adapter<ChatFAdapter.ViewHolder> implements Filterable {

    Activity mContext;
    ArrayList<ChatModel.Data> mArrayList = new ArrayList<>();
    ArrayList<ChatModel.Data> FilteredmArrayList = new ArrayList<>();
    TextView text_no_result;

    public ChatFAdapter(Activity context, ArrayList<ChatModel.Data> mArrayList, TextView text_no_result) {
        this.mContext = context;
        this.mArrayList = mArrayList;
        this.FilteredmArrayList = mArrayList;
        this.text_no_result = text_no_result;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_message_item, parent, false);
        return new ChatFAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ChatModel.Data mModel = mArrayList.get(position);
        Glide.with(mContext)
                .load(mModel.getProfile_image())
                .placeholder(mContext.getResources().getDrawable(R.drawable.ic_person))
                .into(holder.photo);
        String serverResponse = mModel.getLatest_message();
        if (serverResponse != null && !serverResponse.equals("")) {
            try {
                String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(serverResponse);
                holder.tx_lastmessage.setText(fromServerUnicodeDecoded);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            holder.tx_lastmessage.setText(serverResponse);
        }

        if (mModel.getName() != null) {
            holder.tx_username.setText(StringFormatter.capitalizeWord(mModel.getName()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("room_id", mModel.getRoom_id());
                intent.putExtra("name", mModel.getName());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mArrayList = FilteredmArrayList;

                } else {
                    ArrayList<ChatModel.Data> filteredList = new ArrayList<>();
                    for (ChatModel.Data row : FilteredmArrayList) {


                        //change this to filter according to your case
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mArrayList = (ArrayList<ChatModel.Data>) filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mArrayList = (ArrayList) filterResults.values;

                if (mArrayList.size() > 0) {
                    text_no_result.setVisibility(View.GONE);
                } else {
                    text_no_result.setVisibility(View.VISIBLE);
                    text_no_result.setText("No Results found for " + "'" + charSequence.toString() + "'");
                }
                notifyDataSetChanged();
            }
        };
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView photo;
        TextView tx_username, tx_lastmessage, tx_msgcount;
        LinearLayout lly_goto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.chatf_photo);
            tx_username = itemView.findViewById(R.id.chatf_username);
            tx_lastmessage = itemView.findViewById(R.id.chatf_usermessage);
            lly_goto = itemView.findViewById(R.id.chatf_goto_button);
            tx_msgcount = itemView.findViewById(R.id.message_count);
        }
    }
}
