package com.leedgo.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.leedgo.app.R;
import com.leedgo.app.models.AllMessageModel;
import com.leedgo.app.models.AllSupportMessageModel;
import com.leedgo.app.utils.LeedgoPrefrences;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostChatSupportAdapter extends RecyclerView.Adapter<PostChatSupportAdapter.ViewHolder>{
    private Context context;
    ArrayList<AllSupportMessageModel> mChatgroupList;
    private LayoutInflater inflater;
    Date date_format;

    String formattedDate;
    public PostChatSupportAdapter(Context context, ArrayList<AllSupportMessageModel> mChatgroupModel){
        this.context = context;
        this.mChatgroupList = mChatgroupModel;

    }

    @NonNull
    @Override
    public PostChatSupportAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_chat_list_new, parent, false);
        return new PostChatSupportAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostChatSupportAdapter.ViewHolder holder, int position) {
        AllSupportMessageModel mModel = mChatgroupList.get(position);
try {
    if (mModel.getRole()!=null&&mModel.getRole().equals("admin")) {
        holder.lly_leftcomplete.setVisibility(View.VISIBLE);
        holder.lly_rightcomplete.setVisibility(View.GONE);
        holder.llyViewLeft.setVisibility(View.VISIBLE);
        holder.llyViewRight.setVisibility(View.GONE);

        long unix_seconds = Long.parseLong(mModel.getCreation_time());
//convert seconds to milliseconds

        Date date = new Date(unix_seconds * 1000L);

        DateFormat originalFormat = new SimpleDateFormat("hh:mm aaa");

        String formattedDate2 = originalFormat.format(date);
        String str = formattedDate2.replace("am", "AM").replace("pm","PM");
        holder.tx_leftTime.setText(str);
        String serverResponse =mModel.getMessage();

        String fromServerUnicodeDecoded = decodeEmoji(serverResponse);
        holder.tx_leftmessage.setText(fromServerUnicodeDecoded);

        RequestOptions optionsIcon = new RequestOptions()
                .placeholder(R.drawable.ic_person)
                .error(R.drawable.ic_person)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        if (mModel.getIcon_img() != null) {
            Glide.with(context)
                    .load(R.drawable.admin)
                    .apply(optionsIcon)
                    .into(holder.leftProfileImage);

        }
        RequestOptions optionsIcon2 = new RequestOptions()
                .placeholder(R.drawable.admin)
                .error(R.drawable.admin)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();


        holder.tx_leftmessage.setText(mModel.getMessage());





    }
    else {
        holder.lly_rightcomplete.setVisibility(View.VISIBLE);
        holder.lly_leftcomplete.setVisibility(View.GONE);
        holder.llyViewRight.setVisibility(View.VISIBLE);
        holder.llyViewLeft.setVisibility(View.GONE);
        long unix_seconds = Long.parseLong(mModel.getCreation_time());
//convert seconds to milliseconds

        Date date = new Date(unix_seconds * 1000L);

        DateFormat originalFormat = new SimpleDateFormat("hh:mm aaa");

        String formattedDate2 = originalFormat.format(date);
        String str = formattedDate2.replace("am", "AM").replace("pm","PM");
        holder.tx_rightTime.setText(str);
        String serverResponse =mModel.getMessage();

        String fromServerUnicodeDecoded = decodeEmoji(serverResponse);
        holder.tx_rightmessage.setText(fromServerUnicodeDecoded);

        RequestOptions optionsIcon = new RequestOptions()
                .placeholder(R.drawable.ic_person)
                .error(R.drawable.ic_person)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .dontAnimate()
                .dontTransform();
        String image= LeedgoPrefrences.readString(context,LeedgoPrefrences.USER_PROFILE_PIC,null);

        ;

        holder.tx_rightmessage.setText(mModel.getMessage());



    }

}
catch (Exception e)
{
    e.printStackTrace();
}


    }

    @Override
    public int getItemCount() {
        return mChatgroupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView leftImage,rightImage;
        CircleImageView leftProfileImage,rightprofileImage;

        TextView tx_rightTime,tx_leftTime;
        TextView tx_rightmessage, tx_leftmessage;
        LinearLayout lly_leftmessage,lly_rightmessage;
        LinearLayout lly_leftcomplete,lly_rightcomplete;
        LinearLayout llyViewLeft, llyViewRight;

        public ViewHolder(@NonNull View convertView) {
            super(convertView);
            leftImage=convertView.findViewById(R.id.leftImage);
            rightImage=convertView.findViewById(R.id.rightImage);
            leftProfileImage=convertView.findViewById(R.id.chati_left_photo);
            rightprofileImage=convertView.findViewById(R.id.chati_right_photo);
            tx_leftTime=convertView.findViewById(R.id.chati_left_time);
            tx_rightTime=convertView.findViewById(R.id.chati_right_time);
            tx_leftmessage=convertView.findViewById(R.id.chat_left_msg_text_view);
            tx_rightmessage=convertView.findViewById(R.id.chat_right_msg_text_view);
            lly_rightmessage=convertView.findViewById(R.id.chat_right_msg_layout);
            lly_leftmessage=convertView.findViewById(R.id.chat_left_msg_layout);
            lly_leftcomplete=convertView.findViewById(R.id.leftlayoutcomplete);
            lly_rightcomplete=convertView.findViewById(R.id.rightlayoutcomplete);
            llyViewLeft=convertView.findViewById(R.id.viewLeft);
            llyViewRight=convertView.findViewById(R.id.viewRight);


        }
    }
    private void timeChange(String message_time) throws ParseException {
        long unix_seconds = Long.parseLong(message_time);

//convert seconds to milliseconds

        Date date = new Date(unix_seconds * 1000L);
        Date date1=new SimpleDateFormat("dd-MM-yyyy HH:mm aaa").parse(message_time);
        DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm aaa");

        DateFormat targetFormat = new SimpleDateFormat("h:mm aaa");

          formattedDate = originalFormat.format(date);
      //  formattedDate=message_time;
    }

    public static String encodeEmoji (String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }


    public static String decodeEmoji (String message) {
        String myString= null;
        try {
            return URLDecoder.decode(
                    message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }

}
