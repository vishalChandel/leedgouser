package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.activities.ChatActivity;
import com.leedgo.app.models.CompleteTaskModel;
import com.leedgo.app.models.RequestTaskModel;
import com.leedgo.app.models.RoomCreateModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.LeedgoPrefrences;
import com.leedgo.app.utils.StringFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestHistorySubAdapter2 extends RecyclerView.Adapter<RequestHistorySubAdapter2.ViewHolder> {

    private CompleteTaskModel.Datum mArrayList;
    private final Activity mActivity;
    String clientID = "";
    String providerID = "";
    String providerName = "";
    String userId;

    public RequestHistorySubAdapter2(Activity mActivity, CompleteTaskModel.Datum mArrayList, String userId) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.userId=userId;
    }

    @NonNull
    @Override
    public RequestHistorySubAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request_history_complete, parent, false);
        return new RequestHistorySubAdapter2.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestHistorySubAdapter2.ViewHolder holder, int position) {
        final CompleteTaskModel.Datum mModel = mArrayList;
        holder.llyProfileView.setVisibility(View.GONE);

        holder.txViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if ((mModel.getProviderDetail().getPhone().length() > 3)) {
                        if (holder.llyProfileView.getVisibility() == View.VISIBLE) {
                            holder.llyProfileView.setVisibility(View.GONE);
                        } else {
                            holder.llyProfileView.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        // holder.amountTV.setText(mModel.getProvider_detail().get());
        Glide.with(mActivity).load(mModel.getProviderDetail().getProfileImage()).placeholder(R.drawable.ic_person).into(holder.imProviderImage);
        holder.txProviderName.setText(StringFormatter.capitalizeWord(mModel.getProviderDetail().getProviderName()));
        holder.txProviderProffessional.setText("Professional since " + mModel.getProviderDetail().getWorkExperince());
        holder.amountTV.setText("US$ " + mModel.getAssignedAmount() + ".00");

        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(Long.parseLong(mModel.getDateOfCompletion()) * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date currenTimeZone = (Date) calendar.getTime();
            holder.txDateTV.setText("on " + sdf.format(currenTimeZone));


        } catch (Exception e) {
        }

        holder.txtMessageTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                providerID = mModel.getProviderDetail().getProviderId();
                providerName = mModel.getProviderDetail().getProviderName();
                executeMessage();
            }
        });
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", LeedgoPrefrences.readString(mActivity, LeedgoPrefrences.USER_ID, ""));
        mMap.put("provider_id", providerID);
        Log.e("Room", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeMessage() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createRoom(mParam()).enqueue(new Callback<RoomCreateModel>() {
            @Override
            public void onResponse(Call<RoomCreateModel> call, Response<RoomCreateModel> response) {

                RoomCreateModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Intent intent = new Intent(mActivity, ChatActivity.class);
                    intent.putExtra("room_id", mModel.getRoom_id());
                    intent.putExtra("name", providerName);
                    mActivity.startActivity(intent);

                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RoomCreateModel> call, Throwable t) {

                Log.e(String.valueOf(mActivity), "**ERROR**" + t.getMessage());
            }
        });
    }

    private void showToast(Activity mActivity, String message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {

        return 1;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txTaskasiignedornot)
        TextView txTaskassignedorNot;
        @BindView(R.id.txViewProfile)
        TextView txViewProfile;
        @BindView(R.id.amountTV)
        TextView amountTV;
        @BindView(R.id.imProviderImage)
        ImageView imProviderImage;
        @BindView(R.id.txOrderRating)
        RatingBar ratingBar;
        @BindView(R.id.txProviderName)
        TextView txProviderName;
        @BindView(R.id.txProviderProffessional)
        TextView txProviderProffessional;
        @BindView(R.id.txtMessageTV)
        TextView txtMessageTV;
        @BindView(R.id.llyProfile)
        LinearLayout llyProfileView;
        @BindView(R.id.txDateTV)
        TextView txDateTV;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
