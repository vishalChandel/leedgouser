package com.leedgo.app.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgo.app.R;
import com.leedgo.app.interfaces.NotificationClickInterface;
import com.leedgo.app.models.NotificationModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Notificationadapter extends RecyclerView.Adapter<Notificationadapter.ViewHolder> {
    Activity mActivity;
    List<NotificationModel.Data> mArrayList2 = new ArrayList<>();
    String convTime;
    NotificationClickInterface mNotificationClickInterface;

    public Notificationadapter(Activity mActivity, List<NotificationModel.Data> mArrayList2,
                               NotificationClickInterface mNotificationClickInterface) {
        this.mActivity = mActivity;
        this.mArrayList2 = mArrayList2;
        this.mNotificationClickInterface = mNotificationClickInterface;
    }

    @NonNull
    @Override
    public Notificationadapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        return new Notificationadapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Notificationadapter.ViewHolder holder, int position) {
        final NotificationModel.Data mModel = mArrayList2.get(position);

        long coment_time = Long.parseLong(mModel.getCreation_date_timestamp());
        Date date = new java.util.Date(coment_time * 1000L);

        // the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");

        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());
        String formattedDate = sdf.format(date);
        covertTimeToText(formattedDate);
        holder.tx_time.setText(convTime);
        holder.tx_message.setText(mModel.getMessage());

        if (!mModel.getNotification_type().toLowerCase().equals("chat")) {
            holder.tx_heading.setText(mModel.getTitle());
        } else {
            holder.tx_heading.setText("A message");
            // holder.tx_heading.setText(StringFormatter.capitalizeWord(mModel.getNotification_type()));
        }

        if (mModel.getNotification_type() != null) {
            switch (mModel.getNotification_type().toLowerCase()) {
                case "new_bid_reminder":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.orangeNotificationColor));
                    break;
                case "request_submitted":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.orangeNotificationColor));
                    break;
                case "received_bid":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.orangeNotificationColor));
                    break;
                case "bid_approved":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.yellowNotificationColor));
                    break;
                case "task_date_scheduled":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.yellowNotificationColor));
                    break;
                case "task_complete":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.greenNotificationColor));
                    break;
                case "chat":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.blueNotificationColor));
                    break;
                case "unassigned_task":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.orangeNotificationColor));
                    break;
                case "pending_bid_status":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.orangeNotificationColor));
                    break;
                case "request_start_day":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.yellowNotificationColor));
                    break;
                case "order":
                    holder.linearLayout.setBackgroundTintList(ContextCompat.getColorStateList(mActivity, R.color.redNotificationColor));
                    break;
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNotificationClickInterface.getNotification(mModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList2.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_heading, tx_message, tx_time;
        LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tx_heading = itemView.findViewById(R.id.notification_heading);
            tx_message = itemView.findViewById(R.id.notification_message);
            tx_time = itemView.findViewById(R.id.notification_time);
            linearLayout = itemView.findViewById(R.id.llyC);
        }
    }

    public void covertTimeToText(String dataDate) {
        String prefix = "a";
        String suffix = "ago";

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date pasTime = dateFormat.parse(dataDate);

            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day = TimeUnit.MILLISECONDS.toDays(dateDiff);
            if (second < 2) {
                convTime = "now";
            } else if (second < 60) {
                convTime = second + "s " + suffix;
            } else if (minute < 60) {
                convTime = minute + "mins " + suffix;
            } else if (hour < 24) {
                convTime = hour + "h " + suffix;
            } else if (day >= 7) {
                if (day > 360) {
                    convTime = (day / 360) + " Year " + suffix;
                } else if (day > 30) {
                    convTime = (day / 30) + " Month " + suffix;
                } else {
                    convTime = (day / 7) + " Week " + suffix;
                }
            } else if (day < 7 && day > 1) {
                convTime = day + " Days " + suffix;
            } else if (day == 1) {
                convTime = "1 day " + suffix;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }


    }
}
