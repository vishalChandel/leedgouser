package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.leedgo.app.R;
import com.leedgo.app.models.ServicesDataModel;
import com.leedgo.app.models.ServicesSubCatData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;


public class ServicesSingleAdapter extends RecyclerView.Adapter<ServicesSingleAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<ServicesSubCatData> mArrayList;
    ArrayList<ServicesDataModel> mServicesArrayList;
    private Activity mActivity;
    String serviceType;
    String serviceImage;

    public ServicesSingleAdapter(Activity mActivity, ArrayList<ServicesDataModel> mServicesArrayList, ArrayList<ServicesSubCatData> mArrayList, String serviceType,String serviceImage) {
        this.mActivity = mActivity;
        this.mServicesArrayList=mServicesArrayList;
        this.mArrayList = mArrayList;
        this.serviceType=serviceType;
        this.serviceImage=serviceImage;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_services, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ServicesDataModel mModel = mServicesArrayList.get(position);

        holder.txtItemNameTV.setText(serviceType);

        if (!serviceImage.equals("")) {
            Glide.with(mActivity)
                    .load(serviceImage)
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(holder.imgItemIV);
        } else {
            // holder.imgItemIV.setImageResource(R.drawable.ic_pp);
            SharedPreferences preference = mActivity.getSharedPreferences("LeedGo", Context.MODE_PRIVATE);

            Set<String> set = preference.getStringSet("myKey", null);
            List<String> sample = new ArrayList<String>(set);
            Glide.with(mActivity)
                    .load(sample.get(new Random().nextInt(sample.size())))
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(holder.imgItemIV);

        }
        holder.mRecyclerViewRV.setVisibility(View.VISIBLE);
        holder.imgDropDownIV.setImageResource(R.drawable.ic_arrow_bottom);


        //  if (serviceType!=null&&serviceType.equals(mModel.getName())) {

            //Set SubCategories
            ServicesSubCateAdapter mSearchLocationAdapter = new ServicesSubCateAdapter(mActivity, mArrayList,mServicesArrayList, serviceType,mModel.getScatId());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
            holder.mRecyclerViewRV.setLayoutManager(mLayoutManager);
            holder.mRecyclerViewRV.setAdapter(mSearchLocationAdapter);


            holder.rootLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.mRecyclerViewRV.getVisibility() == View.VISIBLE) {
                        holder.imgDropDownIV.setImageResource(R.drawable.ic_arrow_right);
                        holder.mRecyclerViewRV.setVisibility(View.GONE);
                    } else {
                        holder.imgDropDownIV.setImageResource(R.drawable.ic_arrow_bottom);
                        holder.mRecyclerViewRV.setVisibility(View.VISIBLE);
                    }
                }
            });


    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return 1;
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgItemIV,imgDropDownIV;
        public TextView txtItemNameTV;
        public RecyclerView mRecyclerViewRV;
        public LinearLayout rootLL,lly;

        public ViewHolder(View itemView) {
            super(itemView);
            imgItemIV = itemView.findViewById(R.id.imgItemIV);
            txtItemNameTV = itemView.findViewById(R.id.txtItemNameTV);
            imgDropDownIV = itemView.findViewById(R.id.imgDropDownIV);
            mRecyclerViewRV = itemView.findViewById(R.id.mRecyclerViewRV);
            rootLL = itemView.findViewById(R.id.rootLL);
            lly = itemView.findViewById(R.id.lly);
        }
    }
    public void swapeItem(int fromPosition,int toPosition){
        Collections.swap(mArrayList, fromPosition, toPosition);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyItemMoved(fromPosition, toPosition);
            }},1000);


    }
}
