package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.activities.ChatActivity;
import com.leedgo.app.models.PendingTaskModel;
import com.leedgo.app.models.RequestTaskModel;
import com.leedgo.app.models.RoomCreateModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.LeedgoPrefrences;
import com.leedgo.app.utils.StringFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestHistorySubAdapter extends RecyclerView.Adapter<RequestHistorySubAdapter.ViewHolder> {

    private PendingTaskModel.Datum mArrayList;
    private Activity mActivity;
    String clientID = "";
    String providerID = "";
    String providerName = "";

    public RequestHistorySubAdapter(Activity mActivity, PendingTaskModel.Datum mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public RequestHistorySubAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request_history2, parent, false);
        return new RequestHistorySubAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestHistorySubAdapter.ViewHolder holder, int position) {
        final PendingTaskModel.Datum mModel = mArrayList;
        holder.llyProfileView.setVisibility(View.GONE);
        try {
//            holder.txTaskassignedorNot.setText("Your task was " + mModel.getOrder_status().toLowerCase()+"!");
//            if (holder.txTaskassignedorNot.getText().toString().trim().equals("Your task was assigned")) {
//                holder.txtMessageTV.setVisibility(View.VISIBLE);
//            } else {
//                holder.txtMessageTV.setVisibility(View.GONE);
//            }
           holder.txTaskassignedorNot.setText("Your task was " + "assigned"+"!");
            holder.txtMessageTV.setVisibility(View.VISIBLE);
            holder.txViewProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if ((mModel.getProviderDetail().getPhone().length() > 3)) {
                            if (holder.llyProfileView.getVisibility() == View.VISIBLE) {
                                holder.llyProfileView.setVisibility(View.GONE);
                            } else {

                                holder.llyProfileView.setVisibility(View.VISIBLE);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            holder.txProviderPhoneNumeber.setText(mModel.getProviderDetail().getPhone());
            Glide.with(mActivity).load(mModel.getProviderDetail().getProfileImage()).placeholder(R.drawable.ic_person).into(holder.imProviderImage);
            holder.txProviderName.setText(StringFormatter.capitalizeWord(mModel.getProviderDetail().getProviderName()));

            holder.txProviderProffessional.setText("Professional since " + mModel.getProviderDetail().getWorkExperince());


            holder.txtMessageTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //clientID = mModel.getUser_id();
                    providerID = mModel.getProviderDetail().getProviderId();
                    providerName = mModel.getProviderDetail().getProviderName();
                    executeMessage();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id",LeedgoPrefrences.readString(mActivity, LeedgoPrefrences.USER_ID, ""));
        mMap.put("provider_id", providerID);
        Log.e("Room", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeMessage() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createRoom(mParam()).enqueue(new Callback<RoomCreateModel>() {
            @Override
            public void onResponse(Call<RoomCreateModel> call, Response<RoomCreateModel> response) {

                RoomCreateModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Intent intent = new Intent(mActivity, ChatActivity.class);
                    intent.putExtra("room_id", mModel.getRoom_id());
                    intent.putExtra("name", providerName);
                    mActivity.startActivity(intent);

                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RoomCreateModel> call, Throwable t) {

                Log.e(String.valueOf(mActivity), "**ERROR**" + t.getMessage());
            }
        });
    }

    private void showToast(Activity mActivity, String message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {

        return 1;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txTaskasiignedornot)
        TextView txTaskassignedorNot;
        @BindView(R.id.txViewProfile)
        TextView txViewProfile;
        @BindView(R.id.txProviderPhonenumber)
        TextView txProviderPhoneNumeber;
        @BindView(R.id.imProviderImage)
        ImageView imProviderImage;
        @BindView(R.id.txOrderRating)
        RatingBar ratingBar;
        @BindView(R.id.txProviderName)
        TextView txProviderName;
        @BindView(R.id.txProviderProffessional)
        TextView txProviderProffessional;
        @BindView(R.id.txtMessageTV)
        TextView txtMessageTV;
        @BindView(R.id.llyProfile)
        LinearLayout llyProfileView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
