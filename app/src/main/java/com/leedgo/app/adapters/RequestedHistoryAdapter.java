package com.leedgo.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgo.app.R;
import com.leedgo.app.interfaces.PrimaryLocationInterface;
import com.leedgo.app.models.CompleteTaskModel;
import com.leedgo.app.models.GetClientLocationData;
import com.leedgo.app.models.RequestTaskModel;
import com.leedgo.app.models.ServicesDataModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestedHistoryAdapter extends RecyclerView.Adapter<RequestedHistoryAdapter.ViewHolder> {
    private List<CompleteTaskModel.Datum> mArrayList;
    private Activity mActivity;
    private String userId;

    public RequestedHistoryAdapter(FragmentActivity activity, ArrayList<CompleteTaskModel.Datum> mArrayList2, String userId) {
        this.mActivity = activity;
        this.mArrayList = mArrayList2;
        this.userId=userId;
    }


//    public RequestedHistoryAdapter(Activity activity, List<RequestTaskModel.Data> data) {
//        this.mActivity = activity;
//        this.mArrayList = data;
//
//
//    }

    @NonNull
    @Override
    public RequestedHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request_history1, parent, false);
        return new RequestedHistoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestedHistoryAdapter.ViewHolder holder, int position) {
        final CompleteTaskModel.Datum mModel = mArrayList.get(position);
        try {
            holder.txOrderName.setText(mModel.getServiceName()+" Service");

//            if (mModel.getOrder_status().equals("Pending") || mModel.getOrder_status().equals("Not Assigned") || mModel.getOrder_status().equals("Cancelled")) {
//                holder.imCompletePending.setImageResource(R.drawable.clock_gray);
//            } else {
//                //("Complete")
//                holder.imCompletePending.setImageResource(R.drawable.green_check);
//            }
            holder.imCompletePending.setImageResource(R.drawable.ic_green_pending);
            //Set SubCategories
            RequestHistorySubAdapter2 mAdapter = new RequestHistorySubAdapter2(mActivity, mArrayList.get(position),userId);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
            holder.recyclerView.setLayoutManager(mLayoutManager);
            holder.recyclerView.setAdapter(mAdapter);
            holder.recyclerView.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   if (mModel.getOrder_status().equals("Complete"))
                    //    {
                    if (holder.recyclerView.getVisibility() == View.VISIBLE) {
                        holder.imageShoworNot.setImageResource(R.drawable.ic_arrow_right);
                        holder.recyclerView.setVisibility(View.GONE);
                    } else {
                        holder.imageShoworNot.setImageResource(R.drawable.ic_arrow_bottom);
                        holder.recyclerView.setVisibility(View.VISIBLE);
                    }
                    //   }


                }
            });


//        if (mModel.getOrder_status().equals("Pending")||mModel.getOrder_status().equals("Not Assigned")||mModel.getOrder_status().equals("Cancelled")) {
//        holder.lly.setVisibility(View.GONE);
//        //mArrayList.remove(position);
//
//        }
//        else
//        {
//            holder.lly.setVisibility(View.VISIBLE);
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recyclerSubTask)
        RecyclerView recyclerView;
        @BindView(R.id.txOrderName)
        TextView txOrderName;
        @BindView(R.id.txOrderRating)
        RatingBar ratingBar;
        @BindView(R.id.imageCompletedorPending)
        ImageView imCompletePending;
        @BindView(R.id.imageOrdershownot)
        ImageView imageShoworNot;
        @BindView(R.id.llyk11)
        LinearLayout lly;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
