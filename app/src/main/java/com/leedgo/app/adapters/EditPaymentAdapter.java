package com.leedgo.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.interfaces.PrimaryCardInterface;
import com.leedgo.app.models.CardData;
import java.util.ArrayList;

public class EditPaymentAdapter extends RecyclerView.Adapter<EditPaymentAdapter.ViewHolder> {
    private ArrayList<CardData> mArrayList;
    private Activity mActivity;
    private int lastCheckedPosition = 0;

    PrimaryCardInterface mPrimaryLocationInterface;
    EditPaymentAdapter.OnItemClickListener itemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int positon, CardData item, View view);
    }

    public EditPaymentAdapter(Activity mActivity, ArrayList<CardData> mArrayList, PrimaryCardInterface mPrimaryLocationInterface, EditPaymentAdapter.OnItemClickListener onItemClickListener) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mPrimaryLocationInterface = mPrimaryLocationInterface;
        this.itemClickListener = onItemClickListener;
    }

    @Override
    public EditPaymentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_list, parent, false);
        return new EditPaymentAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final EditPaymentAdapter.ViewHolder holder, final int position) {
        final CardData mModel = mArrayList.get(position);

        //Set Locations Name & Address
        holder.itemCityNameTV.setText("ending with **** " + mModel.getCard_last4());
        try {
            if (mModel.getBrand().toLowerCase().equals("visa")) {
                Glide.with(mActivity).load(R.drawable.ic_visa).into(holder.imgItem);
            } else {
                Glide.with(mActivity).load(R.drawable.ic_mastercard).into(holder.imgItem);
            }
        } catch (Exception e) {

        }

        //Bind Up data
        holder.bind(mModel, position, holder, itemClickListener);


        //Set Primary Locations
        if (Integer.parseInt(mModel.getIsPrimary()) == 1) {
            holder.radioButtonRB.setChecked(true);
        }


    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return mArrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RadioButton radioButtonRB;
        public TextView itemCityNameTV, itemAddressTV;
        public ImageView imgItem;
        public View mView;
        ImageView im_delete, im_edit;

        public ViewHolder(View itemView) {
            super(itemView);
            radioButtonRB = itemView.findViewById(R.id.radioButtonRB);
            itemCityNameTV = itemView.findViewById(R.id.itemCityNameTV);
            itemAddressTV = itemView.findViewById(R.id.itemAddressTV);
            imgItem = itemView.findViewById(R.id.im);
            mView = itemView.findViewById(R.id.mView);
            im_delete = itemView.findViewById(R.id.imgItemDeleteIV);
            im_edit = itemView.findViewById(R.id.imgItemEditIV);
        }


        public void bind(CardData mModel, int position, EditPaymentAdapter.ViewHolder holder, EditPaymentAdapter.OnItemClickListener itemClickListener) {
            //Selection Status
            if (lastCheckedPosition == position) {
                mModel.setIsPrimary("1");
                radioButtonRB.setChecked(true);
            } else {
                mModel.setIsPrimary("0");
                radioButtonRB.setChecked(false);
            }


            radioButtonRB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastCheckedPosition = position;
                    notifyDataSetChanged();
                    mPrimaryLocationInterface.getPrimaryCard(mModel);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(position, mModel, v);
                }
            });

            im_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(position, mModel, v);
                }
            });
            im_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(position, mModel, v);
                }
            });
        }
    }
}
