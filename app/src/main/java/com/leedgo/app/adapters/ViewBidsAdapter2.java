package com.leedgo.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgo.app.R;
import com.leedgo.app.interfaces.BidsInteface;
import com.leedgo.app.interfaces.LocationInterface;
import com.leedgo.app.models.BidListModel;
import com.leedgo.app.models.locations.PredictionsItem;
import com.leedgo.app.utils.StringFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewBidsAdapter2  extends RecyclerView.Adapter<ViewBidsAdapter2.ViewHolder>  {
    private int lastCheckedPosition = -1;
    BidsInteface mInterface;
    String finalDate="";
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<BidListModel.Data> mArrayList;
    private Activity mActivity;

    public ViewBidsAdapter2(Activity mActivity, ArrayList<BidListModel.Data> mArrayList, BidsInteface mInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mInterface = mInterface;
    }



    @NonNull
    @Override
    public ViewBidsAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.views_bids_item, parent, false);
        return new ViewBidsAdapter2.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewBidsAdapter2.ViewHolder holder, int position) {
        final BidListModel.Data mModel = mArrayList.get(position);
        holder.linearLayout.setVisibility(View.VISIBLE);
        holder.txBidsPersonName.setText(StringFormatter.capitalizeWord(mModel.getProvider_name()));
        holder.txBidsPrice.setText("$"+mModel.getAmount());
        holder.txBidsDateTime.setText(mModel.getTime());
        timeChange(Long.parseLong(mModel.getTime()));
        holder.txBidsDateTime.setText(finalDate);
       // holder.bind(mModel);
        //Bind Up data
        holder.bind(mModel,position,holder);

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txBidsPersonName)
        TextView txBidsPersonName;
        @BindView(R.id.txBidsDateTime)
        TextView txBidsDateTime;
        @BindView(R.id.txBidsPrice)
        TextView txBidsPrice;
        @BindView(R.id.imCheck)
        ImageView checkBox;
        @BindView(R.id.llyBidCheckBox)
        LinearLayout linearLayout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bind(BidListModel.Data mModel, int position, ViewHolder holder) {

           if(lastCheckedPosition ==position){

            checkBox.setImageResource(R.drawable.cb_checked);
           }
           else {
               checkBox.setImageResource(R.drawable.cb_uncheck);
           }
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    lastCheckedPosition = getAdapterPosition();

                    //checkBox.setChecked(true);
                  if (lastCheckedPosition == position){
                      checkBox.setImageResource(R.drawable.cb_uncheck);
                   }else {
                       checkBox.setImageResource(R.drawable.cb_checked);
                    }
                    notifyDataSetChanged();
                    mInterface.getSelectedPosition(mModel);

                }
            });
        }
    }
    void timeChange(long coment_time)
    {
        Date date = new java.util.Date(coment_time * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat(" hh:mm a");
        sdf.setTimeZone(TimeZone.getDefault());
        SimpleDateFormat sdf2 = new java.text.SimpleDateFormat("dd-MM-yyyy ");
        sdf.setTimeZone(TimeZone.getDefault());
        String  formattedDate = sdf.format(date).toUpperCase();
        String  formattedDate2 = sdf2.format(date);
        finalDate=formattedDate2+"  |  "+formattedDate;
    }


}
