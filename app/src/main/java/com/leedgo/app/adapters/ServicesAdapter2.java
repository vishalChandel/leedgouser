package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.leedgo.app.R;
import com.leedgo.app.models.ServicesDataModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;


public class ServicesAdapter2 extends RecyclerView.Adapter<ServicesAdapter2.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<ServicesDataModel> mArrayList;
    private Activity mActivity;
    String serviceType;

    public ServicesAdapter2(Activity mActivity, ArrayList<ServicesDataModel> mArrayList,String serviceType) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.serviceType=serviceType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_services, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ServicesDataModel mModel = mArrayList.get(position);

            holder.txtItemNameTV.setText(mModel.getName());

            if (!mModel.getImage().equals("")) {
                Glide.with(mActivity)
                        .load(mModel.getImage())
                        .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                        .into(holder.imgItemIV);
            } else {
               // holder.imgItemIV.setImageResource(R.drawable.ic_pp);
                SharedPreferences preference=mActivity.getSharedPreferences("LeedGo", Context.MODE_PRIVATE);

                Set<String> set=preference.getStringSet("myKey",null);
                List<String> sample=new ArrayList<String>(set);
                Glide.with(mActivity)
                        .load(sample.get(new Random().nextInt(sample.size())))
                        .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                        .into(holder.imgItemIV);

            }

            //Set SubCategories
            ServicesSubCateAdapter mSearchLocationAdapter = new ServicesSubCateAdapter(mActivity, mModel.getSubCat(),mArrayList,mModel.getName(), mModel.getScatId());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
            holder.mRecyclerViewRV.setLayoutManager(mLayoutManager);
            holder.mRecyclerViewRV.setAdapter(mSearchLocationAdapter);

            holder.rootLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.mRecyclerViewRV.getVisibility() == View.VISIBLE) {
                        holder.imgDropDownIV.setImageResource(R.drawable.ic_arrow_right);
                        holder.mRecyclerViewRV.setVisibility(View.GONE);
                    } else {
                        holder.imgDropDownIV.setImageResource(R.drawable.ic_arrow_bottom);
                        holder.mRecyclerViewRV.setVisibility(View.VISIBLE);
                    }
                }
            });


    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return mArrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgItemIV,imgDropDownIV;
        public TextView txtItemNameTV;
        public RecyclerView mRecyclerViewRV;
        public LinearLayout rootLL;

        public ViewHolder(View itemView) {
            super(itemView);
            imgItemIV = itemView.findViewById(R.id.imgItemIV);
            txtItemNameTV = itemView.findViewById(R.id.txtItemNameTV);
            imgDropDownIV = itemView.findViewById(R.id.imgDropDownIV);
            mRecyclerViewRV = itemView.findViewById(R.id.mRecyclerViewRV);
            rootLL = itemView.findViewById(R.id.rootLL);
        }
    }

}
