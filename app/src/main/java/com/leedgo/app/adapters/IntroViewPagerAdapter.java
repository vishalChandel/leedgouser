package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.leedgo.app.R;
import com.leedgo.app.fragments.IntroScreen1Fragment;
import com.leedgo.app.fragments.IntroScreen2Fragment;
import com.leedgo.app.fragments.IntroScreen3Fragment;

import org.jetbrains.annotations.NotNull;

public class IntroViewPagerAdapter extends FragmentPagerAdapter {

    private Activity mActivity;
    boolean check=true;



    public IntroViewPagerAdapter(@NonNull @NotNull FragmentManager fm) {
        super(fm);
    }


    @NonNull
    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new IntroScreen1Fragment(); //ChildFragment1 at position 0
            case 1:
                return new IntroScreen2Fragment(); //ChildFragment2 at position 1
            case 2:
                return new IntroScreen3Fragment(); //ChildFragment3 at position 2
        }
        return null; //does not happen
    }

    @Override
    public int getCount() {
        return 3;
    }

}