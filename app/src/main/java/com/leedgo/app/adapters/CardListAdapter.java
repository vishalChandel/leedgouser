package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.StringChange.StringFormatter;
import com.leedgo.app.activities.ChatActivity;
import com.leedgo.app.models.ChatModel;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.ViewHolder> {

    ArrayList<Integer> mCardImagesAL = new ArrayList<>();
    Context context;
    int rowIndex;


    public CardListAdapter(Activity mActivity, ArrayList<Integer> mCardImagesAL) {
        this.mCardImagesAL = mCardImagesAL;
        this.context = mActivity;

    }


    @NonNull
    @Override
    public CardListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_list_item, parent, false);
        return new CardListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardListAdapter.ViewHolder holder, int position) {
        Integer image = mCardImagesAL.get(position);
        Glide.with(context)
                .load(image)
                .into(holder.imageIV);
        holder.llyVisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rowIndex = position;
                notifyDataSetChanged();
            }
        });

        if (rowIndex == position) {
            holder.llyVisa.setBackground(context.getResources().getDrawable(R.drawable.bg_edittext_green));

        } else {
            holder.llyVisa.setBackground(context.getResources().getDrawable(R.drawable.bg_edittext));

        }
    }

    @Override
    public int getItemCount() {
        return mCardImagesAL.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageIV;
        RelativeLayout llyVisa;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageIV = itemView.findViewById(R.id.imageIV);
            llyVisa = itemView.findViewById(R.id.llyVisa);
        }
    }


}
