package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.leedgo.app.R;
import com.leedgo.app.activities.TrackTaskScreen;
import com.leedgo.app.models.RequestTaskModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestedTaskAdapter extends RecyclerView.Adapter<RequestedTaskAdapter.ViewHolder> {
    private ArrayList<RequestTaskModel.Data> mArrayList;
    private Activity mActivity;

    public RequestedTaskAdapter(Activity activity, ArrayList<RequestTaskModel.Data> data) {
        this.mActivity = activity;
        this.mArrayList = data;


    }


    @NonNull
    @Override
    public RequestedTaskAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_requested_task, parent, false);
        return new RequestedTaskAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestedTaskAdapter.ViewHolder holder, int position) {
        final RequestTaskModel.Data mModel = mArrayList.get(position);
        holder.textMain.setText(mModel.getService_name() + "(" + mModel.getSub_category_name() + ")");
        holder.textDate.setText(mModel.getAddress());
        holder.textSubMain.setVisibility(View.VISIBLE);
        holder.textSubMain.setText(mModel.getAddress());
        if (mModel.getOrder_status().equals("Not Assigned")) {
            holder.textStatus.setText(mModel.getOrder_status());
            holder.textStatus.setTextColor(ContextCompat.getColor(mActivity, R.color.colorOrange));
        } else if (mModel.getOrder_status().equals("Pending")) {
            holder.textStatus.setText(mModel.getOrder_status());
            holder.textStatus.setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
        } else {
            holder.textStatus.setText(mModel.getOrder_status());
            holder.textStatus.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlack));
        }

        if (!mModel.getCategory_image().equals("")) {

            Glide.with(mActivity)
                    .load(mModel.getCategory_image())
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(holder.imageView);
        } else {
            SharedPreferences preference = mActivity.getSharedPreferences("LeedGo", Context.MODE_PRIVATE);
            Set<String> set = preference.getStringSet("myKey", null);
            List<String> sample = new ArrayList<String>(set);
            Glide.with(mActivity)
                    .load(sample.get(new Random().nextInt(sample.size())))
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(holder.imageView);
        }
        holder.textDate.setText(mModel.getProvider_detail().getAddress());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, TrackTaskScreen.class);
                intent.putExtra("orderStatus", mModel.getOrder_status());
                intent.putExtra("paymentStatus", mModel.getPayment_status());
                intent.putExtra("serviceName", mModel.getService_name());
                intent.putExtra("categoryImage", mModel.getCategory_image());
                intent.putExtra("subCategoryName", mModel.getSub_category_name());
                intent.putExtra("workLocation", mModel.getAddress());
                intent.putExtra("flexibleWeeks", mModel.getFlexible_week());
                intent.putExtra("workDescription", mModel.getWork_details());
                intent.putExtra("isHistoricalStructure", mModel.getRequested_for_historical_structure().trim().toString());
                intent.putExtra("isInsuranceClaim", mModel.getRequest_covered_by_insurance_claim().trim());
                intent.putExtra("isRepresentative", mModel.getOwner_or_authorized_representative_of_owner().trim());
                intent.putExtra("taskId", mModel.getTask_id());
                intent.putExtra("proId",mModel.getProvider_detail().getProvider_id());
                intent.putExtra("proName",mModel.getProvider_detail().getProvider_name());
                intent.putExtra("proImage",mModel.getProvider_detail().getProfile_image());
                intent.putExtra("proPhone",mModel.getProvider_detail().getPhone());
                intent.putExtra("proEmail",mModel.getProvider_detail().getBusiness_email());
                intent.putExtra("proTime",mModel.getProvider_detail().getWork_experince());
                intent.putExtra("proRating",mModel.getProvider_detail().getRating());
                mActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textMain)
        TextView textMain;
        @BindView(R.id.texySubmain)
        TextView textSubMain;
        @BindView(R.id.textDate)
        TextView textDate;
        @BindView(R.id.textStatus)
        TextView textStatus;
        @BindView(R.id.imageview)
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
