package com.leedgo.app.adapters;

import android.app.Activity;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.leedgo.app.R;
import com.leedgo.app.interfaces.PrimaryLocationInterface;
import com.leedgo.app.models.GetClientLocationData;

import java.util.ArrayList;
import java.util.Collections;


public class GetClientLocationAdapter extends RecyclerView.Adapter<GetClientLocationAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * Initialize Checked Position
     * Initialize Interface
     * */
    private ArrayList<GetClientLocationData> mArrayList;
    private Activity mActivity;
    private int lastCheckedPosition = 0;
    PrimaryLocationInterface mPrimaryLocationInterface;
    OnItemClickListener itemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int positon, GetClientLocationData item, View view);
    }

    public GetClientLocationAdapter(Activity mActivity, ArrayList<GetClientLocationData> mArrayList, PrimaryLocationInterface mPrimaryLocationInterface, OnItemClickListener onItemClickListener) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mPrimaryLocationInterface = mPrimaryLocationInterface;
        this.itemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final GetClientLocationData mModel = mArrayList.get(position);

        //Set Locations Name & Address
        holder.itemCityNameTV.setText(mModel.getLocationTitle());
        holder.itemAddressTV.setText(mModel.getAddress());

        //Bind Up data
        holder.bind(mModel, position, holder, itemClickListener);


        //Set Primary Locations
        if (mModel.getPrimaryValue() == 1) {
            holder.radioButtonRB.setChecked(true);
        }


    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return mArrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RadioButton radioButtonRB, radioButtonRB2;
        public TextView itemCityNameTV, itemAddressTV;
        public View mView;
        ImageView im_delete, im_edit;

        public ViewHolder(View itemView) {
            super(itemView);
            radioButtonRB = itemView.findViewById(R.id.radioButtonRB);
            itemCityNameTV = itemView.findViewById(R.id.itemCityNameTV);
            itemAddressTV = itemView.findViewById(R.id.itemAddressTV);
            mView = itemView.findViewById(R.id.mView);
            im_delete = itemView.findViewById(R.id.imgItemDeleteIV);
            im_edit = itemView.findViewById(R.id.imgItemEditIV);
        }


        public void bind(GetClientLocationData mModel, int position, ViewHolder holder, OnItemClickListener itemClickListener) {


            //Selection Status
            if (lastCheckedPosition == position) {
                mModel.setPrimaryValue(1);
                radioButtonRB.setChecked(true);
            } else {
                mModel.setPrimaryValue(0);
                radioButtonRB.setChecked(false);
            }


            radioButtonRB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastCheckedPosition = position;
                    notifyDataSetChanged();
                    mPrimaryLocationInterface.getPrimaryLocation(mModel, position);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(position, mModel, v);
                }
            });

            im_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(position, mModel, v);
                }
            });

            im_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    itemClickListener.onItemClick(position, mModel, v);
                }
            });
        }
    }

    public void swapItem(int fromPosition, int toPosition) {
        Collections.swap(mArrayList, fromPosition, toPosition);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyItemMoved(fromPosition, toPosition);
            }
        }, 0);
    }
}
