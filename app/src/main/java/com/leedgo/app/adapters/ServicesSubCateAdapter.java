package com.leedgo.app.adapters;

import android.app.Activity;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;


import com.leedgo.app.R;
import com.leedgo.app.StringChange.StringFormatter;
import com.leedgo.app.activities.ServiceRepair;
import com.leedgo.app.models.LocationServicesRawList;
import com.leedgo.app.models.ServicesDataModel;
import com.leedgo.app.models.ServicesSubCatData;
import com.leedgo.app.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;


public class ServicesSubCateAdapter extends RecyclerView.Adapter<ServicesSubCateAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<ServicesSubCatData> mArrayList;
    private Activity mActivity;
    ArrayList<String> ServiceList = new ArrayList<String>();
    ArrayList<ServicesDataModel> modelArrayList;
    String serviceType;
    int scatId;


    public ServicesSubCateAdapter(Activity mActivity, ArrayList<ServicesSubCatData> mArrayList, ArrayList<ServicesDataModel> mModell,String serviceType,  int scatId) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.modelArrayList=mModell;
        this.serviceType=serviceType;
        this.scatId=scatId;

    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_services, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ServicesSubCatData mModel = mArrayList.get(position);
        holder.itemSubServiceTV.setText(StringFormatter.capitalizeWord(mModel.getName()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationServicesRawList mModel1 = new LocationServicesRawList();
                ServiceList.clear();
                for (int i = 0; i < mArrayList.size(); i++) {


                    ServiceList.add(mArrayList.get(i).getName());
                    mModel1.setServiceList(ServiceList);


                }
                String item = holder.itemSubServiceTV.getText().toString().trim();
                Collections.swap(ServiceList,position,0);
               Intent mIntent = new Intent(mActivity, ServiceRepair.class);
                mIntent.putStringArrayListExtra("LIST_ITEMS", ServiceList);
                mIntent.putExtra("serviceType", serviceType);
                mIntent.putExtra(Constants.LIST,mArrayList);
                mIntent.putExtra("serviceId",String.valueOf(scatId));
                mActivity.startActivity(mIntent);
            }
        });


    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return mArrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView itemSubServiceTV;

        public ViewHolder(View itemView) {
            super(itemView);
            itemSubServiceTV = itemView.findViewById(R.id.itemSubServiceTV);

        }
    }
}
