package com.leedgo.app.retrofit;


import com.google.gson.JsonObject;
import com.leedgo.app.models.AddLocationModel;
import com.leedgo.app.models.AddTaskModel;
import com.leedgo.app.models.BidListModel;
import com.leedgo.app.models.CardDetailModel;
import com.leedgo.app.models.ChangePwdModel;
import com.leedgo.app.models.ChatModel;
import com.leedgo.app.models.CheckModel;
import com.leedgo.app.models.CompleteTaskModel;
import com.leedgo.app.models.CustomerSupportModel;
import com.leedgo.app.models.ForgotPwdModel;
import com.leedgo.app.models.GetClientLatestRequestModel;
import com.leedgo.app.models.GetClientLocationModel;
import com.leedgo.app.models.NotificationModel;
import com.leedgo.app.models.PaymentModel;
import com.leedgo.app.models.PaymentSendModel;
import com.leedgo.app.models.PendingTaskModel;
import com.leedgo.app.models.RequestTaskModel;
import com.leedgo.app.models.RoomCreateModel;
import com.leedgo.app.models.ServicesModel;
import com.leedgo.app.models.SignInModel;
import com.leedgo.app.models.SignUpModel;
import com.leedgo.app.models.StripeAccountModel;
import com.leedgo.app.models.TaskDetailsModel;
import com.leedgo.app.models.TicketListModel;
import com.leedgo.app.models.UpdatePriLocationModel;
import com.leedgo.app.models.locationlatlong.PlaceLatLongModel;
import com.leedgo.app.models.locations.DeleteLocationModel;
import com.leedgo.app.models.locations.EditLocationModel;
import com.leedgo.app.models.locations.SearchLocationModel;
import com.leedgo.app.models.profile.ProfileModel;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ApiInterface {


    @POST("Signup.php")
    Call<SignUpModel> signUpRequest(@Body Map<String, String> mParams);

    @POST("Facebook_signup.php")
    Call<SignInModel> facebookRequest(@Body Map<String, String> mParams);

    @POST("Google_signup.php")
    Call<SignInModel> signInGoogleRequest(@Body Map<String, String> mParams2);

    @POST("Login.php")
    Call<SignInModel> signInRequest(@Body Map<String, String> mParams);

    @POST("ForgetClientPassword.php")
    Call<ForgotPwdModel> forgotPasswordRequest(@Body Map<String, String> mParams);

    @GET("GetCategory.php")
    Call<ServicesModel> getServicesRequest();

    @POST("CustomerSupport.php")
    Call<CustomerSupportModel> customerSupportRequest(@Body Map<String, String> mParams);

    @POST("GetClient.php")
    Call<ProfileModel> getUserDataRequest(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);

    @POST("EditProfile.php")
    Call<ProfileModel> updateUserProfileRequest(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);

    @POST("GetClientLocation.php")
    Call<GetClientLocationModel> getUserLocationsRequest(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);

    @GET
    Call<SearchLocationModel> getSearchPlacesRequest(@Url String mUrl);

    @GET
    Call<PlaceLatLongModel> getLatLongFromPlaceId(@Url String mUrl);

    @POST("AddClientLocation.php")
    Call<AddLocationModel> addClientLocationRequest(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);

    @POST("AddPrimaeryAddress.php")
    Call<UpdatePriLocationModel> addPrimaryAddressRequest(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);

    @POST("ClientPendingtask.php")
    Call<PendingTaskModel> pendingTaskModel(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);

    @POST("ClientCompletetask.php")
    Call<CompleteTaskModel> completeTaskModel(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);


    //Pending to implement
    // param
    // id
    //client_id
    //address
    //latitude
    //longitude
    //location_title
    //state
    //zip_code
    //
    //Token   header

    @POST("EditClientLocation.php")
    Call<EditLocationModel> executeEditLocationData(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);


    //Params
    //
    // id
    //header Token
    @POST("DeleteClientLocation.php")
    Call<DeleteLocationModel> executeDeleteLocation(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);

    @POST("ClientLatestrequesttime.php")
    Call<ServicesModel> getClientRequestTime(@Header("access-token") String mAuthorization, @Body Map<String, String> mParams);

    @Multipart
    @POST("AddTask.php")
    Call<AddTaskModel> addPostDataRequest(
            @Part("client_id") RequestBody client_id,
            @Part("category_id") RequestBody category_id,
            @Part("subcategory_id") RequestBody subcategory_id,
            @Part("order_date") RequestBody order_date,
            @Part("order_lat") RequestBody order_lat,
            @Part("order_long") RequestBody order_long,
            @Part("faxble_week") RequestBody faxble_week,
            @Part("order_details") RequestBody order_details,
            @Part("address") RequestBody address,
            @Part("state") RequestBody state,
            @Part("zip") RequestBody zip,
            @Part("promocode") RequestBody promocode,
            @Part MultipartBody.Part orderimage1,
            @Part MultipartBody.Part orderimage2,
            @Part MultipartBody.Part orderimage3,
            @Part("requested_for_historical_structure") RequestBody requested_for_historical_structure,
            @Part("request_covered_by_insurance_claim") RequestBody request_covered_by_insurance_claim,
            @Part("owner_or_authorized_representative_of_owner") RequestBody owner_or_authorized_representative_of_owner
    );

    @Multipart
    @POST("Chat/AddMessage.php")
    Call<JsonObject> addUpdateChatMessage(
            @Part("room_id") RequestBody client_id,
            @Part("sender_type") RequestBody category_id,
            @Part("message") RequestBody subcategory_id,
            @Part MultipartBody.Part image
    );

    @POST("GetAllTasksOfUser.php")
    Call<RequestTaskModel> getAllTasks(@Body Map<String, String> mParam);

    @POST("Chat/RequestChat.php")
    Call<RoomCreateModel> createRoom(@Body Map<String, String> mParam);

    @POST("Chat/GetChatMembersList.php")
    Call<ChatModel> getAllChats(@Body Map<String, String> mParams);

    @POST("Chat/GetChatListing.php")
    Call<JsonObject> chatGroupListRequest(@Body Map<String, String> mParam);

    @POST("CustomerSupportList.php")
    Call<TicketListModel> getTicketList(@Body Map<String, String> mParams);

    @POST("GetSupportMessageList.php")
    Call<JsonObject> chatGroupSupport(@Body Map<String, String> mParam);

    @POST("AddSupportMessage.php")
    Call<JsonObject> chatSupportSendMessage(@Body Map<String, String> mParamt);

    @POST("AddCardForPayment.php")
    Call<CheckModel> addCard(@Header("access-token") String authKey, @Body Map<String, String> mParam);

    @POST("GetCardDetails.php")
    Call<CardDetailModel> getClientDetailsCard(@Header("access-token") String authKey, @Body Map<String, String> mParam);

    @POST("DeleteCard.php")
    Call<DeleteLocationModel> executeDeleteCard(@Header("access-token") String authKey, @Body Map<String, String> mParamDel);

    @POST("ChangeThePrimaryCard.php")
    Call<UpdatePriLocationModel> addPrimaryCard(@Header("access-token") String authKey, @Body Map<String, String> mUpdateParam);


    @POST("UpdateCard.php")
    Call<CheckModel> updateCard(@Header("access-token") String authKey, @Body Map<String, String> mParamnewUpdate);

    @POST("NotificationActivity.php")
    Call<NotificationModel> executeNotificationList(@Body Map<String, String> mParam);

    @POST("ChangePassword.php")
    Call<ChangePwdModel> changePasswordRequest(@Body Map<String, String> mParam);


    @POST("ClientLatestrequesttime.php")
    Call<GetClientLatestRequestModel> executeLatestRequest(@Header("access-token") String authKey, @Body Map<String, String> mParam);

    @POST("GetAllBidsListing.php")
    Call<BidListModel> showBidList(@Body Map<String, String> mParamnewUpdate);

    @POST("ClearAllBids.php")
    Call<CheckModel> clearBidList(@Body Map<String, String> mParamnewUpdate);

    @POST("AcceptBids.php")
    Call<CheckModel> acceptBid(@Body Map<String, String> mParams2);

    @POST("CheckOrderPaymentStatus.php")
    Call<PaymentModel> executePaymentStatus(@Body Map<String, String> mParamnewUpdate);

    @POST("EnableDisableNotification.php")
    Call<CheckModel> getNotification(@Body Map<String, String> mParam2);

    @POST("CheckProviderStripeAccountStatus.php")
    Call<StripeAccountModel> providerStripeAccount(@Body Map<String, String> mParam2);

    @POST("stripe/SendPayment.php")
    Call<PaymentSendModel> sendPayment(@Body Map<String, String> mParam2);

    @POST("GetTaskDetailByTaskId.php")
    Call<TaskDetailsModel> getRequesDetails(@Body Map<String, String> mParam);

    @POST("GetTaskPaymentStatus.php")
    Call<TaskDetailsModel> getTaskPaymentStatus(@Body Map<String, String> mParam);

    @POST("logOut.php")
    Call<JsonObject> logoutRequest(@Body Map<String, String> mParam);

    @POST("UpdateNotificationRead.php")
    Call<NotificationModel> readNotificationRequest(@Body Map<String, String> mParam2);
}
