package com.leedgo.app;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.leedgo.app.utils.Constants;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class LeedgoApplication extends Application {
    /**
     * Getting the Current Class Name
     */
    public static final String TAG = LeedgoApplication.class.getSimpleName();
    /**
     * Initialize the Applications Instance
     */
    private static LeedgoApplication mInstance;
    /*
     * Socket
     * */
    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(Constants.SocketURL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static synchronized LeedgoApplication getInstance() {
        return mInstance;
    }

    public Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        FirebaseApp.initializeApp(this);

    }
}
