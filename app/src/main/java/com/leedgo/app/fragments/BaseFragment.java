package com.leedgo.app.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.leedgo.app.R;
import com.leedgo.app.activities.BaseActivity;
import com.leedgo.app.utils.LeedgoPrefrences;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public class BaseFragment extends Fragment {

    String TAG = BaseFragment.this.getClass().getSimpleName();

    private Dialog progressDialog;


    /*
     * Get Current User is  LoggedIN or Not
     * */
    public boolean IsLogin() {
        return LeedgoPrefrences.readBoolean(getActivity(), LeedgoPrefrences.IS_LOGIN, false);
    }
    /*
     * Get Is Intro Screens
     * */
    public boolean IsIntroScreens() {
        return LeedgoPrefrences.readBoolean(getActivity(), LeedgoPrefrences.IS_INTRO_SCREENS, false);
    }
    /*
     * Get Remember Credentials
     * */
    public boolean IsRememberMe() {
        return LeedgoPrefrences.readBoolean(getActivity(), LeedgoPrefrences.REMEMBER_CREDENTIALS, false);
    }
    /*
     * Get Remember Email
     * */
    public String getRemeberEmail() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.REMEMBER_EMAIL, "");
    }
    /*
     * Get Remember Password
     * */
    public String getRemeberPassword() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.REMEMBER_PASSWORD, "");
    }
    /*
     * Get User ID
     * */
    public String getUserId() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.USER_ID, "");
    }
    /*
     * Get User AuthKey
     * */
    public String getAuthKey() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.AUTH_KEY, "");
    }
    /*
     * Get User Name
     * */
    public String getUserName() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.USER_NAME, "");
    }
    /*
     * Get User Email
     * */
    public String getUserEmail() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.USER_EMAIL, "");
    }
    /*
     * Get User ProfilePicture
     * */
    public String getUserProfilePic() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.USER_PROFILE_PIC, "");
    }
    /*
     * Get User Latitude
     * */
    public String getUserLatitude() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.LATITUDE, "");
    }
    /*
     * Get User Longitude
     * */
    public String getUserLongitude() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.LONGITUDE, "");
    }
    /*
     * Get User Locations
     * */
    public String getUserLocations() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.USER_LOCATIONS, "");
    }
    /*
     * Get User Locations Latitude
     * */
    public String getCurrentLatitude() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.CURRENT_LOCATION_LATITUDE, "");
    }
    /*
     * Get User Locations Longitude
     * */
    public String getCurrentLongitude() {
        return LeedgoPrefrences.readString(getActivity(), LeedgoPrefrences.CURRENT_LOCATION_LONGITUDE, "");
    }





    public void hideKeyBoardFromView(Activity mActivity) {
        InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = mActivity.getCurrentFocus();
        if (view == null) {
            view = new View(mActivity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        progressDialog.show();


    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*
     * Hide Keyboard
     * */
    public boolean hideKeyBoad(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    /*
     * Validate User Name
     * */
    public boolean isValidUserName(String email) {
        return Pattern.compile("/^(?=.{3,30}$)(?:[a-zA-Z\\d]+(?:(?:.|-|)[a-zA-Z\\d])*)+$/").matcher(email).matches();
    }


    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*
     * Error Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    /*
     *
     * Convert Simple Date to Formated Date
     * */
    public String getConvertedDateDiffrentSymbol(String mString) {
        String[] mArray = mString.split("T");
        String formattedDate = "";
        try {
            DateFormat srcDf = new SimpleDateFormat("dd/MM/yyyy");

            // parse the date string into Date object
            Date date = srcDf.parse(mArray[0]);

            DateFormat destDf = new SimpleDateFormat("dd MMM, yyyy");

            // format the date into another format
            formattedDate = destDf.format(date);

            System.out.println("Converted date is : " + formattedDate);
        } catch (Exception e) {
            e.toString();
        }

        return formattedDate;
    }


    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialog(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialogALL(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }


    /*
     * Time Picker
     * */
    public void showTimePicker(Activity mActivity, final TextView mTextView) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String format = "";

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        } else if (hourOfDay == 12) {

                            format = "PM";

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        } else {

                            format = "AM";
                        }

                        String tempHour = "" + hourOfDay;
                        String tempMinute = "" + minute;
                        if (tempHour.length() == 1)
                            tempHour = "0" + hourOfDay;
                        if (tempMinute.length() == 1)
                            tempMinute = "0" + minute;

                        mTextView.setText(tempHour + ":" + tempMinute + " " + format);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    /*
     *
     * Logout Dialog
     * */
    public void showLogoutDialog(final Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_logout);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView btnCancelB = alertDialog.findViewById(R.id.btnCancelB);
        TextView btnOkB = alertDialog.findViewById(R.id.btnOkB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnOkB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                SharedPreferences preferences = LeedgoPrefrences.getPreferences(mActivity);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                editor.apply();

//                Intent mIntent = new Intent(mActivity, SignInActivity.class);
//                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(mIntent);
//                finish();
            }
        });
        alertDialog.show();
    }


    public void openLinkOnBrowser(String mUrl) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(mUrl));
        startActivity(i);
    }


    public int getMonthDaysCount(int year, int month) {
        int number_Of_DaysInMonth = 0;

        switch (month) {
            case 1:
                month = 1;
                number_Of_DaysInMonth = 31;
                break;
            case 2:
                month = 2;
                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                    number_Of_DaysInMonth = 29;
                } else {
                    number_Of_DaysInMonth = 28;
                }
                break;
            case 3:
                month = 3;
                number_Of_DaysInMonth = 31;
                break;
            case 4:
                month = 4;
                number_Of_DaysInMonth = 30;
                break;
            case 5:
                month = 5;
                number_Of_DaysInMonth = 31;
                break;
            case 6:
                month = 6;
                number_Of_DaysInMonth = 30;
                break;
            case 7:
                month = 7;
                number_Of_DaysInMonth = 31;
                break;
            case 8:
                month = 8;
                number_Of_DaysInMonth = 31;
                break;
            case 9:
                month = 9;
                number_Of_DaysInMonth = 30;
                break;
            case 10:
                month = 10;
                number_Of_DaysInMonth = 31;
                break;
            case 11:
                month = 11;
                number_Of_DaysInMonth = 30;
                break;
            case 12:
                month = 12;
                number_Of_DaysInMonth = 31;
        }

        return number_Of_DaysInMonth;
    }


}
