package com.leedgo.app.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.leedgo.app.R;
import com.leedgo.app.activities.AddLocationsActivity;
import com.leedgo.app.adapters.GetClientLocationAdapter;
import com.leedgo.app.interfaces.PrimaryLocationInterface;
import com.leedgo.app.models.GetClientLocationData;
import com.leedgo.app.models.GetClientLocationModel;
import com.leedgo.app.models.ServiceAreaModel;
import com.leedgo.app.models.UpdatePriLocationModel;
import com.leedgo.app.models.locations.DeleteLocationModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;
import com.leedgo.app.utils.LeedgoPrefrences;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class LocationsFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = LocationsFragment.this.getClass().getSimpleName();
    /*
     * Widgets
     * */
    @BindView(R.id.locationsLL)
    LinearLayout locationsLL;
    @BindView(R.id.btnSaveB)
    Button btnSaveB;
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;
    @BindView(R.id.llinear)
    LinearLayout lly;
    LinearLayoutManager mLayoutManager;
    String client_location_id;
    /*
     * Initialize  Un binder for butter knife;
     * */
    private Unbinder unbinder;
    GetClientLocationAdapter mSearchLocationAdapter;
    private long mLastClickTab1 = 0;
    int mLocationID = 0;
    int mPos = 0;
    RadioGroup.LayoutParams rl;
    ArrayList<GetClientLocationData> mArraylist;

    /*
     * Initialize... Default Constructor
     * */
    public LocationsFragment() {
        // Required empty public constructor
    }


    PrimaryLocationInterface mPrimaryLocationInterface = new PrimaryLocationInterface() {
        @Override
        public void getPrimaryLocation(GetClientLocationData mModel, int position) {
            mLocationID = mModel.getId();
            mPos = position;
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_locations, container, false);
        unbinder = ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible()) {
            getUserLocationData();
            client_location_id = "";
        }
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.locationsLL, R.id.btnSaveB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.locationsLL:
                performLocationsClick();
                break;
            case R.id.btnSaveB:
                performSaveClick();
                break;

        }
    }

    public void preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
            performSaveClick();
            return;
        }
        mLastClickTab1 = SystemClock.elapsedRealtime();
    }

    private void performLocationsClick() {
        LeedgoPrefrences.writeString(getContext(), LeedgoPrefrences.EDITORADDLOCATION, "add");
        Intent mIntent = new Intent(getActivity(), AddLocationsActivity.class);
        ServiceAreaModel mServiceAreaModel = new ServiceAreaModel();
        mServiceAreaModel.setId(client_location_id);
        startActivityForResult(mIntent, Constants.REQUEST_CODE);
    }

    private void performSaveClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(getContext(), R.anim.bounce);
        btnSaveB.startAnimation(myAnim);
        if (isNetworkAvailable(getActivity())) {
            if (mLocationID != 0) {
                executePrimaryLocationRequest();
            }
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));

        }
    }

    private void getUserLocationData() {
        if (isNetworkAvailable(getActivity())) {
            if (mArraylist != null) {
                mArraylist.clear();
            }
            executeLocationRequest();
        } else
            showToast(getActivity(), getString(R.string.internet_connection_error));
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLocationRequest() {

        if (mArraylist != null) {
            mArraylist.clear();
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUserLocationsRequest(getAuthKey(), mParam()).enqueue(new Callback<GetClientLocationModel>() {
            @Override
            public void onResponse(Call<GetClientLocationModel> call, Response<GetClientLocationModel> response) {
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                GetClientLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArraylist = mModel.getData();
                    if (mArraylist.size() > 0) {
                        for (int i = 0; i < mArraylist.size(); i++) {
                            if (mArraylist.get(i).getLocationTitle().trim().equals("nullzxdsre")) {
                                mArraylist.remove(i);
                                i = 0;
                            }
                        }
                    }

                    for (int i = 0; i < mArraylist.size(); i++) {
                        if (mArraylist.get(i).getPrimaryValue() == 1) {
                            Collections.swap(mArraylist, i, 0);
                        }
                    }
                    setAdapter(mArraylist);
                } else {
                    showToast(getActivity(), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetClientLocationModel> call, Throwable t) {
                // dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    public void setAdapter(ArrayList<GetClientLocationData> data) {
        try {
            mLayoutManager = new LinearLayoutManager(getActivity());
            mSearchLocationAdapter = new GetClientLocationAdapter(getActivity(), data, mPrimaryLocationInterface, new GetClientLocationAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int positon, GetClientLocationData item, View view) {
                    switch (view.getId()) {
                        case R.id.imgItemDeleteIV:
                            View layout = mLayoutManager.findViewByPosition(positon);
                            DeleteLocation(item, positon);
                            layout.findViewById(R.id.imgItemDeleteIV).setClickable(false);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    layout.findViewById(R.id.imgItemDeleteIV).setClickable(true);
                                }
                            }, 1000);

                            break;
                        case R.id.imgItemEditIV:
                            View layout2 = mLayoutManager.findViewByPosition(positon);
                            TextView txTitle = layout2.findViewById(R.id.itemCityNameTV);
                            String title = txTitle.getText().toString().trim();
                            EditLocation(item, positon, title);
                            layout2.findViewById(R.id.imgItemEditIV).setClickable(false);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    layout2.findViewById(R.id.imgItemEditIV).setClickable(true);
                                }
                            }, 1000);

                            break;
                    }
                }
            });

            mRecyclerViewRV.setLayoutManager(mLayoutManager);
            mRecyclerViewRV.setAdapter(mSearchLocationAdapter);
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void DeleteLocation(GetClientLocationData item, int positon) {
        String message = "delete location";
        String type = "delete";
        client_location_id = String.valueOf(item.getId());
        funDelalertDialog(message, type, positon, item);

    }

    private void EditLocation(GetClientLocationData item, int positon, String title) {
        String message = "edit location";
        String type = "edit";
        client_location_id = String.valueOf(item.getId());
        LeedgoPrefrences.writeString(getContext(), LeedgoPrefrences.CLIENT_LOCTION_NEW_ID, client_location_id);
        funDelalertDialog(message, type, positon, item);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    void funDelalertDialog(String message, String type, int positon, GetClientLocationData item) {


        new IOSDialog.Builder(getContext())
                .setMessage("Are you sure you want to " + message + " ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (type.equals("delete")) {
                            executeDeleteapi(positon);

                        } else {
                            performClick(item);
                        }
                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private void performClick(GetClientLocationData item) {
        LeedgoPrefrences.writeString(getContext(), LeedgoPrefrences.EDITORADDLOCATION, "edit");
        Intent mIntent = new Intent(getContext(), AddLocationsActivity.class);
        mIntent.putExtra("locationtype", "edit");
        mIntent.putExtra("id", item.getId());
        mIntent.putExtra("title", item.getLocationTitle());
        mIntent.putExtra("location", item.getAddress());
        mIntent.putExtra("latitude", item.getLatitude());
        mIntent.putExtra("longitude", item.getLongitude());
        mIntent.putExtra("state", item.getState());
        mIntent.putExtra("zipcode", item.getZipCode());
        startActivity(mIntent);
    }

    /*
     * Execute api
     * */
    private Map<String, String> mUpdateParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", "" + mLocationID);
        mMap.put("primary_value", "1");
        mMap.put("client_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executePrimaryLocationRequest() {
        if (mArraylist != null) {
            mArraylist.clear();
        }
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addPrimaryAddressRequest(getAuthKey(), mUpdateParam()).enqueue(new Callback<UpdatePriLocationModel>() {
            @Override
            public void onResponse(Call<UpdatePriLocationModel> call, Response<UpdatePriLocationModel> response) {
                dismissProgressDialog();
                UpdatePriLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    getUserLocationData();
                } else {
                    showToast(getActivity(), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UpdatePriLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private Map<String, String> mParamDel() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", client_location_id);
        return mMap;
    }

    private void executeDeleteapi(int positon) {
        showProgressDialog(getActivity());
        Log.e("", "1" + mParam());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeDeleteLocation(getAuthKey(), mParamDel()).enqueue(new Callback<DeleteLocationModel>() {
            @Override
            public void onResponse(Call<DeleteLocationModel> call, Response<DeleteLocationModel> response) {
                dismissProgressDialog();
                Log.e("Dataq", "**RESPONSE**" + response.body().toString());
                client_location_id = "";
                DeleteLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArraylist.remove(positon);
                    mSearchLocationAdapter.notifyDataSetChanged();
//                    mSearchLocationAdapter.notifyItemChanged(positon);
//                    mSearchLocationAdapter.notifyItemRangeChanged(positon, mArraylist.size());
                } else {
                    showAlertDialog((Activity) getContext(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DeleteLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}