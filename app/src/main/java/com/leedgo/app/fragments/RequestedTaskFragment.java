package com.leedgo.app.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgo.app.R;
import com.leedgo.app.adapters.RequestedTaskAdapter;
import com.leedgo.app.models.RequestTaskModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class RequestedTaskFragment extends BaseFragment {
    @BindView(R.id.recyclerViewRvq)
    RecyclerView recyclerView;

    LinearLayoutManager mLayoutManager;
    ArrayList<RequestTaskModel.Data> mArrayList;

    public RequestedTaskFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_requested_task, container, false);
        ButterKnife.bind(this, mView);

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        performCompeletdTasks();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void performCompeletdTasks() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllTasks(mParam()).enqueue(new Callback<RequestTaskModel>() {
            @Override
            public void onResponse(Call<RequestTaskModel> call, Response<RequestTaskModel> response) {
                dismissProgressDialog();
                try {
                    Log.e(TAG, "**RESPONSE**" + mParam().toString() + ":::::" + response.body().toString());
                    RequestTaskModel mModel = null;
                    mModel = response.body();
                    if (mModel.getStatus() == 1) {
                        if (mArrayList != null) {
                            mArrayList.clear();
                        }
                        mArrayList = (ArrayList<RequestTaskModel.Data>) mModel.getData();


                        Collections.reverse(mArrayList);
                        mLayoutManager = new LinearLayoutManager(getActivity());
                        RequestedTaskAdapter mAdapter = new RequestedTaskAdapter(getActivity(), (ArrayList<RequestTaskModel.Data>) mArrayList);
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setAdapter(mAdapter);

                    } else {
                        showToast(getActivity(), mModel.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<RequestTaskModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });


    }
}