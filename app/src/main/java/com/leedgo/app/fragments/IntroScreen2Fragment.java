package com.leedgo.app.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.leedgo.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class IntroScreen2Fragment extends Fragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = IntroScreen2Fragment.this.getClass().getSimpleName();
    pl.droidsonroids.gif.GifImageView imgStepIV;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_intro_screen2, container, false);
        ButterKnife.bind(this,view);
        imgStepIV = (pl.droidsonroids.gif.GifImageView) view.findViewById(R.id.imgStepIV);
        imgStepIV.setImageResource(R.color.colorWhite);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: Fragment 2");
        Glide.with(requireContext())
                .asGif()
                .load(R.drawable.ic_step_2) // Replace with a valid url
                .addListener(new RequestListener<GifDrawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GifDrawable resource2, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
                        resource2.clearAnimationCallbacks();
                        resource2.setLoopCount(1);
                        return false;
                    }
                })
                .into(imgStepIV);
    }
}