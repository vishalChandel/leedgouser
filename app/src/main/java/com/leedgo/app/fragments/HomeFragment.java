package com.leedgo.app.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.leedgo.app.R;
import com.leedgo.app.activities.SeeAllActivity;
import com.leedgo.app.adapters.ServicesAdapter;
import com.leedgo.app.models.GetClientLatestRequestModel;
import com.leedgo.app.models.NotificationModel;
import com.leedgo.app.models.ServicesDataModel;
import com.leedgo.app.models.ServicesModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.EasyLocationProvider;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class HomeFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeFragment.this.getClass().getSimpleName();
    /*
     * Initialize  Un binder for butter knife;
     * */
    private Unbinder unbinder;
    /*
     * Widgets
     * */
    @BindView(R.id.txtSeeAllTV)
    TextView txtSeeAllTV;
    @BindView(R.id.txtStatusTV)
    TextView txtStatusTV;
    @BindView(R.id.txtNoUpcAppTV)
    TextView txtNoUpcAppTV;
    @BindView(R.id.imgItemIV)
    ImageView imageView;
    @BindView(R.id.llyTimer)
    LinearLayout llyTImer;
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;

    @BindView(R.id.mCountDownCD)
    com.leedgo.app.views.CountdownChronometer mCountDownCD;

    /************************
     *Fused Google Location
     **************/
    public static final int REQUEST_PERMISSION_CODE = 919;
    public String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    public String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    EasyLocationProvider easyLocationProvider;
    /*
     * Initialize...
     * */
    ArrayList<ServicesDataModel> mServicesArrayList = new ArrayList<>();
    ArrayList<ServicesDataModel> copyArrayList = new ArrayList<>();

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, mView);
        getServicesData();
        return mView;
    }


    @Override
    public void onResume() {
        executeChronoApi();
        super.onResume();
    }

    @Override
    public void onPause() {
        mCountDownCD.stop();
        super.onPause();
    }

    private void setCountDownTimmer() {
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "AvenirBold.otf");
        mCountDownCD.setTypeface(font);
        //mCountDownCD.setFormat("DD:HH:MM:SS");
        mCountDownCD.setBase(System.currentTimeMillis() + 160000000);

    }

    private void getServicesData() {
        if (isNetworkAvailable(getActivity())) {
            executeServicesRequest();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
    }

    private void executeServicesRequest() {
        mServicesArrayList.clear();
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getServicesRequest().enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                dismissProgressDialog();
                try {
                    ServicesModel mModel = response.body();
                    if (mModel.getStatus() == 1) {
                        for (int i = 0; i <= 5; i++) {
                            mServicesArrayList.add(response.body().getData().get(i));

                        }
                        // mServicesArrayList.addAll(response.body().getData());
                        ArrayList<String> mArrayList = new ArrayList<>();
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            if (!response.body().getData().get(i).getImage().equals("")) {
                                mArrayList.add(response.body().getData().get(i).getImage());
                            }
                        }
                        try {

                            SharedPreferences prefs = getContext().getSharedPreferences("LeedGo", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            Set<String> set = new HashSet<String>();
                            set.addAll(mArrayList);
                            editor.putStringSet("myKey", set);
                            editor.commit();

                            setAdapter();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        showAlertDialog(getActivity(), mModel.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//               Log.e(TAG, "**RESPONSE**"+"   "+getAuthKey()+" "+getUserId());

            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        ServicesAdapter mSearchLocationAdapter = new ServicesAdapter(getActivity(), mServicesArrayList);
        mRecyclerViewRV.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mRecyclerViewRV.setAdapter(mSearchLocationAdapter);
        updateLocation();
    }

    private void updateLocation() {
        if (checkPermission()) {
            getLocationLatLong();
        } else {
            requestPermission();
        }
    }

    /*********
     * Support for Marshmallows Version
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    private boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(getActivity(), mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(getActivity(), mAccessCourseLocation);
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getLocationLatLong();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //requestPermission();

                }
                return;
            }

        }
    }

    public void getLocationLatLong() {
        easyLocationProvider = new EasyLocationProvider.Builder(getActivity())
                .setInterval(3000)
                .setFastestInterval(1000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setListener(new EasyLocationProvider.EasyLocationCallback() {
                    @Override
                    public void onGoogleAPIClient(GoogleApiClient googleApiClient, String message) {
                        Log.e("EasyLocationProvider", "onGoogleAPIClient: " + message);
                    }

                    @Override
                    public void onLocationUpdated(double latitude, double longitude) {
                        Log.e("EasyLocationProvider", "onLocationUpdated:: " + "Latitude: " + latitude + " Longitude: " + longitude);
                    }

                    @Override
                    public void onLocationUpdateRemoved() {
                        Log.e("EasyLocationProvider", "onLocationUpdateRemoved");
                    }
                }).build();

        getLifecycle().addObserver(easyLocationProvider);

    }


    /*
     * Execute api update profile
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());

        return mMap;
    }

    private void executeChronoApi() {
        //showProgressDialog(getActivity());
        Log.e("Dataq", "1");
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeLatestRequest(getAuthKey(), mParam()).enqueue(new Callback<GetClientLatestRequestModel>() {
            @Override
            public void onResponse(Call<GetClientLatestRequestModel> call, Response<GetClientLatestRequestModel> response) {
                dismissProgressDialog();
                try {
                    Log.e(TAG, "**RESPONSE**" + response.body().toString());
                    txtStatusTV.setVisibility(View.VISIBLE);
                    GetClientLatestRequestModel mModel = response.body();
                    if (mModel.getStatus() == 1) {
                        setDataChrorno(mModel.getData());
                        if (mModel.getData().getWork_date().length() > 2) {
                            imageView.setVisibility(View.VISIBLE);
                            llyTImer.setVisibility(View.VISIBLE);
                            mCountDownCD.setVisibility(View.VISIBLE);
                            txtStatusTV.setText(R.string.your_request_is);
                        } else {
                            imageView.setVisibility(View.GONE);
                            llyTImer.setVisibility(View.GONE);
                            txtNoUpcAppTV.setVisibility(View.VISIBLE);
                            txtStatusTV.setVisibility(View.GONE);
                            mCountDownCD.setVisibility(View.GONE);
                        }


                    } else if (mModel.getStatus() == 0) {
                        imageView.setVisibility(View.GONE);
                        llyTImer.setVisibility(View.GONE);
                        txtNoUpcAppTV.setVisibility(View.VISIBLE);
                        txtStatusTV.setVisibility(View.GONE);
                        mCountDownCD.setVisibility(View.GONE);
                        //  Log.e("Data", "error fetch");

                    } else {
                        imageView.setVisibility(View.VISIBLE);
                        setDataChrorno(mModel.getData());
                        llyTImer.setVisibility(View.VISIBLE);
                        mCountDownCD.setVisibility(View.VISIBLE);
                        txtStatusTV.setText("Upcoming Request in");
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetClientLatestRequestModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setDataChrorno(GetClientLatestRequestModel.Data model) {
        String endDate = model.getWork_date();
        Log.e("Checklp", endDate);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "AvenirBold.otf");
        mCountDownCD.setTypeface(font);
        String value;
        String timeDifference = "";

        //date formatter as per the coder need
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH mm");

        //parse the string date-ti
        // me to Date object


        DateFormat srcDf = new SimpleDateFormat("dd-MM-yyyy");


        // parse the date string into Date object
        Date enddDate = null;
        //  try {111111111111
        try {
            enddDate = srcDf.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.e("checklop", "" + enddDate);
        // } catch (ParseException e) {
        //   e.printStackTrace();
        //  }


        //end date will be the current system time to calculate the lapse time difference
        //if needed, coder can add end date to whatever date
        Date currenTdate = new Date();
        System.out.println(sdf.format(currenTdate));
        String val = sdf.format(currenTdate);
        Date currentDatek = null;
        try {
            currentDatek = sdf.parse(val);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert enddDate != null;
        long duration = enddDate.getTime() - currentDatek.getTime();
        mCountDownCD.setBase(System.currentTimeMillis() + TimeUnit.MILLISECONDS.toMillis(duration));
        mCountDownCD.start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.txtSeeAllTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSeeAllTV:
                performSeeAllClick();
                break;

        }
    }

    private void performSeeAllClick() {
        startActivity(new Intent(getActivity(), SeeAllActivity.class));
    }

}