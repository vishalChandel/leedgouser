package com.leedgo.app.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.leedgo.app.R;
import com.leedgo.app.adapters.ChatFAdapter;
import com.leedgo.app.models.ChatModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class InboxFragment extends BaseFragment {

    @BindView(R.id.chatf_recyclerr)
    RecyclerView recyclerView;
    @BindView(R.id.swiperefresh_chat)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.p_bar_chat)
    ProgressBar p_bar;
    @BindView(R.id.lly_chatf_nothing)
    LinearLayout lly_chatf_nothing;
    @BindView(R.id.chatf_searchTab_text)
    EditText   tx_searchtab;
    @BindView(R.id.text_no_result_chattab)
    TextView text_no_result;


    ChatModel mGetDetailsModel;
    List<ChatModel.Data> mArrayList = new ArrayList<>();
    ChatFAdapter chatFAdapter;


    public InboxFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_inbox, container, false);
        ButterKnife.bind(this,mView);
        swiperefresh.setProgressViewOffset(false, 0, 200);
        swiperefresh.setColorSchemeResources(R.color.colorBlack);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getDetails();
            }
        });

        getDetails();

        tx_searchtab.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if (mArrayList.size()>0) {
                    chatFAdapter.getFilter().filter(string);
                }
            }
        });

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        executeDetailsApi();
    }

    private void getDetails() {
        executeDetailsApi();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        mMap.put("user_type","client");
        return mMap;
    }

    private void executeDetailsApi() {
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getAllChats(mParams()).enqueue(new Callback<ChatModel>() {
            @Override
            public void onResponse(Call<ChatModel> call, Response<ChatModel> response) {
                dismissProgressDialog();
                swiperefresh.setRefreshing(false);
                Log.e("", "**RESPONSE**" + response.body());
                mGetDetailsModel = response.body();
                if (mGetDetailsModel.getStatus() == 1) {
                    lly_chatf_nothing.setVisibility(View.GONE);
                    mArrayList =response.body().getData();
                    setChatAdapter();
                } else {
                    lly_chatf_nothing.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ChatModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setChatAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        chatFAdapter = new ChatFAdapter(getActivity(), (ArrayList<ChatModel.Data>) mArrayList,text_no_result);
        recyclerView.setAdapter(chatFAdapter);
        chatFAdapter.notifyDataSetChanged();
    }

}