package com.leedgo.app.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.leedgo.app.R;
import com.leedgo.app.activities.HomeActivity;
import com.leedgo.app.activities.IntroActivity;
import com.leedgo.app.activities.SignInActivity;
import com.leedgo.app.adapters.GetClientLocationAdapter;
import com.leedgo.app.adapters.RequestedHistoryAdapter;
import com.leedgo.app.adapters.RequestedHistoryAdapter2;
import com.leedgo.app.models.CompleteTaskModel;
import com.leedgo.app.models.GetClientLocationModel;
import com.leedgo.app.models.PendingTaskModel;
import com.leedgo.app.models.RequestTaskModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class RequestHistoryFragment extends BaseFragment {

    public RequestHistoryFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.PendingNotShowItem)
    TextView txPendingNotShow;
    @BindView(R.id.PendingShowItem)
    TextView txPendingShow;
    @BindView(R.id.RequestShowItem)
    TextView txCompletedShow;
    @BindView(R.id.RequestHideItem)
    TextView txCompletedNotShow;
    @BindView(R.id.recyclerRequestedHistory)
    RecyclerView mRecyclerview;
    @BindView(R.id.recyclerRequestedHistory2)
    RecyclerView mRecyclerview2;

    @BindView(R.id.dummyTV)
    TextView dummyTV;


    LinearLayoutManager mLayoutManager;
    ArrayList<PendingTaskModel.Datum> mArrayList;
    ArrayList<CompleteTaskModel.Datum> mArrayList2;
    ArrayList<CompleteTaskModel.Datum> mArrayList2t;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_request_history, container, false);
        ButterKnife.bind(this, mView);
        performCompeletdTasks();
        return mView;
    }

    @OnClick({R.id.PendingNotShowItem, R.id.RequestHideItem})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.PendingNotShowItem:
                dummyTV.setVisibility(View.GONE);
                performPendingTasks();
                break;
            case R.id.RequestHideItem:
                dummyTV.setVisibility(View.GONE);
                performCompeletdTasks();
                break;

        }
    }

    private void performCompeletdTasks() {

        txCompletedNotShow.setVisibility(View.GONE);
        txCompletedShow.setVisibility(View.VISIBLE);
        txPendingShow.setVisibility(View.GONE);
        txPendingNotShow.setVisibility(View.VISIBLE);
        executeCompletedApi();

    }


    private void performPendingTasks() {
        txPendingShow.setVisibility(View.VISIBLE);
        txPendingNotShow.setVisibility(View.GONE);
        txCompletedNotShow.setVisibility(View.VISIBLE);
        txCompletedShow.setVisibility(View.GONE);
        executePendingApi();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCompletedApi() {
        mRecyclerview2.setVisibility(View.GONE);
        mRecyclerview.setVisibility(View.VISIBLE);
        Log.e("Checklop", "0");
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.completeTaskModel(getAuthKey(),mParam()).enqueue(new Callback<CompleteTaskModel>() {
            @Override
            public void onResponse(Call<CompleteTaskModel> call, Response<CompleteTaskModel> response) {
                dismissProgressDialog();
                Log.e("Checklq", "0");
                Log.e("Checklop", "1");
                Log.e(TAG, "**RESPONSE**" + mParam().toString() + ":::::" + response.body().toString());
                CompleteTaskModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    dummyTV.setVisibility(View.GONE);
                    if (mArrayList2 != null) {
                        mArrayList2.clear();
                    }
                    mArrayList2 = (ArrayList<CompleteTaskModel.Datum>) mModel.getData();

                    Log.e("Checklop", "2");
                    if (mArrayList2.size() > 0) {
                        for (int i = mArrayList2.size() - 1; i >= 0; i--) {

                        }
                    }

                    if (mArrayList2.size() == 0) {
                        dummyTV.setVisibility(View.VISIBLE);
                        setAdapter(mArrayList2);
                    } else {
                        setAdapter(mArrayList2);
                    }
                } else {
                    dummyTV.setVisibility(View.VISIBLE);
                    showToast(getActivity(), mModel.getMessage());
                }




            }

            @Override
            public void onFailure(Call<CompleteTaskModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void executePendingApi() {
        mRecyclerview.setVisibility(View.GONE);
        mRecyclerview2.setVisibility(View.VISIBLE);
        try {
            showProgressDialog(getActivity());
            ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            mApiInterface.pendingTaskModel(getAuthKey(),mParam()).enqueue(new Callback<PendingTaskModel>() {
                @Override
                public void onResponse(Call<PendingTaskModel> call, Response<PendingTaskModel> response) {
                    dismissProgressDialog();
                    PendingTaskModel mModel = response.body();
                    if (mModel.getStatus() == 1) {
                        dummyTV.setVisibility(View.GONE);
                        mArrayList = (ArrayList<PendingTaskModel.Datum>) mModel.getData();
                        setAdapter2(mArrayList);
                    } else {
                        dummyTV.setVisibility(View.VISIBLE);
                        showToast(getActivity(), mModel.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<PendingTaskModel> call, Throwable t) {
                    dismissProgressDialog();
                    Log.e(TAG, "**ERROR**" + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setAdapter(List<CompleteTaskModel.Datum> data) {
        try {
            Collections.reverse(data);
            mRecyclerview2.setVisibility(View.GONE);
            mRecyclerview.setVisibility(View.VISIBLE);
            mLayoutManager = new LinearLayoutManager(getActivity());
            RequestedHistoryAdapter mAdapter = new RequestedHistoryAdapter(getActivity(),mArrayList2,getUserId());
            mRecyclerview.setLayoutManager(mLayoutManager);
            mRecyclerview.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setAdapter2(ArrayList<PendingTaskModel.Datum> data) {
        try {

            Collections.reverse(data);

            mRecyclerview.setVisibility(View.GONE);
            mRecyclerview2.setVisibility(View.VISIBLE);
            mLayoutManager = new LinearLayoutManager(getActivity());
            RequestedHistoryAdapter2 mAdapter = new RequestedHistoryAdapter2(getActivity(), (ArrayList<PendingTaskModel.Datum>) data);
            mRecyclerview2.setLayoutManager(mLayoutManager);
            mRecyclerview2.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}