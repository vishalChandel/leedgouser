package com.leedgo.app.models.profile;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("updated_on")
	private Object updatedOn;

	@SerializedName("google_id")
	private Object googleId;

	@SerializedName("role")
	private String role;

	@SerializedName("gender")
	private String gender;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("lastvisit")
	private String lastvisit;

	@SerializedName("location_id")
	private String locationId;

	@SerializedName("zip_code")
	private String zipCode;

	@SerializedName("password")
	private String password;

	@SerializedName("cellno")
	private String cellno;


	@SerializedName("category_id")
	private String categoryId;

	@SerializedName("location_title")
	private String locationTitle;

	@SerializedName("id")
	private String id;

	@SerializedName("state")
	private String state;

	@SerializedName("email")
	private String email;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("authKey")
	private String authKey;

	@SerializedName("address")
	private String address;

	@SerializedName("push_status")
	private String pushStatus;

	@SerializedName("disable_notification")
	private String disableNotification;

	@SerializedName("profile_pic")
	private String profilePic;

	@SerializedName("usertype")
	private String usertype;

	@SerializedName("apple_id")
	private Object appleId;

	@SerializedName("facebook_id")
	private Object facebookId;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("name")
	private String name;

	@SerializedName("user_location")
	private String userLocation;

	@SerializedName("primary_value")
	private String primaryValue;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("superuser")
	private String superuser;

	@SerializedName("username")
	private String username;

	@SerializedName("status")
	private String status;

	public void setUpdatedOn(Object updatedOn){
		this.updatedOn = updatedOn;
	}

	public Object getUpdatedOn(){
		return updatedOn;
	}

	public void setGoogleId(Object googleId){
		this.googleId = googleId;
	}

	public Object getGoogleId(){
		return googleId;
	}

	public void setRole(String role){
		this.role = role;
	}

	public String getRole(){
		return role;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDeviceType(String deviceType){
		this.deviceType = deviceType;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public void setLastvisit(String lastvisit){
		this.lastvisit = lastvisit;
	}

	public String getLastvisit(){
		return lastvisit;
	}

	public void setLocationId(String locationId){
		this.locationId = locationId;
	}

	public String getLocationId(){
		return locationId;
	}

	public void setZipCode(String zipCode){
		this.zipCode = zipCode;
	}

	public String getZipCode(){
		return zipCode;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setCellno(String cellno){
		this.cellno = cellno;
	}

	public String getCellno(){
		return cellno;
	}

	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
	}

	public String getCategoryId(){
		return categoryId;
	}

	public void setLocationTitle(String locationTitle){
		this.locationTitle = locationTitle;
	}

	public String getLocationTitle(){
		return locationTitle;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	public void setAuthKey(String authKey){
		this.authKey = authKey;
	}

	public String getAuthKey(){
		return authKey;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setPushStatus(String pushStatus){
		this.pushStatus = pushStatus;
	}

	public String getPushStatus(){
		return pushStatus;
	}

	public void setDisableNotification(String disableNotification){
		this.disableNotification = disableNotification;
	}

	public String getDisableNotification(){
		return disableNotification;
	}

	public void setProfilePic(String profilePic){
		this.profilePic = profilePic;
	}

	public String getProfilePic(){
		return profilePic;
	}

	public void setUsertype(String usertype){
		this.usertype = usertype;
	}

	public String getUsertype(){
		return usertype;
	}

	public void setAppleId(Object appleId){
		this.appleId = appleId;
	}

	public Object getAppleId(){
		return appleId;
	}

	public void setFacebookId(Object facebookId){
		this.facebookId = facebookId;
	}

	public Object getFacebookId(){
		return facebookId;
	}

	public void setDeviceToken(String deviceToken){
		this.deviceToken = deviceToken;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setUserLocation(String userLocation){
		this.userLocation = userLocation;
	}

	public String getUserLocation(){
		return userLocation;
	}

	public void setPrimaryValue(String primaryValue){
		this.primaryValue = primaryValue;
	}

	public String getPrimaryValue(){
		return primaryValue;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setSuperuser(String superuser){
		this.superuser = superuser;
	}

	public String getSuperuser(){
		return superuser;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"updated_on = '" + updatedOn + '\'' + 
			",google_id = '" + googleId + '\'' + 
			",role = '" + role + '\'' + 
			",gender = '" + gender + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",device_type = '" + deviceType + '\'' + 
			",lastvisit = '" + lastvisit + '\'' + 
			",location_id = '" + locationId + '\'' + 
			",zip_code = '" + zipCode + '\'' + 
			",password = '" + password + '\'' + 
			",cellno = '" + cellno + '\'' + 
			",category_id = '" + categoryId + '\'' + 
			",location_title = '" + locationTitle + '\'' + 
			",id = '" + id + '\'' + 
			",state = '" + state + '\'' + 
			",email = '" + email + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",authKey = '" + authKey + '\'' + 
			",address = '" + address + '\'' + 
			",push_status = '" + pushStatus + '\'' + 
			",disable_notification = '" + disableNotification + '\'' + 
			",profile_pic = '" + profilePic + '\'' + 
			",usertype = '" + usertype + '\'' + 
			",apple_id = '" + appleId + '\'' + 
			",facebook_id = '" + facebookId + '\'' + 
			",device_token = '" + deviceToken + '\'' + 
			",name = '" + name + '\'' + 
			",user_location = '" + userLocation + '\'' + 
			",primary_value = '" + primaryValue + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			",superuser = '" + superuser + '\'' + 
			",username = '" + username + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}