package com.leedgo.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class LocationServicesRawList  implements Serializable {
    ArrayList<String> ServiceList = new ArrayList<String>();

    public ArrayList<String> getServiceList() {
        return ServiceList;
    }

    public void setServiceList(ArrayList<String> serviceList) {
        ServiceList = serviceList;
    }
}
