package com.leedgo.app.models;

public class CardData {

        private String id;

        private String client_id;

        private String customer_id;

        private String card_holder_name;

        private String object;

        private String brand;

        private String country;

        private String cvc_check;

        private String exp_month;

        private String exp_year;

        private String funding;

        private String card_id;

        private String card_last4;

        private String creation_time;

        private String isPrimary;

        public void setId(String id) {
            this.id = id;
        }

        public String getId() {
            return this.id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }

        public String getClient_id() {
            return this.client_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public String getCustomer_id() {
            return this.customer_id;
        }

        public void setCard_holder_name(String card_holder_name) {
            this.card_holder_name = card_holder_name;
        }

        public String getCard_holder_name() {
            return this.card_holder_name;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public String getObject() {
            return this.object;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getBrand() {
            return this.brand;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCountry() {
            return this.country;
        }

        public void setCvc_check(String cvc_check) {
            this.cvc_check = cvc_check;
        }

        public String getCvc_check() {
            return this.cvc_check;
        }

        public void setExp_month(String exp_month) {
            this.exp_month = exp_month;
        }

        public String getExp_month() {
            return this.exp_month;
        }

        public void setExp_year(String exp_year) {
            this.exp_year = exp_year;
        }

        public String getExp_year() {
            return this.exp_year;
        }

        public void setFunding(String funding) {
            this.funding = funding;
        }

        public String getFunding() {
            return this.funding;
        }

        public void setCard_id(String card_id) {
            this.card_id = card_id;
        }

        public String getCard_id() {
            return this.card_id;
        }

        public void setCard_last4(String card_last4) {
            this.card_last4 = card_last4;
        }

        public String getCard_last4() {
            return this.card_last4;
        }

        public void setCreation_time(String creation_time) {
            this.creation_time = creation_time;
        }

        public String getCreation_time() {
            return this.creation_time;
        }

        public void setIsPrimary(String isPrimary) {
            this.isPrimary = isPrimary;
        }

        public String getIsPrimary() {
            return this.isPrimary;
        }

}
