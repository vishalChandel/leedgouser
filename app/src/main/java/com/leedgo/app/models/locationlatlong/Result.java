package com.leedgo.app.models.locationlatlong;

import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("geometry")
    private Geometry geometry;

    @SerializedName("formatted_address")
    private String formatted_address;

    @SerializedName("place_id")
    private String placeId;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    @Override
    public String toString() {
        return
                "Result{" +
                        ",geometry = '" + geometry + '\'' +
                        ",place_id = '" + placeId + '\'' +
                        ",formatted_address = '" + formatted_address + '\'' +
                        "}";
    }
}