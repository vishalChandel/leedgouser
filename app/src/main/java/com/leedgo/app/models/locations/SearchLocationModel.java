package com.leedgo.app.models.locations;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchLocationModel{

	@SerializedName("predictions")
	private ArrayList<PredictionsItem> predictions;

	@SerializedName("status")
	private String status;

	public void setPredictions(ArrayList<PredictionsItem> predictions){
		this.predictions = predictions;
	}

	public ArrayList<PredictionsItem> getPredictions(){
		return predictions;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"SearchLocationModel{" +
			"predictions = '" + predictions + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}