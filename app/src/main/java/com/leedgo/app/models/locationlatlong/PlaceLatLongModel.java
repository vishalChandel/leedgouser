package com.leedgo.app.models.locationlatlong;

import com.google.gson.annotations.SerializedName;

public class PlaceLatLongModel {

	@SerializedName("result")
	private Result result;

	@SerializedName("status")
	private String status;

	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return result;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"Response{" +
			"result = '" + result + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}