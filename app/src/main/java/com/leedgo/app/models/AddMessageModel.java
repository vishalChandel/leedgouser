package com.leedgo.app.models;

public class AddMessageModel {
    private int status;

    private String message;

    private Data data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(Data data){
        this.data = data;
    }
    public Data getData(){
        return this.data;
    }
    public class Data
    {
        private String id;

        private String sender_id;

        private String receiver_id;

        private String room_id;

        private String sender_type;

        private String image;

        private String message;

        private String message_time;

        private String message_seen;

        private String receiver_profile_pic;

        private String sender_profile_pic;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setSender_id(String sender_id){
            this.sender_id = sender_id;
        }
        public String getSender_id(){
            return this.sender_id;
        }
        public void setReceiver_id(String receiver_id){
            this.receiver_id = receiver_id;
        }
        public String getReceiver_id(){
            return this.receiver_id;
        }
        public void setRoom_id(String room_id){
            this.room_id = room_id;
        }
        public String getRoom_id(){
            return this.room_id;
        }
        public void setSender_type(String sender_type){
            this.sender_type = sender_type;
        }
        public String getSender_type(){
            return this.sender_type;
        }
        public void setImage(String image){
            this.image = image;
        }
        public String getImage(){
            return this.image;
        }
        public void setMessage(String message){
            this.message = message;
        }
        public String getMessage(){
            return this.message;
        }
        public void setMessage_time(String message_time){
            this.message_time = message_time;
        }
        public String getMessage_time(){
            return this.message_time;
        }
        public void setMessage_seen(String message_seen){
            this.message_seen = message_seen;
        }
        public String getMessage_seen(){
            return this.message_seen;
        }
        public void setReceiver_profile_pic(String receiver_profile_pic){
            this.receiver_profile_pic = receiver_profile_pic;
        }
        public String getReceiver_profile_pic(){
            return this.receiver_profile_pic;
        }
        public void setSender_profile_pic(String sender_profile_pic){
            this.sender_profile_pic = sender_profile_pic;
        }
        public String getSender_profile_pic(){
            return this.sender_profile_pic;
        }
    }
}
