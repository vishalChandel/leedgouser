package com.leedgo.app.models;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class GetClientLocationModel{

	@SerializedName("data")
	private ArrayList<GetClientLocationData> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(ArrayList<GetClientLocationData> data){
		this.data = data;
	}

	public ArrayList<GetClientLocationData> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"GetClientLocationModel{" +
			"data = '" + data + '\'' +
			",message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}