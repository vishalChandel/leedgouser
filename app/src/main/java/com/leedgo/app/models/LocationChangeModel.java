package com.leedgo.app.models;

import java.io.Serializable;

public class LocationChangeModel implements Serializable {

    String id="";
    String title="";
    String latitude="";
    String longitude="";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "LocationChangeModel{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
