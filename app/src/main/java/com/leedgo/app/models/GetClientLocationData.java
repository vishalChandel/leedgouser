package com.leedgo.app.models;

import com.google.gson.annotations.SerializedName;

public class GetClientLocationData {

	@SerializedName("address")
	private String address;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("location_title")
	private String locationTitle;

	@SerializedName("id")
	private int id;

	@SerializedName("primary_value")
	private int primaryValue;

	@SerializedName("state")
	private String state;

	@SerializedName("client_id")
	private int clientId;

	@SerializedName("zip_code")
	private String zipCode;

	@SerializedName("longitude")
	private String longitude;

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setLocationTitle(String locationTitle){
		this.locationTitle = locationTitle;
	}

	public String getLocationTitle(){
		return locationTitle;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPrimaryValue(int primaryValue){
		this.primaryValue = primaryValue;
	}

	public int getPrimaryValue(){
		return primaryValue;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setClientId(int clientId){
		this.clientId = clientId;
	}

	public int getClientId(){
		return clientId;
	}

	public void setZipCode(String zipCode){
		this.zipCode = zipCode;
	}

	public String getZipCode(){
		return zipCode;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return
			"DataItem{" +
			"address = '" + address + '\'' +
			",latitude = '" + latitude + '\'' +
			",location_title = '" + locationTitle + '\'' +
			",id = '" + id + '\'' +
			",primary_value = '" + primaryValue + '\'' +
			",state = '" + state + '\'' +
			",client_id = '" + clientId + '\'' +
			",zip_code = '" + zipCode + '\'' +
			",longitude = '" + longitude + '\'' +
			"}";
		}
}