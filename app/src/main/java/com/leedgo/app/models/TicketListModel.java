package com.leedgo.app.models;

import java.util.List;

public class TicketListModel {

        private int status;

        private String message;

        private List<Data> data;

        public void setStatus(int status){
        this.status = status;
    }
        public int getStatus(){
        return this.status;
    }
        public void setMessage(String message){
        this.message = message;
    }
        public String getMessage(){
        return this.message;
    }
        public void setData(List<Data> data){
        this.data = data;
    }
        public List<Data> getData(){
        return this.data;
    }

    public class Data
    {
        private String id;

        private String user_id;

        private String role;

        private String task_id;

        private String ticket_no;

        private String description;

        private String document;

        private String ticket_status;

        private String creation_time;
        private String category_id;
        private String subcategory_id;
        private String category_name;

        private  String icon_img;
        private  String sub_category_name;

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getSubcategory_id() {
            return subcategory_id;
        }

        public void setSubcategory_id(String subcategory_id) {
            this.subcategory_id = subcategory_id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getSub_category_name() {
            return sub_category_name;
        }

        public String getIcon_img() {
            return icon_img;
        }

        public void setIcon_img(String icon_img) {
            this.icon_img = icon_img;
        }

        public void setSub_category_name(String sub_category_name) {
            this.sub_category_name = sub_category_name;
        }

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setUser_id(String user_id){
            this.user_id = user_id;
        }
        public String getUser_id(){
            return this.user_id;
        }
        public void setRole(String role){
            this.role = role;
        }
        public String getRole(){
            return this.role;
        }
        public void setTask_id(String task_id){
            this.task_id = task_id;
        }
        public String getTask_id(){
            return this.task_id;
        }
        public void setTicket_no(String ticket_no){
            this.ticket_no = ticket_no;
        }
        public String getTicket_no(){
            return this.ticket_no;
        }
        public void setDescription(String description){
            this.description = description;
        }
        public String getDescription(){
            return this.description;
        }
        public void setDocument(String document){
            this.document = document;
        }
        public String getDocument(){
            return this.document;
        }
        public void setTicket_status(String ticket_status){
            this.ticket_status = ticket_status;
        }
        public String getTicket_status(){
            return this.ticket_status;
        }
        public void setCreation_time(String creation_time){
            this.creation_time = creation_time;
        }
        public String getCreation_time(){
            return this.creation_time;
        }
    }
}
