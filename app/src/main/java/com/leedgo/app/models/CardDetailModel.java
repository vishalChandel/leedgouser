package com.leedgo.app.models;

import java.util.List;

public class CardDetailModel {
    private int status;

    private String message;

    private List<CardData> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<CardData> data){
        this.data = data;
    }
    public List<CardData> getData(){
        return this.data;
    }


}
