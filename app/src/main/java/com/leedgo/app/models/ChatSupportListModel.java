package com.leedgo.app.models;

import java.util.List;

public class ChatSupportListModel {
    private int status;

    private String message;

    private List<AllSupportMessageModel> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<AllSupportMessageModel> data){
        this.data = data;
    }
    public List<AllSupportMessageModel> getData(){
        return this.data;
    }


}
