package com.leedgo.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompleteTaskModel {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("service_name")
        @Expose
        private String serviceName;
        @SerializedName("service_rating")
        @Expose
        private String serviceRating;
        @SerializedName("work_date")
        @Expose
        private String workDate;
        @SerializedName("work_time")
        @Expose
        private Object workTime;
        @SerializedName("task_id")
        @Expose
        private String taskId;
        @SerializedName("assigned_amount")
        @Expose
        private String assignedAmount;
        @SerializedName("work_details")
        @Expose
        private String workDetails;
        @SerializedName("date_of_completion")
        @Expose
        private String dateOfCompletion;
        @SerializedName("provider_detail")
        @Expose
        private ProviderDetail providerDetail;

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public String getServiceRating() {
            return serviceRating;
        }

        public void setServiceRating(String serviceRating) {
            this.serviceRating = serviceRating;
        }

        public String getWorkDate() {
            return workDate;
        }

        public void setWorkDate(String workDate) {
            this.workDate = workDate;
        }

        public Object getWorkTime() {
            return workTime;
        }

        public void setWorkTime(Object workTime) {
            this.workTime = workTime;
        }

        public String getTaskId() {
            return taskId;
        }

        public void setTaskId(String taskId) {
            this.taskId = taskId;
        }

        public String getAssignedAmount() {
            return assignedAmount;
        }

        public void setAssignedAmount(String assignedAmount) {
            this.assignedAmount = assignedAmount;
        }

        public String getWorkDetails() {
            return workDetails;
        }

        public void setWorkDetails(String workDetails) {
            this.workDetails = workDetails;
        }

        public String getDateOfCompletion() {
            return dateOfCompletion;
        }

        public void setDateOfCompletion(String dateOfCompletion) {
            this.dateOfCompletion = dateOfCompletion;
        }

        public ProviderDetail getProviderDetail() {
            return providerDetail;
        }

        public void setProviderDetail(ProviderDetail providerDetail) {
            this.providerDetail = providerDetail;
        }

        public class ProviderDetail {

            @SerializedName("provider_id")
            @Expose
            private String providerId;
            @SerializedName("provider_name")
            @Expose
            private String providerName;
            @SerializedName("business_email")
            @Expose
            private String businessEmail;
            @SerializedName("password")
            @Expose
            private String password;
            @SerializedName("facebook_id")
            @Expose
            private Object facebookId;
            @SerializedName("google_id")
            @Expose
            private String googleId;
            @SerializedName("apple_id")
            @Expose
            private Object appleId;
            @SerializedName("profile_image")
            @Expose
            private String profileImage;
            @SerializedName("profile_pic_server")
            @Expose
            private String profilePicServer;
            @SerializedName("latitude")
            @Expose
            private Object latitude;
            @SerializedName("longitude")
            @Expose
            private Object longitude;
            @SerializedName("address")
            @Expose
            private Object address;
            @SerializedName("creation_time")
            @Expose
            private String creationTime;
            @SerializedName("disable")
            @Expose
            private String disable;
            @SerializedName("device_token")
            @Expose
            private String deviceToken;
            @SerializedName("device_type")
            @Expose
            private String deviceType;
            @SerializedName("disable_notification")
            @Expose
            private String disableNotification;
            @SerializedName("account_id")
            @Expose
            private String accountId;
            @SerializedName("stripe_account_status")
            @Expose
            private String stripeAccountStatus;
            @SerializedName("phone")
            @Expose
            private String phone;
            @SerializedName("work_experince")
            @Expose
            private String workExperince;

            public String getProviderId() {
                return providerId;
            }

            public void setProviderId(String providerId) {
                this.providerId = providerId;
            }

            public String getProviderName() {
                return providerName;
            }

            public void setProviderName(String providerName) {
                this.providerName = providerName;
            }

            public String getBusinessEmail() {
                return businessEmail;
            }

            public void setBusinessEmail(String businessEmail) {
                this.businessEmail = businessEmail;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public Object getFacebookId() {
                return facebookId;
            }

            public void setFacebookId(Object facebookId) {
                this.facebookId = facebookId;
            }

            public String getGoogleId() {
                return googleId;
            }

            public void setGoogleId(String googleId) {
                this.googleId = googleId;
            }

            public Object getAppleId() {
                return appleId;
            }

            public void setAppleId(Object appleId) {
                this.appleId = appleId;
            }

            public String getProfileImage() {
                return profileImage;
            }

            public void setProfileImage(String profileImage) {
                this.profileImage = profileImage;
            }

            public String getProfilePicServer() {
                return profilePicServer;
            }

            public void setProfilePicServer(String profilePicServer) {
                this.profilePicServer = profilePicServer;
            }

            public Object getLatitude() {
                return latitude;
            }

            public void setLatitude(Object latitude) {
                this.latitude = latitude;
            }

            public Object getLongitude() {
                return longitude;
            }

            public void setLongitude(Object longitude) {
                this.longitude = longitude;
            }

            public Object getAddress() {
                return address;
            }

            public void setAddress(Object address) {
                this.address = address;
            }

            public String getCreationTime() {
                return creationTime;
            }

            public void setCreationTime(String creationTime) {
                this.creationTime = creationTime;
            }

            public String getDisable() {
                return disable;
            }

            public void setDisable(String disable) {
                this.disable = disable;
            }

            public String getDeviceToken() {
                return deviceToken;
            }

            public void setDeviceToken(String deviceToken) {
                this.deviceToken = deviceToken;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getDisableNotification() {
                return disableNotification;
            }

            public void setDisableNotification(String disableNotification) {
                this.disableNotification = disableNotification;
            }

            public String getAccountId() {
                return accountId;
            }

            public void setAccountId(String accountId) {
                this.accountId = accountId;
            }

            public String getStripeAccountStatus() {
                return stripeAccountStatus;
            }

            public void setStripeAccountStatus(String stripeAccountStatus) {
                this.stripeAccountStatus = stripeAccountStatus;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getWorkExperince() {
                return workExperince;
            }

            public void setWorkExperince(String workExperince) {
                this.workExperince = workExperince;
            }

        }
    }
}
