package com.leedgo.app.models;

public class AllSupportMessageModel {
    private String id;

    private String user_id;

    private String role;

    private String ticket_no;

    private String message;

    private String creation_time;

    private String category_id;

    private String subcategory_id;

    private String category_name;

    private String icon_img;

    private String sub_category_name;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setUser_id(String user_id){
        this.user_id = user_id;
    }
    public String getUser_id(){
        return this.user_id;
    }
    public void setRole(String role){
        this.role = role;
    }
    public String getRole(){
        return this.role;
    }
    public void setTicket_no(String ticket_no){
        this.ticket_no = ticket_no;
    }
    public String getTicket_no(){
        return this.ticket_no;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setCreation_time(String creation_time){
        this.creation_time = creation_time;
    }
    public String getCreation_time(){
        return this.creation_time;
    }
    public void setCategory_id(String category_id){
        this.category_id = category_id;
    }
    public String getCategory_id(){
        return this.category_id;
    }
    public void setSubcategory_id(String subcategory_id){
        this.subcategory_id = subcategory_id;
    }
    public String getSubcategory_id(){
        return this.subcategory_id;
    }
    public void setCategory_name(String category_name){
        this.category_name = category_name;
    }
    public String getCategory_name(){
        return this.category_name;
    }
    public void setIcon_img(String icon_img){
        this.icon_img = icon_img;
    }
    public String getIcon_img(){
        return this.icon_img;
    }
    public void setSub_category_name(String sub_category_name){
        this.sub_category_name = sub_category_name;
    }
    public String getSub_category_name(){
        return this.sub_category_name;
    }

}
