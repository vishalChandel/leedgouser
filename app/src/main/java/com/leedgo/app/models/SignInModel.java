package com.leedgo.app.models;

import com.google.gson.annotations.SerializedName;

public class SignInModel{

	@SerializedName("data")
	private SignInData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(SignInData data){
		this.data = data;
	}

	public SignInData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"SignInModel{" +
			"data = '" + data + '\'' +
			",message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}