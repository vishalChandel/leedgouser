package com.leedgo.app.models.locations;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PredictionsItem{

	@SerializedName("reference")
	private String reference;



	private String showCityState = "";
	private String showActualCityState = "";

	@SerializedName("types")
	private ArrayList<String> types;

	@SerializedName("matched_substrings")
	private ArrayList<MatchedSubstringsItem> matchedSubstrings;

	@SerializedName("terms")
	private ArrayList<TermsItem> terms;

	@SerializedName("structured_formatting")
	private StructuredFormatting structuredFormatting;

	@SerializedName("description")
	private String description;

	@SerializedName("place_id")
	private String placeId;

	private boolean isSelection = false;

	public String getShowCityState() {
		return showCityState;
	}

	public String getShowActualCityState() {
		return showActualCityState;
	}

	public void setShowActualCityState(String showActualCityState) {
		this.showActualCityState = showActualCityState;
	}

	public void setShowCityState(String showCityState) {
		this.showCityState = showCityState;
	}

	public boolean isSelection() {
		return isSelection;
	}

	public void setSelection(boolean selection) {
		isSelection = selection;
	}

	public void setReference(String reference){
		this.reference = reference;
	}

	public String getReference(){
		return reference;
	}

	public void setTypes(ArrayList<String> types){
		this.types = types;
	}

	public ArrayList<String> getTypes(){
		return types;
	}

	public void setMatchedSubstrings(ArrayList<MatchedSubstringsItem> matchedSubstrings){
		this.matchedSubstrings = matchedSubstrings;
	}

	public ArrayList<MatchedSubstringsItem> getMatchedSubstrings(){
		return matchedSubstrings;
	}

	public void setTerms(ArrayList<TermsItem> terms){
		this.terms = terms;
	}

	public ArrayList<TermsItem> getTerms(){
		return terms;
	}

	public void setStructuredFormatting(StructuredFormatting structuredFormatting){
		this.structuredFormatting = structuredFormatting;
	}

	public StructuredFormatting getStructuredFormatting(){
		return structuredFormatting;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setPlaceId(String placeId){
		this.placeId = placeId;
	}

	public String getPlaceId(){
		return placeId;
	}

	@Override
 	public String toString(){
		return
			"PredictionsItem{" +
			"reference = '" + reference + '\'' +
			",types = '" + types + '\'' +
			",matched_substrings = '" + matchedSubstrings + '\'' +
			",terms = '" + terms + '\'' +
			",structured_formatting = '" + structuredFormatting + '\'' +
			",description = '" + description + '\'' +
			",place_id = '" + placeId + '\'' +
			"}";
		}

}