package com.leedgo.app.models;

import com.google.gson.annotations.SerializedName;

public class AddLocationModel{

	@SerializedName("id")
	private int id;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AddLocationModel{" + 
			"id = '" + id + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}