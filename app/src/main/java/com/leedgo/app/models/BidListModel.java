package com.leedgo.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BidListModel {

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<Data> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<Data> data){
        this.data = data;
    }
    public List<Data> getData(){
        return this.data;
    }
    public class Data
    {
        private String bid_id;

        private String provider_name;

        private String amount;

        private String time;

        public void setBid_id(String bid_id){
            this.bid_id = bid_id;
        }
        public String getBid_id(){
            return this.bid_id;
        }
        public void setProvider_name(String provider_name){
            this.provider_name = provider_name;
        }
        public String getProvider_name(){
            return this.provider_name;
        }
        public void setAmount(String amount){
            this.amount = amount;
        }
        public String getAmount(){
            return this.amount;
        }
        public void setTime(String time){
            this.time = time;
        }
        public String getTime(){
            return this.time;
        }
    }
}
