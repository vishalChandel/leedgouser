package com.leedgo.app.interfaces;


import com.leedgo.app.models.locations.PredictionsItem;

public interface LocationInterface {
    void getSelectedLocation(PredictionsItem mModel);
}
