package com.leedgo.app.interfaces;

import com.leedgo.app.models.GetClientLocationData;

public interface PrimaryLocationInterface {
    public void getPrimaryLocation(GetClientLocationData mModel, int position);
}
