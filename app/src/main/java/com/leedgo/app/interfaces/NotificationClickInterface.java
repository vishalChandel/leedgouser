package com.leedgo.app.interfaces;

import com.leedgo.app.models.NotificationModel;

public interface NotificationClickInterface {
    void getNotification(NotificationModel.Data mModel);
}
