package com.leedgo.app.interfaces;

import com.leedgo.app.models.CardData;

public interface PrimaryCardInterface {
    public void getPrimaryCard(CardData mModel);

}
