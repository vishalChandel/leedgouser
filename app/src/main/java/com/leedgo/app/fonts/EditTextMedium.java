package com.leedgo.app.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by Dharmani Apps on 1/18/2018.
 */


/*
 * EditText with Custom font family
 * */
public class EditTextMedium extends EditText {
    /*
     * Getting Current Class Name
     * */
    private String mTag = EditTextMedium.this.getClass().getSimpleName();

    /*
     * Constructor with
     * #Context
     * */
    public EditTextMedium(Context context) {
        super(context);
        applyCustomFont(context);
    }

    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */
    public EditTextMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */
    public EditTextMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }


    /*
     * Apply font.
     * */
    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new AvenirMedium(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
