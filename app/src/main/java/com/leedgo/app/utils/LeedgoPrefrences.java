package com.leedgo.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class LeedgoPrefrences {

    /***************
     * Define Shared Preferences Keys & MODE
     ****************/
    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;

    /*
     * Keys
     * */

    public static final String IS_LOGIN = "is_login";
    public static final String CARD_ADD_STATUS = "card_add_status";
    public static final String USER_ID = "user_id";
    public static final String AUTH_KEY = "auth_key";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PROFILE_PIC = "user_profile_pic";
    public static final String REMEMBER_CREDENTIALS = "remember_credentials";
    public static final String REMEMBER_EMAIL = "remember_email";
    public static final String REMEMBER_PASSWORD = "remeber_password";
    public static final String IS_INTRO_SCREENS = "is_intro_screes";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String USER_LOCATIONS = "user_location";
    public static final String CURRENT_LOCATION_LATITUDE = "current_location_latitude";
    public static final String CURRENT_LOCATION_LONGITUDE = "current_location_longitude";

    public static final String SCROLL = "scroll";
    public static final String ROOM_ID = "room_id";
    public static final String TICKET_ID = "ticket_id";
    public static final String CELL_NO = "Phone";
    public static final String CLIENT_LOCTION_NEW_ID = "client_location_new_id";
    public static final String EDITORADDLOCATION = "edit_or_add_location";
    public static final String ENABLE_PUSH = "enable_push";

    /*
     * Write the Boolean Value
     * */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /*
     * Read the Boolean Value
     * */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /*
     * Write the Integer Value
     * */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();

    }

    /*
     * Read the Interger Value
     * */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /*
     * Write the String Value
     * */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();

    }

    /*
     * Read the String Value
     * */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /*
     * Write the Float Value
     * */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /*
     * Read the Float Value
     * */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /*
     * Write the Long Value
     * */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /*
     * Read the Long Value
     * */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}
