package com.leedgo.app.utils;

public class Constants {

    //Staging Server Url
  // public static final String BASE_URL = "https://www.dharmani.com/solidittechnologies/solidwebservice/";

    //Production Server Url
    public static final String BASE_URL = "https://dev.leedgo.com/solidwebservice/";
    //Google Places Apis
    public static final String GOOGLE_PLACES_SEARCH = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
    public static final String GOOGLE_PLACES_LAT_LONG= "https://maps.googleapis.com/maps/api/place/details/json?place_id=";

    //Working Socket URL
    public static final String SocketURL = "http://jaohar-uk.herokuapp.com:80";
    /*
     * Constants @params
     * */
    public static int REQUEST_CODE = 123;
    public static String MODEL = "model";
    public static String ID = "id";
    public static String LIST = "list";

    public static String LINK_TYPE = "link_type";
    public static String PRIVACY_POLICY = "privacy_policy";
    public static String TERMS_SERVICES = "terms_condtions";
    public static String DONT_SELL_DATA = "dont_sell_my_data";
    public static String ABOUT_US = "about_us";
    public static String TOKEN = "stripe_token";
    public static String CARD_NO = "CARDnO";
    public static String NOTIFICATION_TYPE = "notification_type";
    public static String PUSH = "push";

    /*
     * App Links
     * */
    public static final String PRIVACY_POLICY_LINK = "";
    public static final String TERMS_SERVICES_LINK = "";


    public static String publishable_key="pk_test_WFMzQw8uWTQ6PqMDnAbQV2lv00hvuv1zn3";
}
