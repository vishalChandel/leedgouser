package com.leedgo.app.activities;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.leedgo.app.R;
import com.leedgo.app.models.AddLocationModel;
import com.leedgo.app.models.LocationChangeModel;
import com.leedgo.app.models.ServiceAreaModel;
import com.leedgo.app.models.SignInModel;
import com.leedgo.app.models.UpdatePriLocationModel;
import com.leedgo.app.models.locations.DeleteLocationModel;
import com.leedgo.app.models.locations.EditLocationModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;
import com.leedgo.app.utils.EasyLocationProvider;
import com.leedgo.app.utils.LeedgoPrefrences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_TERRAIN;


public class AddLocationsActivity extends BaseActivity implements OnMapReadyCallback {
    /************************
     *Fused Google Location
     **************/
    public static final int REQUEST_PERMISSION_CODE = 919;
    public String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    public String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    /*
     * Initalize
     * */
    EasyLocationProvider easyLocationProvider; //Declare Global Variable
    /**
     * Getting the Current Class Name
     */
    String TAG = AddLocationsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AddLocationsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.btnSaveB)
    Button btnSaveB;
    @BindView(R.id.btnSaveB2Final)
    Button btnSaveBFinal;
    @BindView(R.id.txtSrchedLocTV)
    TextView txtSrchedLocTV;
    @BindView(R.id.viewP)
    View viewP;
    @BindView(R.id.editLocNameET)
    EditText editLocNameET;
    String mLocationID = "";
    String client_location_id = "";
    public static String new_clientLocid = "";

    SupportMapFragment mapFragment;
    String confirmAddress = "";
    String confirmLatitude = "";
    String confirmLongitude = "";
    String confirmCity = "";
    String confirmZipcode = "";
    String confirmState = "";
    private long mLastClickTab1 = 0;

    /*
     * Initialize DataTypes
     * */
    ServiceAreaModel mServiceAreaModel;
    LocationChangeModel locationChangeModel;
    /*
     * Activity Override method
     * #onActivityCreated
     * */
    private GoogleMap mGoogleMap;
    public static String locationType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_locations);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, "Add Location");
        updateLocation();
        setUpGoogleMap();
        if (getIntent() != null) {
            try {
                if (getIntent().getStringExtra("Check") != null) {
                    if (getIntent().getStringExtra("Check").equals("confirm")) {
                        setToolbarText(txtHeaderTV, "Add Location");
                        btnSaveB.setVisibility(View.GONE);
                        btnSaveBFinal.setVisibility(View.VISIBLE);
                        editLocNameET.setVisibility(View.GONE);
                        viewP.setVisibility(View.VISIBLE);

                        if (!getIntent().getStringExtra("longitude").equals("")) {
                            confirmLongitude = getIntent().getStringExtra("longitude");
                            confirmLatitude = getIntent().getStringExtra("latitude");
                            confirmAddress = getIntent().getStringExtra("address");
                            confirmCity = getIntent().getStringExtra("city");
                            confirmState = getIntent().getStringExtra("state");
                        }
                        if (!getIntent().getStringExtra("zipcode").equals("")) {
                            confirmZipcode = getIntent().getStringExtra("zipcode");
                        }
                    } else {
                        viewP.setVisibility(View.GONE);
                        editLocNameET.setVisibility(View.VISIBLE);
                    }
                }


//                locationChangeModel = (LocationChangeModel) getIntent().getSerializableExtra(Constants.MODEL);
                client_location_id = getIntent().getStringExtra("id");
                locationType = getIntent().getStringExtra("locationtype");
                if (locationType.equals("edit")) {
                    confirmLongitude = getIntent().getStringExtra("longitude");
                    confirmLatitude = getIntent().getStringExtra("latitude");
                    confirmAddress = getIntent().getStringExtra("location");
                    confirmState = getIntent().getStringExtra("state");
                    confirmZipcode = getIntent().getStringExtra("zipcode");
                }

            } catch (Exception e) {
                Log.e(TAG, "exception::" + e.toString());
            }
        }

    }


    private void setUpGoogleMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(MAP_TYPE_NORMAL);
        //   mGoogleMap.setMaxZoomPreference(80);
        //Custom Theme
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style_custom);
        mGoogleMap.setMapStyle(style);
        if (getCurrentLatitude() != null && getCurrentLatitude().length() > 0) {

            if (client_location_id != null) {
                try {
                    editLocNameET.setText(getIntent().getStringExtra("title"));
                    txtSrchedLocTV.setText(getIntent().getStringExtra("location"));
                    setToolbarText(txtHeaderTV, "Edit the Location");
                    setGoogleMap(Double.parseDouble(getIntent().getStringExtra("latitude")), Double.parseDouble(getIntent().getStringExtra("longitude")));
                } catch (Exception e) {
                    e.printStackTrace();
                    txtHeaderTV.setText("Add Location");
                    setGoogleMap(Double.parseDouble(getCurrentLatitude()), Double.parseDouble(getCurrentLongitude()));
                    Geocoder gCoder = new Geocoder(mActivity);
                    ArrayList<Address> addresses = null;
                    try {
                        addresses = (ArrayList<Address>) gCoder.getFromLocation(Double.parseDouble(getCurrentLatitude()), Double.parseDouble(getCurrentLongitude()), 1);

                        if (addresses != null && addresses.size() > 0) {
                            // Toast.makeText(myContext, "country: " + addresses.get(0).getCountryName(), Toast.LENGTH_LONG).show();
                            txtSrchedLocTV.setText(addresses.get(0).getAddressLine(0));
                            mServiceAreaModel = new ServiceAreaModel();
                            mServiceAreaModel.setAddress(addresses.get(0).getAddressLine(0));
                            mServiceAreaModel.setCity(addresses.get(0).getAdminArea());
                            mServiceAreaModel.setLatitude("" + addresses.get(0).getLatitude());
                            mServiceAreaModel.setLongitude("" + addresses.get(0).getLongitude());
                            mServiceAreaModel.setState(addresses.get(0).getSubAdminArea());
                            mServiceAreaModel.setZipcode(addresses.get(0).getPostalCode());


                            Log.e("checklp", addresses.get(0).toString());
                        }
                    } catch (Exception we) {
                        we.printStackTrace();
                    }

                }
            } else {
                if (confirmLongitude.equals("")) {

                    setGoogleMap(Double.parseDouble(getCurrentLatitude()), Double.parseDouble(getCurrentLongitude()));
                    Geocoder gCoder = new Geocoder(mActivity);
                    ArrayList<Address> addresses = null;
                    try {
                        addresses = (ArrayList<Address>) gCoder.getFromLocation(Double.parseDouble(getCurrentLatitude()), Double.parseDouble(getCurrentLongitude()), 1);

                        if (addresses != null && addresses.size() > 0) {
                            // Toast.makeText(myContext, "country: " + addresses.get(0).getCountryName(), Toast.LENGTH_LONG).show();
                            txtSrchedLocTV.setText(addresses.get(0).getAddressLine(0));
                            mServiceAreaModel = new ServiceAreaModel();
                            mServiceAreaModel.setAddress(addresses.get(0).getAddressLine(0));
                            mServiceAreaModel.setCity(addresses.get(0).getAdminArea());
                            mServiceAreaModel.setLatitude("" + addresses.get(0).getLatitude());
                            mServiceAreaModel.setLongitude("" + addresses.get(0).getLongitude());
                            mServiceAreaModel.setState(addresses.get(0).getSubAdminArea());
                            mServiceAreaModel.setZipcode(addresses.get(0).getPostalCode());


                            Log.e("checklp", addresses.get(0).toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    editLocNameET.setText(getIntent().getStringExtra("title"));
                    txtSrchedLocTV.setText(getIntent().getStringExtra("location"));
                    mServiceAreaModel = new ServiceAreaModel();
                    mServiceAreaModel.setAddress(confirmAddress);
                    mServiceAreaModel.setCity(confirmCity);
                    mServiceAreaModel.setLatitude(confirmLatitude);
                    mServiceAreaModel.setLongitude(confirmLongitude);
                    mServiceAreaModel.setState(confirmState);
                    mServiceAreaModel.setZipcode(confirmZipcode);


                    //txtSrchedLocTV.setText(confirmAddress);
                    setGoogleMap(Double.parseDouble(confirmLatitude), Double.parseDouble(confirmLongitude));

                }
            }

        } else {
            LatLng sydney = new LatLng(-33.852, 151.211);
            googleMap.addMarker(new MarkerOptions()
                    .position(sydney));
        }
    }

    /*
     * Widgets Click Listener
     * */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.backRL, R.id.btnSaveB, R.id.btnSaveB2Final, R.id.txtSrchedLocTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.btnSaveB:
//                btnSaveB.setEnabled(false);
                performSaveClick();
                break;
            case R.id.btnSaveB2Final:
                performSave2Click();
                break;
            case R.id.txtSrchedLocTV:
                performSearchAddClick();
                break;
        }
    }

    public void preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {

            return;
        }
        mLastClickTab1 = SystemClock.elapsedRealtime();
    }

    private void performSave2Click() {

        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSaveB.startAnimation(myAnim);

        if (isValidate2()) {
            if (isNetworkAvailable(mActivity))
                executeAddLocationRequest2();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void performSaveClick() {

        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSaveB.startAnimation(myAnim);

        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                if (getAddorEdit().equals("edit")) {
                    executeEditLocApi();
                } else {
                    executeAddLocationRequest();
                }

            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }


    private void performSearchAddClick() {
        new_clientLocid = client_location_id;
        Intent mIntent = new Intent(mActivity, SearchLocationActivity.class);
        startActivityForResult(mIntent, Constants.REQUEST_CODE);
    }


    private void updateLocation() {
        if (checkPermission()) {
            getLocationLatLong();
        } else {
            requestPermission();
        }
    }


    public void getLocationLatLong() {
        easyLocationProvider = new EasyLocationProvider.Builder(mActivity)
                .setInterval(3000)
                .setFastestInterval(1000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setListener(new EasyLocationProvider.EasyLocationCallback() {
                    @Override
                    public void onGoogleAPIClient(GoogleApiClient googleApiClient, String message) {
                        Log.e("EasyLocationProviderqw", "onGoogleAPIClient: " + message);
                    }

                    @Override
                    public void onLocationUpdated(double latitude, double longitude) {
                        Log.e("EasyLocationProvider", "onLocationUpdated:: " + "Latitude: " + latitude + " Longitude: " + longitude);
                    }


                    @Override
                    public void onLocationUpdateRemoved() {
                        Log.e("EasyLocationProvider", "onLocationUpdateRemoved");
                    }
                }).build();

        getLifecycle().addObserver(easyLocationProvider);

    }


    private void setGoogleMap(double mLatitude, double mLongitude) {
        Drawable circleDrawable = getResources().getDrawable(R.drawable.mapmarker222);
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);
        mGoogleMap.clear();
        mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(mLatitude, mLongitude))
                .icon(markerIcon)
        );
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mMap.setMapType(MAP_TYPE_NORMAL);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), 13));
            }
        });
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editLocNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_location));
            btnSaveB.setVisibility(View.VISIBLE);
            btnSaveBFinal.setVisibility(View.GONE);
            flag = false;
        } else if (txtSrchedLocTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_your_locations));
            btnSaveB.setVisibility(View.VISIBLE);
            btnSaveBFinal.setVisibility(View.GONE);
            flag = false;
        }
        return flag;
    }

    /*
     * Set up validations for fields
     * */
    private boolean isValidate2() {
        boolean flag = true;
        if (txtSrchedLocTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_your_locations));
            btnSaveB.setVisibility(View.GONE);
            btnSaveBFinal.setVisibility(View.VISIBLE);
            flag = false;
        }
        return flag;
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", getUserId());
        mMap.put("address", mServiceAreaModel.getAddress());
        mMap.put("latitude", mServiceAreaModel.getLatitude());
        mMap.put("longitude", mServiceAreaModel.getLongitude());
        if(editLocNameET.getText().toString().trim().equals("")){
            mMap.put("location_title","nullzxdsre");
        }else{
            mMap.put("location_title", editLocNameET.getText().toString().trim());
        }
        mMap.put("state", mServiceAreaModel.getState());
        mMap.put("zip_code", mServiceAreaModel.getZipcode());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeAddLocationRequest() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addClientLocationRequest(getAuthKey(), mParam()).enqueue(new Callback<AddLocationModel>() {
            @Override
            public void onResponse(Call<AddLocationModel> call, Response<AddLocationModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                AddLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*********
     * Support for Marshmallows Version
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    private boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);
         return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getLocationLatLong();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //requestPermission();

                }
                return;
            }

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == Constants.REQUEST_CODE) {
                mServiceAreaModel = (ServiceAreaModel) data.getSerializableExtra(Constants.MODEL);
                txtSrchedLocTV.setText(mServiceAreaModel.getAddress());
                setGoogleMap(Double.parseDouble(mServiceAreaModel.getLatitude()), Double.parseDouble(mServiceAreaModel.getLongitude()));
            }
        }
    }


    private void executeAddLocationRequest2() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addClientLocationRequest(getAuthKey(), mParam()).enqueue(new Callback<AddLocationModel>() {
            @Override
            public void onResponse(Call<AddLocationModel> call, Response<AddLocationModel> response) {
                //dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                AddLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mLocationID = String.valueOf(mModel.getId());
                    executePrimaryLocationRequest();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Execute api
     * */
    private Map<String, String> mUpdateParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", "" + mLocationID);
        mMap.put("primary_value", "1");
        mMap.put("client_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executePrimaryLocationRequest() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addPrimaryAddressRequest(getAuthKey(), mUpdateParam()).enqueue(new Callback<UpdatePriLocationModel>() {
            @Override
            public void onResponse(Call<UpdatePriLocationModel> call, Response<UpdatePriLocationModel> response) {
                dismissProgressDialog();

                UpdatePriLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UpdatePriLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private Map<String, String> mParamEdit() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", getEditLocatioId());
        mMap.put("client_id", getUserId());
        mMap.put("address", mServiceAreaModel.getAddress());
        mMap.put("latitude", mServiceAreaModel.getLatitude());
        mMap.put("longitude", mServiceAreaModel.getLongitude());
        mMap.put("location_title", editLocNameET.getText().toString().trim());
        mMap.put("state", mServiceAreaModel.getState());
        mMap.put("zip_code", mServiceAreaModel.getZipcode());
        return mMap;
    }

    private void executeEditLocApi() {

        Log.e("Dataq2", "edit **RESPONSE**" + mParamEdit().toString());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeEditLocationData(getAuthKey(), mParamEdit()).enqueue(new Callback<EditLocationModel>() {
            @Override
            public void onResponse(Call<EditLocationModel> call, Response<EditLocationModel> response) {

                Log.e("Dataq2", "**RESPONSE**" + response.body().toString());

                EditLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    client_location_id = "";
                    finish();
                    Log.e("Test", "Success");
                } else {
                        showAlertDialog(mActivity, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<EditLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }


}