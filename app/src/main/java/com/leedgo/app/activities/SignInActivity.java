package com.leedgo.app.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.leedgo.app.R;
import com.leedgo.app.models.CheckModel;
import com.leedgo.app.models.SignInModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;
import com.leedgo.app.utils.EasyLocationProvider;
import com.leedgo.app.utils.LeedgoPrefrences;

import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.InstanceIdResult;

public class SignInActivity extends BaseActivity {


    /************************
     *Fused Google Location
     **************/
    public static final int REQUEST_PERMISSION_CODE = 919;
    public String mAccessFineLocation = Manifest.permission.ACCESS_FINE_LOCATION;
    public String mAccessCourseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
    EasyLocationProvider easyLocationProvider;
    /*
    /**
     * Getting the Current Class Name
     */
    String TAG = SignInActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignInActivity.this;
    String google_id, gooogle_name, google_email, google_image;

    /*
     * Widgets
     * */
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.cbRememberMe)
    CheckBox cbRememberMe;
    @BindView(R.id.txtForgotPwdTV)
    TextView txtForgotPwdTV;
    @BindView(R.id.btnSignIn)
    Button btnSignIn;
    @BindView(R.id.btnGoogleB)
    Button btnGoogleB;
    @BindView(R.id.btnFacebookB)
    Button btnFacebookB;
    @BindView(R.id.txtSignupTV)
    TextView txtSignupTV;
    @BindView(R.id.txtTermsServicesTV)
    TextView txtTermsServicesTV;
    @BindView(R.id.txtPrivacyPolicyTV)
    TextView txtPrivacyPolicyTV;
    @BindView(R.id.txtDontSellTV)
    TextView txtDontSellTV;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 1;
    /*
     * Initialize...
     * */
    String strDeviceToken = "";
    private String fbEmail, fbLastName, fbFirstName, fbId, userName, fbSocialUserserName;
    String fb_Username = "";
    String facebook_id = "";
    String fb_email = "";
    String FacebookProfilePicture = "";
    private URL fbProfilePicture;
    CallbackManager mCallbackManager;

    String mNotification_Status = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        setCheckBoxTypeface();
        updateLocation();
        getDeviceToken();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void setRemembersDetails() {
        if (IsRememberMe() == true) {
            Log.e(TAG, "setRemembersDetails: " + getRemeberEmail());
            Log.e(TAG, "setRemembersDetails: " + getRemeberPassword());
            cbRememberMe.setChecked(true);
            editEmailET.setText(getRemeberEmail());
            editEmailET.setSelection(getRemeberEmail().length());
            editPasswordET.setText(getRemeberPassword());
            editPasswordET.setSelection(getRemeberPassword().length());
        } else {
            cbRememberMe.setChecked(false);
        }
    }

    private void setRememberCredentials() {
        if (cbRememberMe.isChecked() == true) {
            LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.REMEMBER_CREDENTIALS, true);
            LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_EMAIL, editEmailET.getText().toString().trim());
            LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_PASSWORD, editPasswordET.getText().toString().trim());
        } else {
            LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.REMEMBER_CREDENTIALS, false);
            LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_EMAIL, "");
            LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_PASSWORD, "");
        }
        cbRememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.REMEMBER_CREDENTIALS, true);
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_EMAIL, editEmailET.getText().toString().trim());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_PASSWORD, editPasswordET.getText().toString().trim());
                } else {
                    LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.REMEMBER_CREDENTIALS, false);
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_EMAIL, "");
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_PASSWORD, "");
                }
            }
        });
    }

    private void getDeviceToken() {
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if (!task.isSuccessful()) {
//                            return;
//                        }
//                        // Get new Instance ID token
//                        strDeviceToken = task.getResult().getToken();
//                        Log.e(TAG, "**Push Token**" + strDeviceToken);
//                    }
//                });

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    strDeviceToken = task.getResult();
                    Log.e(TAG, "**Push Token**" + strDeviceToken);

                });

    }

    private void setCheckBoxTypeface() {
        Typeface font = Typeface.createFromAsset(getAssets(), "AvenirMedium.otf");
        cbRememberMe.setTypeface(font);
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.txtForgotPwdTV, R.id.btnSignIn, R.id.btnGoogleB, R.id.btnFacebookB, R.id.txtSignupTV,
            R.id.txtTermsServicesTV, R.id.txtPrivacyPolicyTV, R.id.txtDontSellTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtForgotPwdTV:
                performForgotPwdClick();
                break;
            case R.id.btnSignIn:
                performSignInClick();
                break;
            case R.id.btnGoogleB:
                performGoogleLoginClick();
                break;
            case R.id.btnFacebookB:
                performFacebookLoginClick();
                break;
            case R.id.txtSignupTV:
                performSignUpClick();
                break;
            case R.id.txtTermsServicesTV:
                performTermServicesClick();
                break;
            case R.id.txtPrivacyPolicyTV:
                performPrivacyPolicyClick();
                break;
            case R.id.txtDontSellTV:
                performDontSellClick();
                break;
        }
    }

    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        }
        return flag;
    }

    private void performForgotPwdClick() {
        startActivity(new Intent(mActivity, ForgotPwdActivity.class));
    }

    private void performSignInClick() {
        setRememberCredentials();
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSignIn.startAnimation(myAnim);

        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeSignInApi();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }

    private void performGoogleLoginClick() {

        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnGoogleB.startAnimation(myAnim);

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void performFacebookLoginClick() {
        performFbLogin();
    }

    private void performSignUpClick() {
        startActivity(new Intent(mActivity, SignUpActivity.class));
        finish();
    }

    private void performTermServicesClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.TERMS_SERVICES);
        startActivity(mIntent);
    }

    private void performPrivacyPolicyClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.PRIVACY_POLICY);
        startActivity(mIntent);
    }

    private void performDontSellClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.DONT_SELL_DATA);
        startActivity(mIntent);
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("userinput", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("device_token", strDeviceToken);
        mMap.put("device_type", "ANDROID");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSignInApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signInRequest(mParam()).enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignInModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.IS_LOGIN, true);
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_ID, mModel.getData().getId());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.AUTH_KEY, mModel.getData().getAuthKey());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_NAME, mModel.getData().getName());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_EMAIL, mModel.getData().getEmail());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_PROFILE_PIC, mModel.getData().getProfilePic());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LATITUDE, mModel.getData().getLatitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LONGITUDE, mModel.getData().getLongitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_LOCATIONS, mModel.getData().getLongitude());

                    if (mModel.getData().getDisableNotification() != null && !mModel.getData().getDisableNotification().equals("")) {
                        mNotification_Status = mModel.getData().getDisableNotification();
                    }

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }

                /* execute get notification status */
                executeNotificationApi();
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mCallbackManager!=null){
            Log.e("Check", "2");
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignIn.getClient(mActivity,
                    new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()
            ).signOut();
            Log.e("Check", "3");
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {

                Log.e("Check", "4");
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                if (result.isSuccess()) {
                    // Google Sign In was successful, save Token and a state then authenticate with Firebase
                    GoogleSignInAccount account1 = result.getSignInAccount();
                    gooogle_name = account1.getDisplayName();
                    google_email = account1.getEmail();
                    google_id = account1.getId();
                    Uri photoUri = account1.getPhotoUrl();
                    google_image = photoUri.toString();
                    Log.e("Check", "40" + account1.getId() + " token:" + account1.getIdToken());
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    //  Toast.makeText(mActivity, "Successssghsvs ", Toast.LENGTH_SHORT).show();
                    // firebaseAuthWithGoogle(account);
                    executeGoogleSignInApi();
                } else {
                    Log.e("Check", "41");
                }
            } catch (ApiException e) {

            }
        }
    }

    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", google_email);
        mMap.put("username", gooogle_name);
        mMap.put("google_id", google_id);
        mMap.put("google_image", google_image);
        mMap.put("device_token", strDeviceToken);
        mMap.put("device_type", "ANDROID");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGoogleSignInApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signInGoogleRequest(mParams2()).enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                dismissProgressDialog();
                executeNotificationApi();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignInModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, "Signin Successfully");
                    LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.IS_LOGIN, true);
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_ID, mModel.getData().getId());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.AUTH_KEY, mModel.getData().getAuthKey());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_NAME, mModel.getData().getName());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_EMAIL, mModel.getData().getEmail());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_PROFILE_PIC, mModel.getData().getProfilePic());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LATITUDE, mModel.getData().getLatitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LONGITUDE, mModel.getData().getLongitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_LOCATIONS, mModel.getData().getLongitude());

                    if (mModel.getData().getDisableNotification() != null && !mModel.getData().getDisableNotification().equals("")) {
                        mNotification_Status = mModel.getData().getDisableNotification();
                    }

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private Map<String, String> mParam2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        mMap.put("user_type", "client");
        mMap.put("notification_status", mNotification_Status);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeNotificationApi() {
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getNotification(mParam2()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {


                CheckModel model;
                model = response.body();
                if (model.getStatus() == 1) {
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.ENABLE_PUSH, mNotification_Status);

                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    private void updateLocation() {
        if (checkPermission()) {
            getLocationLatLong();
        } else {
            requestPermission();
        }
    }

    /*********
     * Support for Marshmallows Version
     * 1) ACCESS_FINE_LOCATION
     * 2) ACCESS_COARSE_LOCATION
     **********/
    private boolean checkPermission() {
        int mlocationFineP = ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation);
        int mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation);
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{mAccessFineLocation, mAccessCourseLocation}, REQUEST_PERMISSION_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getLocationLatLong();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //requestPermission();

                }
                return;
            }

        }
    }

    public void getLocationLatLong() {
        easyLocationProvider = new EasyLocationProvider.Builder(mActivity)
                .setInterval(3000)
                .setFastestInterval(1000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setListener(new EasyLocationProvider.EasyLocationCallback() {
                    @Override
                    public void onGoogleAPIClient(GoogleApiClient googleApiClient, String message) {
                        Log.e("EasyLocationProvider", "onGoogleAPIClient: " + message);
                    }

                    @Override
                    public void onLocationUpdated(double latitude, double longitude) {
                        Log.e("EasyLocationProvider", "onLocationUpdated:: " + "Latitude: " + latitude + " Longitude: " + longitude);
                    }

                    @Override
                    public void onLocationUpdateRemoved() {
                        Log.e("EasyLocationProvider", "onLocationUpdateRemoved");
                    }
                }).build();

        getLifecycle().addObserver(easyLocationProvider);

    }

    //fb
    //fb Login
    private void performFbLogin() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            FacebookSdk.sdkInitialize(getApplicationContext());
            LoginManager.getInstance().logOut();
            AppEventsLogger.activateApp(getApplication());
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
            mCallbackManager = CallbackManager.Factory.create();
            loginWithFacebook();
        }

    }

    private void loginWithFacebook() {

        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"));
            LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d("onSuccess: ", loginResult.getAccessToken().getToken());
                    getFacebookData(loginResult);
                }

                @Override
                public void onCancel() {
                    // Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException error) {
                     Toast.makeText(mActivity, error.toString(), Toast.LENGTH_SHORT).show();
                    Log.e("error", "is" + error.toString());
                }

            });
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    private void getFacebookData(LoginResult loginResult) {
        showProgressDialog(mActivity);
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                dismissProgressDialog();
                try {

                    if (object.has("id")) {
                        fbId = object.getString("id");
                        Log.e("LoginActivity", "id" + fbId);

                    }
                    //check permission first userName
                    if (object.has("first_name")) {
                        fbFirstName = object.getString("first_name");
                        Log.e("LoginActivity", "first_name" + fbFirstName);

                    }
                    //check permisson last userName
                    if (object.has("last_name")) {
                        fbLastName = object.getString("last_name");
                        Log.e("LoginActivity", "last_name" + fbLastName);
                    }
                    //check permisson email
                    if (object.has("email")) {
                        fbEmail = object.getString("email");
                        Log.e("LoginActivity", "email" + fbEmail);
                    }

                    fbSocialUserserName = fbFirstName + " " + fbLastName;

                    JSONObject jsonObject = new JSONObject(object.getString("picture"));
                    if (jsonObject != null) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        Log.e("Loginactivity", "json oject get picture" + dataObject);
                        fbProfilePicture = new URL("https://graph.facebook.com/" + fbId + "/picture?width=500&height=500");
                        Log.e("LoginActivity", "json object=>" + object.toString());
                    }

                    fb_Username = fbSocialUserserName;
                    fb_email = fbEmail;
                    facebook_id = fbId;

                    if (fbProfilePicture != null) {
                        FacebookProfilePicture = String.valueOf(fbProfilePicture);

                    } else {
                        FacebookProfilePicture = "";
                    }


                    executeLoginWithFbApi();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle bundle = new Bundle();
        Log.e("LoginActivity", "bundle set");
        bundle.putString("fields", "id, first_name, last_name,email,picture,gender,location");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();

    }

    private Map<String, String> mParams3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("username", fb_Username);
        mMap.put("email", fb_email);
        mMap.put("facebook_id", fbId);
        mMap.put("device_type",  "android");
        mMap.put("device_token",strDeviceToken);
        mMap.put("facebook_image",FacebookProfilePicture);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLoginWithFbApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.facebookRequest(mParams3()).enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignInModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    showToast(mActivity, "Signin Successfully");
                    LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.IS_LOGIN, true);
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_ID, mModel.getData().getId());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.AUTH_KEY, mModel.getData().getAuthKey());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_NAME, mModel.getData().getName());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_EMAIL, mModel.getData().getEmail());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_PROFILE_PIC, mModel.getData().getProfilePic());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LATITUDE, mModel.getData().getLatitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LONGITUDE, mModel.getData().getLongitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_LOCATIONS, mModel.getData().getLongitude());
                    if (mModel.getData().getDisableNotification() != null && !mModel.getData().getDisableNotification().equals("")) {
                        mNotification_Status = mModel.getData().getDisableNotification();
                    }
                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRemembersDetails();
    }
}