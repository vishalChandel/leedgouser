package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.leedgo.app.R;
import com.leedgo.app.adapters.CardListAdapter;
import com.leedgo.app.adapters.SearchLocationAdapter;
import com.leedgo.app.models.CardDetailModel;
import com.leedgo.app.models.CheckModel;
import com.leedgo.app.models.UpdatePriLocationModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity2 extends BaseActivity {

    Activity mActivity = PaymentActivity2.this;

    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.llyVisa)
    LinearLayout llyVisa;
    @BindView(R.id.llyMasterCard)
    LinearLayout llyMastercard;
    @BindView(R.id.llyPayPal)
    LinearLayout llyPayPal;
    @BindView(R.id.edCardHoldername)
    EditText edCardHolderName;
    @BindView(R.id.edCardNumber)
    EditText edCardNumber;
    @BindView(R.id.edCvvnumber)
    EditText edCvvnumber;
    @BindView(R.id.txCardNumber)
    TextView txCardNumber;
    @BindView(R.id.txExpirationDate)
    TextView txExpirationDate;
    @BindView(R.id.btnNextTV)
    Button btnSave;
    @BindView(R.id.cardTypeRV)
    RecyclerView cardTypeRV;

    String card_id="",last_four_digits;
    String cardNumber = "", cvc = "";
    int expirayYear;
    int expiraydate;
    String tokenId = "";
    String expMonth;
    Card card;
    String update_cardornot;
    String cardBrand, cardYear, cardMonth, cardHoldername, cardLstnumber, updateCardId;
    String cardnumber;
    ArrayList<Integer> mCardImagesAL = new ArrayList<>();
    private String mCardHolderName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment2);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, "Payment");
        mCardImagesAL.add(R.drawable.ic_mastercard);
        mCardImagesAL.add(R.drawable.ic_visa);
        mCardImagesAL.add(R.drawable.ic_paypal);
        mCardImagesAL.add(R.drawable.american_card);
        mCardImagesAL.add(R.drawable.discover_card);
        mCardImagesAL.add(R.drawable.other_card);
        setAdapter();
        executeCardRequest();

        edCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    String d = editable.toString();
                    char firstChar = d.charAt(0);
                    if (firstChar == '4') {
                        VisaClick();
                    } else if (firstChar == '5') {
                        MasterClick();
                    } else {
                        PayPalClick();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(card_id!="") {
                        Intent intent = new Intent(PaymentActivity2.this, PaymentActivity1.class);
                        intent.putExtra(Constants.TOKEN, card_id);
                        intent.putExtra(Constants.CARD_NO, cardnumber);
                        startActivity(intent);
                    }else {
                        GetCardCheck();
                    }
                }
            });

        txCardNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                card_id="";
                txCardNumber.setVisibility(View.GONE);
                edCardNumber.setVisibility(View.VISIBLE);
                edCvvnumber.setText("");
                edCardNumber.setText("");
                edCardHolderName.setText("");
                txExpirationDate.setText("");
            }
        });
    }

    @OnClick({R.id.backRL, R.id.llyVisa, R.id.llyMasterCard, R.id.llyPayPal, R.id.txExpirationDate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.llyVisa:
                VisaClick();
                break;
            case R.id.llyMasterCard:
                MasterClick();
                break;
            case R.id.llyPayPal:
                PayPalClick();
                break;
            case R.id.txExpirationDate:
                txExpirationDate();
                break;

        }
    }

    private void GetCardCheck() {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        btnSave.startAnimation(myAnim);
            GetCardData();
    }

    private void GetCardData() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                String expDateYear = txExpirationDate.getText().toString().trim();
                String value[] = expDateYear.split("/");
                String expirayDate = value[0];
                String expiraryYear = value[1];
                cardNumber = edCardNumber.getText().toString().trim();
//                expirayYear = Integer.parseInt(expirayDate);
//                expiraydate = Integer.parseInt(expiraryYear);
                mCardHolderName=edCardHolderName.getText().toString();
                cvc = edCvvnumber.getText().toString().trim();
                card = new Card(cardNumber, Integer.valueOf(expiraydate), Integer.valueOf(expirayYear), cvc,mCardHolderName,"","","","","","","");
                CreateToken(card);
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }

        }
    }

    public void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        CardListAdapter mCardListAdapter = new CardListAdapter(mActivity, mCardImagesAL);
        cardTypeRV.setLayoutManager(layoutManager);
        cardTypeRV.setAdapter(mCardListAdapter);
    }

//validations

    private boolean isValidate() {
        boolean flag = true;
        if (edCardHolderName.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter name on Card");
            flag = false;
        } else if (edCardNumber.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter Card number");
            flag = false;
        } else if (edCardNumber.getText().toString().trim().length() < 15) {
            showAlertDialog(mActivity, "Please enter valid Card number");
            flag = false;
        } else if (edCvvnumber.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter CVV number");
            flag = false;
        } else if (edCvvnumber.getText().toString().trim().length() < 3) {
            showAlertDialog(mActivity, "Please enter valid CVV number");
            flag = false;
        } else if (txExpirationDate.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter card expiration date");
            flag = false;
        }
        return flag;
    }


    private void saveCard() {
        cardNumber = edCardNumber.getText().toString().trim();

        cvc = edCvvnumber.getText().toString().trim();

        card = new Card(cardNumber, Integer.valueOf(expiraydate), Integer.valueOf(expirayYear), cvc);
        if (card == null) {
            showAlertDialog(mActivity, getString(R.string.The_card_invalid));
        } else {
            if (!card.validateCard()) {
                // Do not continue token creation.
                showAlertDialog(mActivity, getString(R.string.The_card_invalid));
            } else {
                saveData();
                //               CreateToken(card);
            }
        }
    }

    /*
     * To Get Token
     **/

    private void CreateToken(Card card) {
        showProgressDialog(mActivity);
        Stripe stripe = null;
        try {
            stripe = new Stripe(Constants.publishable_key);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }

        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        dismissProgressDialog();
                        // Send token to your server
                        Log.e("Stripe Token", token.toString());
                        tokenId = token.getId();
                        Intent intent = new Intent(PaymentActivity2.this, PaymentActivity1.class);
                        intent.putExtra(Constants.TOKEN, tokenId);
                        intent.putExtra(Constants.CARD_NO, edCardNumber.getText().toString().trim());
                        startActivity(intent);
                        // saveCard();
                    }

                    public void onError(Exception error) {

                        showAlertDialog(mActivity, String.valueOf(error));
                        Log.e(TAG, "VALUE::" + error);
                        dismissProgressDialog();

                    }
                }
        );
    }

    @SuppressLint("ResourceAsColor")
    private void txExpirationDate() {
        try {
            InputMethodManager manager
                    = (InputMethodManager)
                    getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            manager
                    .hideSoftInputFromWindow(
                            txExpirationDate.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance(Locale.getDefault());

        calendar.set(Calendar.HOUR_OF_DAY, Calendar.HOUR);
        calendar.set(Calendar.MINUTE, Calendar.MINUTE);

        final Date defaultDate = calendar.getTime();
        Date currentDate = new Date(System.currentTimeMillis() - 1000);
        new SingleDateAndTimePickerDialog.Builder(this)
                .bottomSheet()
                .curved()
                .mainColor(R.color.colorGreenDark)
                .defaultDate(defaultDate)
                .minDateRange(currentDate)
                .displayMinutes(false)
                .displayHours(false)
                .displayDays(false)
                .displayMonth(true)
                .displayYears(true)
                .displayDaysOfMonth(false)
                // .display()
                .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                    @Override
                    public void onDisplayed(SingleDateAndTimePicker picker) {

                    }
                })
                .title(" Expiry Date  ")
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                SimpleDateFormat DateFor11 = new SimpleDateFormat("EEE d MMM HH:mm", Locale.getDefault());
                                String s = DateFor11.format(date);
                                // String s=date;

                                SimpleDateFormat DateFor12 = new SimpleDateFormat("MM/yyyy");
                                SimpleDateFormat DateFor13 = new SimpleDateFormat("yyyy");
                                SimpleDateFormat DateFor14 = new SimpleDateFormat("MM");
                                String stringDate = DateFor12.format(date);
                                txExpirationDate.setText(stringDate);
                                expiraydate = Integer.parseInt(DateFor14.format(date));
                                expirayYear = Integer.parseInt(DateFor13.format(date));
                                //  tx_appointmentbook.setText("Submit");

                            }
                        }, 500);
                    }
                }).display();

    }

    private void MasterClick() {
        llyMastercard.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext_green));
        llyPayPal.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext));
        llyVisa.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext));
    }

    private void PayPalClick() {
        llyMastercard.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext));
        llyPayPal.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext_green));
        llyVisa.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext));
    }

    private void VisaClick() {
        llyMastercard.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext));
        llyPayPal.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext));
        llyVisa.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext_green));

    }

    // Return true if the card number is valid
    public static boolean validitychk(long cnumber) {
        return (thesize(cnumber) >= 13 && thesize(cnumber) <= 16) && (prefixmatch(cnumber, 4)
                || prefixmatch(cnumber, 5) || prefixmatch(cnumber, 37) || prefixmatch(cnumber, 6))
                && ((sumdoubleeven(cnumber) + sumodd(cnumber)) % 10 == 0);
    }

    // Get the result from Step 2
    public static int sumdoubleeven(long cnumber) {
        int sum = 0;
        String num = cnumber + "";
        for (int i = thesize(cnumber) - 2; i >= 0; i -= 2)
            sum += getDigit(Integer.parseInt(num.charAt(i) + "") * 2);
        return sum;
    }

    // Return this cnumber if it is a single digit, otherwise,
    // return the sum of the two digits
    public static int getDigit(int cnumber) {
        if (cnumber < 9)
            return cnumber;
        return cnumber / 10 + cnumber % 10;
    }

    // Return sum of odd-place digits in cnumber
    public static int sumodd(long cnumber) {
        int sum = 0;
        String num = cnumber + "";
        for (int i = thesize(cnumber) - 1; i >= 0; i -= 2)
            sum += Integer.parseInt(num.charAt(i) + "");
        return sum;
    }

    // Return true if the digit d is a prefix for cnumber
    public static boolean prefixmatch(long cnumber, int d) {
        return getprefx(cnumber, thesize(d)) == d;
    }

    // Return the number of digits in d
    public static int thesize(long d) {
        String num = d + "";
        return num.length();
    }

    // Return the first k number of digits from
    // number. If the number of digits in number
    // is less than k, return number.
    public static long getprefx(long cnumber, int k) {
        if (thesize(cnumber) > k) {
            String num = cnumber + "";
            return Long.parseLong(num.substring(0, k));
        }
        return cnumber;
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("token", tokenId);
        mMap.put("client_id", getUserId());
        return mMap;
    }


    private void saveData() {

        Log.e("Checkl", mParam().toString() + "::::::" + getAuthKey());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addCard(getAuthKey(), mParam()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                CheckModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", getUserId());
        return mMap;
    }

    private void executeCardRequest() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getClientDetailsCard(getAuthKey(), mParam2()).enqueue(new Callback<CardDetailModel>() {
            @Override
            public void onResponse(Call<CardDetailModel> call, Response<CardDetailModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                CardDetailModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    for (int i = 0; i < mModel.getData().size(); i++) {
                        if (mModel.getData().get(i).getIsPrimary().equals("1")) {
                            card_id= mModel.getData().get(i).getCard_id();
                            cardnumber = mModel.getData().get(i).getCard_last4();
                            txCardNumber.setText("**** **** **** " + cardnumber);
                            edCvvnumber.setText("***");
                            edCardHolderName.setText(mModel.getData().get(i).getCard_holder_name());
                            txExpirationDate.setText(mModel.getData().get(i).getExp_month() + "/" + mModel.getData().get(i).getExp_year());
                        }
                    }
                } else {
                    showAlertDialog(mActivity, "No cards added");
                }
            }

            @Override
            public void onFailure(Call<CardDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


}


