package com.leedgo.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.gson.JsonObject;
import com.leedgo.app.BuildConfig;
import com.leedgo.app.R;
import com.leedgo.app.models.CheckModel;
import com.leedgo.app.models.profile.ProfileModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;
import com.leedgo.app.utils.LeedgoPrefrences;
import com.suke.widget.SwitchButton;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeftMenuActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = LeftMenuActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = LeftMenuActivity.this;
    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.imgProfileCIV)
    CircleImageView imgProfileCIV;
    @BindView(R.id.txtUserNameTV)
    TextView txtUserNameTV;
    @BindView(R.id.txtUserEmailTV)
    TextView txtUserEmailTV;
    @BindView(R.id.editProfileLL)
    LinearLayout editProfileLL;
    @BindView(R.id.notificationsLL)
    LinearLayout notificationsLL;
    @BindView(R.id.notificationsSB)
    SwitchButton notificationsSB;
    @BindView(R.id.editPaymentLL)
    LinearLayout editPaymentLL;
    @BindView(R.id.customerSupportLL)
    LinearLayout customerSupportLL;
    @BindView(R.id.aboutLeedGoLL)
    LinearLayout aboutLeedGoLL;
    @BindView(R.id.termServicesLL)
    LinearLayout termServicesLL;
    @BindView(R.id.privacyPolicyLL)
    LinearLayout privacyPolicyLL;
    @BindView(R.id.rateUsLL)
    LinearLayout rateUsLL;
    @BindView(R.id.logoutLL)
    LinearLayout logoutLL;
    ProfileModel mProfileModel;

    String notificationVal;

    /*
     * Initialize...
     * */


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_left_menu);
        ButterKnife.bind(this);

        setSavedDataOnWidgets();

        executeProfileDataApi();

        notificationsSB.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (view.isChecked()) {
                    notificationVal = "0";
                    executeNotificationApi();
                } else {
                    notificationVal = "1";
                    executeNotificationApi();
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        executeProfileDataApi();
    }

    private void setSavedDataOnWidgets() {
        if (getUserProfilePic() != null) {
            Glide.with(mActivity)
                    .asBitmap()
                    .load(getUserProfilePic())
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            if (resource != null) {
                                imgProfileCIV.setImageBitmap(resource);
                                // mBitmap = resource;
                            }
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                        }
                    });
        } else {

            imgProfileCIV.setImageResource(R.drawable.ic_person);
        }

        if (getNotificationVal() != null) {
            if (getNotificationVal().equals("0")) {
                notificationsSB.setChecked(true);
            } else {
                notificationsSB.setChecked(false);
            }
        }

        if (getUserEmail() != null) {
            txtUserEmailTV.setText(getUserEmail());
        }
        if (getUserName() != null) {
            txtUserNameTV.setText(getUserName());
        }
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.backRL, R.id.editProfileLL, R.id.notificationsLL, R.id.editPaymentLL, R.id.customerSupportLL,
            R.id.aboutLeedGoLL, R.id.termServicesLL, R.id.privacyPolicyLL, R.id.shareAppLL, R.id.rateUsLL, R.id.logoutLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                performCloseClick();
                break;
            case R.id.editProfileLL:
                performEditProfileClick();
                break;
            case R.id.notificationsLL:
                performNotificationsClick();
                break;
            case R.id.editPaymentLL:
                performEditPaymentClick();
                break;
            case R.id.customerSupportLL:
                performCustomerSupportClick();
                break;
            case R.id.aboutLeedGoLL:
                performAboutLeedgoClick();
                break;
            case R.id.termServicesLL:
                performTermServicesClick();
                break;
            case R.id.privacyPolicyLL:
                performPrivacyPolicyClick();
                break;
            case R.id.shareAppLL:
                performShareAppClick();
                break;
            case R.id.rateUsLL:
                performRateUsClick();
                break;
            case R.id.logoutLL:
                performLogoutClick();
                break;
        }
    }


    private void performCloseClick() {
        finish();
    }

    private void performEditProfileClick() {
        startActivity(new Intent(mActivity, EditProfileActivity.class));
    }

    private void performNotificationsClick() {
        startActivity(new Intent(mActivity, NotificationsActivity.class));
    }

    private void performEditPaymentClick() {
        startActivity(new Intent(mActivity, EditPaymentActivity.class));
    }

    private void performCustomerSupportClick() {
        startActivity(new Intent(mActivity, CuctomerTicketListMain.class));

    }

    private void performAboutLeedgoClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.ABOUT_US);
        startActivity(mIntent);
    }

    private void performTermServicesClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.TERMS_SERVICES);
        startActivity(mIntent);
    }

    private void performPrivacyPolicyClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.PRIVACY_POLICY);
        startActivity(mIntent);
    }

    private void performShareAppClick() {
        /*Create an ACTION_SEND Intent*/
        Intent intent = new Intent(Intent.ACTION_SEND);
        /*This will be the actual content you wish you share.*/
        String shareBody = "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;
        /*The type of the content is text, obviously.*/
        intent.setType("text/plain");
        /*Applying information Subject and Body.*/
        intent.putExtra(Intent.EXTRA_TEXT, shareBody);
        /*Fire!*/
        startActivity(Intent.createChooser(intent, getString(R.string.app_name)));
    }

    private void performRateUsClick() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }


    private void performLogoutClick() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_logout);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView btnOkB = alertDialog.findViewById(R.id.btnOkB);
        TextView btnCancelB = alertDialog.findViewById(R.id.btnCancelB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnOkB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                executeLogoutApi();
            }
        });

        alertDialog.show();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeProfileDataApi() {
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUserDataRequest(getAuthKey(), mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                try {
                    dismissProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                mProfileModel = response.body();
                if (mProfileModel.getStatus() == 1) {
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_ID, mProfileModel.getData().getId());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_NAME, mProfileModel.getData().getName());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_EMAIL, mProfileModel.getData().getEmail());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_PROFILE_PIC, mProfileModel.getData().getProfilePic());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.CELL_NO, mProfileModel.getData().getCellno());
                    setSavedDataOnWidgets();

                } else {

                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        mMap.put("user_type", "client");
        mMap.put("notification_status", notificationVal);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeNotificationApi() {
        backRL.setEnabled(false);
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getNotification(mParam2()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {

                backRL.setEnabled(true);
                CheckModel model;
                model = response.body();
                if (model.getStatus() == 1) {
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.ENABLE_PUSH, notificationVal);
                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                backRL.setEnabled(true);
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Execute logout api for user
     *
     * type 1 for user
     * type 2 for provider
     *
     * */
    private Map<String, String> mLogoutParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_provider_id", getUserId());
        mMap.put("type", "1");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        backRL.setEnabled(false);
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(mLogoutParams()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog();
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                        if (jsonObjectMM.getString("status").equals("1")) {

                            String reEmail = getRemeberEmail();
                            String rePassword = getRemeberPassword();
                            boolean reRememberCre = IsRememberMe();
                            SharedPreferences preferences = LeedgoPrefrences.getPreferences(Objects.requireNonNull(mActivity));
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.apply();
                            editor.commit();

                            LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_EMAIL, reEmail);
                            LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_PASSWORD, rePassword);
                            LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.REMEMBER_CREDENTIALS, reRememberCre);

                            Intent mIntent = new Intent(mActivity, SignInActivity.class);
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(mIntent);
                            finish();

                            LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_EMAIL, reEmail);
                            LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.REMEMBER_PASSWORD, rePassword);
                            try {
                                GoogleSignIn.getClient(mActivity,
                                        new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()
                                ).signOut();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            showAlertDialog(mActivity, jsonObjectMM.getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}