package com.leedgo.app.activities;

import static com.yalantis.ucrop.util.FileUtils.getPath;
import static com.yalantis.ucrop.util.FileUtils.isExternalStorageDocument;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.leedgo.app.R;
import com.leedgo.app.utils.LeedgoPrefrences;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    String TAG = BaseActivity.this.getClass().getSimpleName();
    public static final String DOCUMENTS_DIR = "documents";
    private Dialog progressDialog;
    private static final boolean DEBUG = false; // Set to true to enable logging
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    /*
     * Get Current User is  LoggedIN or Not
     * */
    public boolean IsLogin() {
        return LeedgoPrefrences.readBoolean(BaseActivity.this, LeedgoPrefrences.IS_LOGIN, false);
    }
    /*
     * Get Is Intro Screens
     * */
    public boolean IsIntroScreens() {
        return LeedgoPrefrences.readBoolean(BaseActivity.this, LeedgoPrefrences.IS_INTRO_SCREENS, false);
    }
    /*
     * Get Remember Credentials
     * */
    public boolean IsRememberMe() {
        return LeedgoPrefrences.readBoolean(BaseActivity.this, LeedgoPrefrences.REMEMBER_CREDENTIALS, false);
    }
    /*
     * Get Remember Email
     * */
    public String getRemeberEmail() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.REMEMBER_EMAIL, "");
    }
    /*
     * Get Remember Password
     * */
    public String getRemeberPassword() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.REMEMBER_PASSWORD, "");
    }
    /*
     * Get User ID
     * */
    public String getUserId() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.USER_ID, "");
    }
    /*
     * Get User AuthKey
     * */
    public String getAuthKey() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.AUTH_KEY, "");
    }
    /*
     * Get User Name
     * */
    public String getUserName() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.USER_NAME, "");
    }

    /*
     * Get EDit Loction id
     * */
    public String getEditLocatioId() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.CLIENT_LOCTION_NEW_ID, "");
    }
    /*
     * Get EDit Loction id
     * */
    public String getAddorEdit() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.EDITORADDLOCATION, "");
    }
    /*
     * Get User Name
     * */
    public String getCellno() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.CELL_NO, "");
    }
    /*
     * Get User Email
     * */
    public String getUserEmail() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.USER_EMAIL, "");
    }
    /*
     * Get User ProfilePicture
     * */
    public String getUserProfilePic() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.USER_PROFILE_PIC, "");
    }
    /*
     * Get User Notification
     * */
    public String getNotificationVal() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.ENABLE_PUSH, "");
    }

    /*
     * Get User Latitude
     * */
    public String getUserLatitude() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.LATITUDE, "");
    }
    /*
     * Get User Longitude
     * */
    public String getUserLongitude() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.LONGITUDE, "");
    }
    /*
     * Get User Locations
     * */
    public String getUserLocations() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.USER_LOCATIONS, "");
    }

    /*
     * Get User Locations Latitude
     * */
    public String getCurrentLatitude() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.CURRENT_LOCATION_LATITUDE, "");
    }
    /*
     * Get User Locations Longitude
     * */
    public String getCurrentLongitude() {
        return LeedgoPrefrences.readString(BaseActivity.this, LeedgoPrefrences.CURRENT_LOCATION_LONGITUDE, "");
    }
    /*
     * Set Toolbar Text
     * */
    public void setToolbarText(TextView mTextView,String mString){
        mTextView.setText(mString);
    }

    /*
     * Finish the activity
     * */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionSlideDownExit();
    }

    public void hideKeyBoardFromView(Activity mActivity) {
        InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = mActivity.getCurrentFocus();
        if (view == null) {
            view = new View(mActivity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /*
     * To Start the New Activity
     * */
    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionSlideUPEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    public void overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    public void overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down);
    }

    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        progressDialog.show();


    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*
     * Hide Keyboard
     * */
    public boolean hideKeyBoad(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    /*
     * Validate User Name
     * */
    public boolean isValidUserName(String email) {
        return Pattern.compile("/^(?=.{3,30}$)(?:[a-zA-Z\\d]+(?:(?:.|-|)[a-zA-Z\\d])*)+$/").matcher(email).matches();
    }


    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*
     * Error Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }


    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    /*
     * Generate String Dynamically
     * */
    public String getAlphaNumericString() {
        int n = 20;

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }


    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    /*
     *
     * Convert Simple Date to Formated Date
     * */
    public String getConvertedDateDiffrentSymbol(String mString) {
        String[] mArray = mString.split("T");
        String formattedDate = "";
        try {
            DateFormat srcDf = new SimpleDateFormat("dd/MM/yyyy");

            // parse the date string into Date object
            Date date = srcDf.parse(mArray[0]);

            DateFormat destDf = new SimpleDateFormat("dd MMM, yyyy");

            // format the date into another format
            formattedDate = destDf.format(date);

            System.out.println("Converted date is : " + formattedDate);
        } catch (Exception e) {
            e.toString();
        }

        return formattedDate;
    }


    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialog(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialogALL(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }


    /*
     * Time Picker
     * */
    public void showTimePicker(Activity mActivity, final TextView mTextView) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String format = "";

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        } else if (hourOfDay == 12) {

                            format = "PM";

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        } else {

                            format = "AM";
                        }

                        String tempHour = "" + hourOfDay;
                        String tempMinute = "" + minute;
                        if (tempHour.length() == 1)
                            tempHour = "0" + hourOfDay;
                        if (tempMinute.length() == 1)
                            tempMinute = "0" + minute;

                        mTextView.setText(tempHour + ":" + tempMinute + " " + format);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    /*
     *
     * Logout Dialog
     * */
    public void showLogoutDialog(final Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_logout);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView btnCancelB = alertDialog.findViewById(R.id.btnCancelB);
        TextView btnOkB = alertDialog.findViewById(R.id.btnOkB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnOkB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                SharedPreferences preferences = LeedgoPrefrences.getPreferences(mActivity);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                editor.apply();

//                Intent mIntent = new Intent(mActivity, SignInActivity.class);
//                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(mIntent);
//                finish();
            }
        });
        alertDialog.show();
    }


    public void openLinkOnBrowser(String mUrl) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(mUrl));
        startActivity(i);
    }


    public int getMonthDaysCount(int year, int month) {
        int number_Of_DaysInMonth = 0;

        switch (month) {
            case 1:
                month = 1;
                number_Of_DaysInMonth = 31;
                break;
            case 2:
                month = 2;
                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                    number_Of_DaysInMonth = 29;
                } else {
                    number_Of_DaysInMonth = 28;
                }
                break;
            case 3:
                month = 3;
                number_Of_DaysInMonth = 31;
                break;
            case 4:
                month = 4;
                number_Of_DaysInMonth = 30;
                break;
            case 5:
                month = 5;
                number_Of_DaysInMonth = 31;
                break;
            case 6:
                month = 6;
                number_Of_DaysInMonth = 30;
                break;
            case 7:
                month = 7;
                number_Of_DaysInMonth = 31;
                break;
            case 8:
                month = 8;
                number_Of_DaysInMonth = 31;
                break;
            case 9:
                month = 9;
                number_Of_DaysInMonth = 30;
                break;
            case 10:
                month = 10;
                number_Of_DaysInMonth = 31;
                break;
            case 11:
                month = 11;
                number_Of_DaysInMonth = 30;
                break;
            case 12:
                month = 12;
                number_Of_DaysInMonth = 31;
        }

        return number_Of_DaysInMonth;
    }


    public void hideStatusBar() {

        //Hide the status bar on Android 4.0 and Lower
        if (Build.VERSION.SDK_INT < 16) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int visibility = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(visibility);

        }
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public String getRealPathFromURI_API19(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
//                String fileName = getFilePath(context, uri);
//                if (fileName != null) {
//                    return Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName;
//                }
//
//                String id = DocumentsContract.getDocumentId(uri);
//                if (id.startsWith("raw:")) {
//                    id = id.replaceFirst("raw:", "");
//                    File file = new File(id);
//                    if (file.exists())
//                        return id;
//                }
//
//                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//                return getDataColumn(context, contentUri, null, null);
//            }


                final String id = DocumentsContract.getDocumentId(uri);

                if (id != null && id.startsWith("raw:")) {
                    return id.substring(4);
                }

                String[] contentUriPrefixesToTry = new String[]{
                        "content://downloads/public_downloads",
                        "content://downloads/my_downloads",
                        "content://downloads/all_downloads"
                };

                for (String contentUriPrefix : contentUriPrefixesToTry) {

                    Uri contentUri = null;
                    try {
                        contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), Long.valueOf(Integer.parseInt(id)));
                        String path = getDataColumn(context, contentUri, null, null);
                        if (path != null) {
                            return path;
                        }
                    } catch (NumberFormatException ex) { // handle your exception
                        Log.e(TAG, ex.toString());
                    } catch (Exception e) {

                    }
                }

                // path could not be retrieved using ContentResolver, therefore copy file to accessible cache using streams
                String fileName = getFileName(context, uri);
                File cacheDir = getDocumentCacheDir(context);
                File file = generateFileName(fileName, cacheDir);
                String destinationPath = null;
                if (file != null) {
                    destinationPath = file.getAbsolutePath();
                    saveFileFromUri(context, uri, destinationPath);
                }

                return destinationPath;


//                final String id = DocumentsContract.getDocumentId(uri);
//                final Uri contentUri = ContentUris.withAppendedId(
//                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//
//                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } catch (Exception e) {
            Log.e(TAG, "url::" + e.toString());

        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
    public static String getFileName(@NonNull Context context, Uri uri) {
        String mimeType = context.getContentResolver().getType(uri);
        String filename = null;

        if (mimeType == null && context != null) {
            String path = getPath(context, uri);
            if (path == null) {
                filename = getName(uri.toString());
            } else {
                File file = new File(path);
                filename = file.getName();
            }
        } else {
            Cursor returnCursor = context.getContentResolver().query(uri, null,
                    null, null, null);
            if (returnCursor != null) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                returnCursor.moveToFirst();
                filename = returnCursor.getString(nameIndex);
                returnCursor.close();
            }
        }

        return filename;
    }

    public static String getName(String filename) {
        if (filename == null) {
            return null;
        }
        int index = filename.lastIndexOf('/');
        return filename.substring(index + 1);
    }

    public static File getDocumentCacheDir(@NonNull Context context) {
        File dir = new File(context.getCacheDir(), DOCUMENTS_DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        logDir(context.getCacheDir());
        logDir(dir);

        return dir;
    }

    @Nullable
    public static File generateFileName(@Nullable String name, File directory) {
        if (name == null) {
            return null;
        }

        File file = new File(directory, name);

        if (file.exists()) {
            String fileName = name;
            String extension = "";
            int dotIndex = name.lastIndexOf('.');
            if (dotIndex > 0) {
                fileName = name.substring(0, dotIndex);
                extension = name.substring(dotIndex);
            }

            int index = 0;

            while (file.exists()) {
                index++;
                name = fileName + '(' + index + ')' + extension;
                file = new File(directory, name);
            }
        }

        try {
            if (!file.createNewFile()) {
                return null;
            }
        } catch (IOException e) {
            Log.w("TAG", e);
            return null;
        }

        logDir(directory);

        return file;
    }

    private static void saveFileFromUri(Context context, Uri uri, String destinationPath) {
        InputStream is = null;
        BufferedOutputStream bos = null;
        try {
            is = context.getContentResolver().openInputStream(uri);
            bos = new BufferedOutputStream(new FileOutputStream(destinationPath, false));
            byte[] buf = new byte[1024];
            is.read(buf);
            do {
                bos.write(buf);
            } while (is.read(buf) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private static void logDir(File dir) {
        if (!DEBUG) return;
        Log.d("TAG", "Dir=" + dir);
        File[] files = dir.listFiles();
        for (File file : files) {
            Log.d("TAG", "File=" + file.getPath());
        }
    }


}
