package com.leedgo.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.adapters.Notificationadapter;
import com.leedgo.app.interfaces.NotificationClickInterface;
import com.leedgo.app.models.CheckModel;
import com.leedgo.app.models.NotificationModel;
import com.leedgo.app.models.RequestTaskModel;
import com.leedgo.app.models.RoomCreateModel;
import com.leedgo.app.models.StripeAccountModel;
import com.leedgo.app.models.TaskDetailsModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;
import com.leedgo.app.utils.LeedgoPrefrences;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = NotificationsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = NotificationsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.no_notiifcations)
    TextView tx_no_notifications;
    @BindView(R.id.notification_recycler)
    RecyclerView recyclerView;
    List<NotificationModel.Data> mArrayList2 = new ArrayList<NotificationModel.Data>();
    String mNotId, mNotType;
    String providerID = "", providerName = "", taskId = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);

        setToolbarText(txtHeaderTV, getString(R.string.notification));

        executeApi();
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.backRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }

    /*
     * Execute api update profile
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        mMap.put("user_type", "client");
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(mActivity);
        Log.e("Dataq", "1");
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeNotificationList(mParam()).enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                Log.e("Dataq", "2" + response.body());
                NotificationModel mModel = response.body();

                if (mModel.getStatus() == 1) {
                    mArrayList2 = response.body().getData();
                    setAdapter();
                    tx_no_notifications.setVisibility(View.GONE);
                } else {
                    Log.e("Data", "error fetch");
                    recyclerView.setVisibility(View.GONE);
                    tx_no_notifications.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        Notificationadapter mSearchLocationAdapter = new Notificationadapter(mActivity, mArrayList2, notificationClickInterface);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mSearchLocationAdapter);
    }

    NotificationClickInterface notificationClickInterface = new NotificationClickInterface() {
        @Override
        public void getNotification(NotificationModel.Data mModel) {
            if (mModel.getNotification_type().equals("chat")) {
                mNotId = mModel.getMessage_id();
                mNotType = "chat";
            } else {
                mNotId = mModel.getNotification_id();
                mNotType = "notification";
            }

            taskId = mModel.getTask_id();
            providerID = mModel.getSender_id();
            providerName = mModel.getSender_name();
            if (mModel.getNotification_type().equals("chat")) {
                executeNotApi();
                executeMessage();
            } else if (mModel.getNotification_type().equals("request_submitted") ||
                    mModel.getNotification_type().equals("received_bid") ||
                    mModel.getNotification_type().equals("unassigned_task") ||
                    mModel.getNotification_type().equals("new_bid_reminder") ||
                    mModel.getNotification_type().equals("pending_bid_status")) {
                executeNotApi();
                executeDetailApi();
            } else if (mModel.getNotification_type().equals("task_date_scheduled") ||
                    mModel.getNotification_type().equals("bid_approved") ||
                    mModel.getNotification_type().equals("request_start_day")) {
                executeNotApi();
                executeGetAllTasksAPI(mModel.getTask_id());
            } else if (mModel.getNotification_type().equals("task_complete")) {
                executeNotApi();
                executeTaskPaymentApi();
            }
        }
    };

    /*
     * Execute create room api
     * */
    private Map<String, String> mMessageParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", LeedgoPrefrences.readString(mActivity, LeedgoPrefrences.USER_ID, ""));
        mMap.put("provider_id", providerID);
        Log.e("Room", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeMessage() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createRoom(mMessageParam()).enqueue(new Callback<RoomCreateModel>() {
            @Override
            public void onResponse(Call<RoomCreateModel> call, Response<RoomCreateModel> response) {
                dismissProgressDialog();
                RoomCreateModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Intent intent = new Intent(mActivity, ChatActivity.class);
                    intent.putExtra("room_id", mModel.getRoom_id());
                    intent.putExtra("name", providerName);
                    mActivity.startActivity(intent);

                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RoomCreateModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(String.valueOf(mActivity), "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Execute Get Task Detail By Task Id Api
     * */
    private Map<String, String> mTaskDetailsParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("task_id", taskId);
        Log.e("INpro", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getRequesDetails(mTaskDetailsParam()).enqueue(new Callback<TaskDetailsModel>() {
            @Override
            public void onResponse(Call<TaskDetailsModel> call, Response<TaskDetailsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TaskDetailsModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    Intent intent = new Intent(mActivity, TrackTaskScreen.class);
                    intent.putExtra("orderStatus", mModel.getData().getOrder_status());
                    intent.putExtra("paymentStatus", mModel.getData().getPayment_status());
                    intent.putExtra("serviceName", mModel.getData().getCategory_name());
                    intent.putExtra("categoryImage", mModel.getData().getCategory_image());
                    intent.putExtra("subCategoryName", mModel.getData().getSubcategory_name());
                    intent.putExtra("workLocation", mModel.getData().getAddress());
                    intent.putExtra("flexibleWeeks", mModel.getData().getFlexible_week());
                    intent.putExtra("workDescription", mModel.getData().getWork_details());
                    intent.putExtra("isHistoricalStructure", mModel.getData().getRequested_for_historical_structure().trim().toString());
                    intent.putExtra("isInsuranceClaim", mModel.getData().getRequest_covered_by_insurance_claim().trim());
                    intent.putExtra("isRepresentative", mModel.getData().getOwner_or_authorized_representative_of_owner().trim());
                    intent.putExtra("taskId", mModel.getData().getId());
                    mActivity.startActivity(intent);

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TaskDetailsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    Dialog alertDialog;
    String formattedDate;
    String providerId = "";

    /*
     * Execute Get Task Payment Status Api
     * */
    private Map<String, String> mTaskPaymentParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("task_id", taskId);
        Log.e("INpro", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeTaskPaymentApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getTaskPaymentStatus(mTaskPaymentParam()).enqueue(new Callback<TaskDetailsModel>() {
            @Override
            public void onResponse(Call<TaskDetailsModel> call, Response<TaskDetailsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TaskDetailsModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    providerId = mModel.getData().getAssign_user();

                    showAlertPDialog(mActivity, mModel.getData().getCategory_image(), mModel.getData().getCategory_name(),
                            mModel.getData().getSubcategory_name(), mModel.getData().getTask_complete_date(), mModel.getData().getUnique_task_id(),
                            mModel.getData().getAssigned_amount(), mModel.getData().getId());

                } else if (mModel.getStatus() == 0) {
                    showAlertPaymentDoneDialog(mActivity, mModel.getData().getUnique_task_id(), mModel.getData().getTransaction());
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TaskDetailsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void showAlertPDialog(Activity mActivity, String category_image, String category_name, String subcategory_name, String task_complete_date, String unique_task_id, String amount, String providerId) {
        alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert_payment);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txServiceName = alertDialog.findViewById(R.id.txServiceName);
        TextView txServiceType = alertDialog.findViewById(R.id.txServiceNameType);
        TextView txPaymentMessage = alertDialog.findViewById(R.id.txPaymentMessage);
        TextView txLater = alertDialog.findViewById(R.id.txLater);
        Button btnPay = alertDialog.findViewById(R.id.btnPay);
        ImageView imServiceImage = alertDialog.findViewById(R.id.imServiceImage);
        TextView txdialogHeading = alertDialog.findViewById(R.id.txdialogHeading);
        Glide.with(this).load(category_image).placeholder(R.drawable.ic_pp).into(imServiceImage);
        txServiceName.setText(category_name);
        long coment_time = Long.parseLong(task_complete_date);
        timeChange(coment_time);
        txServiceType.setText(subcategory_name);
        txPaymentMessage.setText(getString(R.string.paymentpending1) + " " + unique_task_id + " " + getString(R.string.paymentpending2));
        // txPaymentMessage.setText(strMessage);
        txLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To check provider Account added or not
                executeCheckStripeStatusApi(subcategory_name, unique_task_id, formattedDate, category_image, amount, providerId);

                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    void timeChange(long coment_time) {
        Date date = new java.util.Date(coment_time * 1000L);

        // the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());
        formattedDate = sdf.format(date);
    }

    private Map<String, String> mParam2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", providerId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCheckStripeStatusApi(String subcategory_name, String unique_task_id, String formattedDate, String category_image, String amount, String providerId) {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.providerStripeAccount(mParam2()).enqueue(new Callback<StripeAccountModel>() {
            @Override
            public void onResponse(Call<StripeAccountModel> call, Response<StripeAccountModel> response) {
                dismissProgressDialog();
                CheckModel model;
                StripeAccountModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    startActivity(new Intent(NotificationsActivity.this, PaymentActivity2.class)
                            .putExtra("serviceName", subcategory_name)
                            .putExtra("serviceId", unique_task_id)
                            .putExtra("serviceTime", formattedDate)
                            .putExtra("serviceImage", category_image)
                            .putExtra("servicePrice", amount)
                    );

                } else {
                    showAlertDialog(mActivity, getString(R.string.you_cant_proceed_to_checkout));
                }
            }

            @Override
            public void onFailure(Call<StripeAccountModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /**
     * Animation Time Out
     */
    public final int ANIMATION_TIME_OUT = 500;

    public void showAlertPaymentDoneDialog(Activity mActivity, String unique_task_id, String transaction) {
        alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_done_payment);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button

        TextView txPaymentMessage = alertDialog.findViewById(R.id.txPaymentMessage);
        TextView txdialogHeading = alertDialog.findViewById(R.id.txdialogHeading);
        Button btnOkay = alertDialog.findViewById(R.id.btnOkay);
        ImageView checkIV = alertDialog.findViewById(R.id.checkIV);

        Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.image_anim);
        animation.setDuration(ANIMATION_TIME_OUT);
        checkIV.startAnimation(animation);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                txPaymentMessage.setVisibility(View.VISIBLE);
                txdialogHeading.setVisibility(View.VISIBLE);
                btnOkay.setVisibility(View.VISIBLE);
            }
        }, ANIMATION_TIME_OUT);

        txPaymentMessage.setText(getString(R.string.paymentDone1) + " " + unique_task_id + " " + getString(R.string.paymentDone2) + " " + transaction);
        // txPaymentMessage.setText(strMessage);

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To check provider Account added or not
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mAllTasksParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    String provider_name = "", provider_image = " ", provider_email = "",
            service_name = "", sub_category_name = "", work_date = "", category_image = "";

    private void executeGetAllTasksAPI(String Task_Id) {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllTasks(mAllTasksParam()).enqueue(new Callback<RequestTaskModel>() {
            @Override
            public void onResponse(Call<RequestTaskModel> call, Response<RequestTaskModel> response) {
                dismissProgressDialog();
                try {
                    Log.e(TAG, "**RESPONSE**" + mParam().toString() + ":::::" + response.body().toString());
                    RequestTaskModel mModel = null;
                    mModel = response.body();
                    if (mModel.getStatus() == 1) {

                        for (int i = 0; i < mModel.getData().size(); i++) {
                            if (mModel.getData().get(i).getId().equals(Task_Id)) {

                                if (mModel.getData().get(i).getProvider_detail() != null) {

                                    if (mModel.getData().get(i).getProvider_detail().getProvider_id() != null)
                                        providerID = mModel.getData().get(i).getProvider_detail().getProvider_id();

                                    if (mModel.getData().get(i).getProvider_detail().getProvider_name() != null)
                                        provider_name = mModel.getData().get(i).getProvider_detail().getProvider_name();

                                    if (mModel.getData().get(i).getProvider_detail().getProfile_image() != null)
                                        provider_image = mModel.getData().get(i).getProvider_detail().getProfile_image();

                                    if (mModel.getData().get(i).getProvider_detail().getBusiness_email() != null)
                                        provider_email = mModel.getData().get(i).getProvider_detail().getBusiness_email();
                                }

                                if (mModel.getData().get(i).getService_name() != null)
                                    service_name = mModel.getData().get(i).getService_name();

                                if (mModel.getData().get(i).getSub_category_name() != null)
                                    sub_category_name = mModel.getData().get(i).getSub_category_name();

                                if (mModel.getData().get(i).getProvider_work_date() != null)
                                    work_date = mModel.getData().get(i).getWork_date();

                                if (mModel.getData().get(i).getCategory_image() != null)
                                    category_image = mModel.getData().get(i).getCategory_image();
                            }
                        }

                        startActivity(new Intent(NotificationsActivity.this, TaskDetailsActivity.class)
                                .putExtra("providerID", providerID)
                                .putExtra("provider_name", provider_name)
                                .putExtra("provider_image", provider_image)
                                .putExtra("provider_email", provider_email)
                                .putExtra("service_name", service_name)
                                .putExtra("sub_category_name", sub_category_name)
                                .putExtra("work_date", work_date)
                                .putExtra("category_image", category_image)
                        );

                    } else {
                        showToast(mActivity, mModel.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<RequestTaskModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getIntent() != null && getIntent().getStringExtra(Constants.NOTIFICATION_TYPE) != null
                && getIntent().getStringExtra(Constants.NOTIFICATION_TYPE).equals(Constants.PUSH)) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mIntent);
            finish();
        } else {
            finish();
        }
    }

    /*
     * Execute api update profile
     * */
    private Map<String, String> mNotParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        mMap.put("user_type", "client");
        mMap.put("notification_id", mNotId);
        mMap.put("notification_type", mNotType);
        Log.i(TAG, "mNotParam: " + mNotId + mNotType);
        return mMap;
    }

    private void executeNotApi() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.readNotificationRequest(mNotParam()).enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {

            }

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
            }
        });
    }

}