package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.internal.$Gson$Preconditions;
import com.leedgo.app.R;
import com.leedgo.app.adapters.CustomerTicketListAdapter;
import com.leedgo.app.adapters.ServicesAdapter;
import com.leedgo.app.models.ServicesDataModel;
import com.leedgo.app.models.ServicesModel;
import com.leedgo.app.models.TicketListModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CuctomerTicketListMain extends BaseActivity {
    /**
     * Current Activity Instance
     */
    Activity mActivity = CuctomerTicketListMain.this;


    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.txNoTickets)
    TextView txNoTickets;
    @BindView(R.id.ticketsRecyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.btnSaveB)
    Button btnSaveB;

   public static ArrayList<String> mData=new ArrayList<>();
    /*
     * Initialize...
     * */
    ArrayList<TicketListModel.Data> mArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_support_ticket);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV,"Support Tickets");

        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSaveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
                btnSaveB.startAnimation(myAnim);


                Intent intent=new Intent(mActivity,CustomerSupportActivity.class);
                intent.putExtra("taskArray",mData);

                startActivity(intent);

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        executeRequest();

    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        mMap.put("role","client");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeRequest() {
        mArrayList.clear();
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getTicketList(mParams()).enqueue(new Callback<TicketListModel>() {
            @Override
            public void onResponse(Call<TicketListModel> call, Response<TicketListModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TicketListModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArrayList.addAll(response.body().getData());
                    try {
                        for (int i=0;i<=mArrayList.size();i++)
                        {
                            mData.add(mArrayList.get(i).getTask_id());

                        }

                    }
                    catch (Exception e)
                    {e.printStackTrace();}

                    setAdapter();
                    txNoTickets.setVisibility(View.GONE);
                } else {
                    txNoTickets.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<TicketListModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        CustomerTicketListAdapter mAdapter = new CustomerTicketListAdapter(mActivity, mArrayList);
        recyclerView.setAdapter(mAdapter);
    }

}