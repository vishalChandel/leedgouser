package com.leedgo.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.leedgo.app.R;
import com.leedgo.app.adapters.IntroViewPagerAdapter;
import com.leedgo.app.adapters.SimpleFragmentPagerAdapter;
import com.leedgo.app.utils.LeedgoPrefrences;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator3;

public class IntroActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = IntroActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = IntroActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.mIntroViewPagerVP)
    ViewPager2 mIntroViewPagerVP;

    @BindView(R.id.imgPreviousIV)
    ImageView imgPreviousIV;
    @BindView(R.id.imgNextIV)
    ImageView imgNextIV;
    @BindView(R.id.btnLetsStartedB)
    Button btnLetsStartedB;
    @BindView(R.id.txtSignInTV)
    TextView txtSignInTV;
    @BindView(R.id.txtSkipTV)
    TextView txtSkipTV;
    @BindView(R.id.mIndicatorCI)
    CircleIndicator3 mIndicatorCI;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
        setViewPagerAdapter();
    }

    private void setViewPagerAdapter() {
        // IntroViewPagerAdapter mIntroViewPagerAdapter = new IntroViewPagerAdapter(mActivity);
//        mIntroViewPagerVP.setAdapter(new IntroViewPagerAdapter(getSupportFragmentManager()));
        // Create an adapter that knows which fragment should be shown on each page
        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter( getSupportFragmentManager(),getLifecycle());
        // Set the adapter onto the view pager
        mIntroViewPagerVP.setAdapter(adapter);
        mIndicatorCI.setViewPager(mIntroViewPagerVP);
        Log.e(TAG, "setViewPagerAdapter: " + mIntroViewPagerVP.getCurrentItem());
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.imgPreviousIV, R.id.imgNextIV, R.id.btnLetsStartedB, R.id.txtSignInTV, R.id.txtSkipTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgPreviousIV:
                performPreviousClick();
                break;
            case R.id.imgNextIV:
                performNextClick();
                break;
            case R.id.btnLetsStartedB:
                performLetsStartClick();
                break;
            case R.id.txtSignInTV:
                performSignInClick();
                break;
            case R.id.txtSkipTV:
                performSkipClick();
                break;
        }
    }

    private void performSkipClick() {
        startActivity(new Intent(mActivity, SignInActivity.class));
        finish();
    }

    private void performPreviousClick() {
        mIntroViewPagerVP.setCurrentItem(getItem(-1), true);
        if (getItem(0) == 0) {
            imgPreviousIV.setImageResource(R.drawable.ic_previous_grey);
        } else {
            imgPreviousIV.setImageResource(R.drawable.ic_previous);
        }

        if (getItem(0) == 2) {
            imgNextIV.setImageResource(R.drawable.ic_next_grey);
        } else {
            imgNextIV.setImageResource(R.drawable.ic_next);
        }
    }

    private void performNextClick() {
        mIntroViewPagerVP.setCurrentItem(getItem(+1), true);
        if (getItem(0) == 0) {
            imgPreviousIV.setImageResource(R.drawable.ic_previous_grey);
        } else {
            imgPreviousIV.setImageResource(R.drawable.ic_previous);
        }

        if (getItem(0) == 2) {
            imgNextIV.setImageResource(R.drawable.ic_next_grey);
        } else {
            imgNextIV.setImageResource(R.drawable.ic_next);
        }
    }

    private void performLetsStartClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnLetsStartedB.startAnimation(myAnim);
        LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.IS_INTRO_SCREENS, true);
        startActivity(new Intent(mActivity, SignInActivity.class));
        finish();
    }

    private void performSignInClick() {
        startActivity(new Intent(mActivity, SignInActivity.class));
        finish();
    }

    private int getItem(int i) {
        return mIntroViewPagerVP.getCurrentItem() + i;
    }
}