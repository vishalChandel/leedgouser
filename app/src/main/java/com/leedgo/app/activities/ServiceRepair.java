package com.leedgo.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.leedgo.app.R;
import com.leedgo.app.adapters.CustomAdapter;
import com.leedgo.app.models.AddTaskModel;
import com.leedgo.app.models.GetClientLocationModel;
import com.leedgo.app.models.LocationServicesRawList;
import com.leedgo.app.models.ServicesSubCatData;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceRepair extends BaseActivity implements OnDateSelectedListener, AdapterView.OnItemSelectedListener {

    /**
     * Getting the Current Class Name
     */
    String TAG = ServiceRepair.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ServiceRepair.this;
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;

    @BindView(R.id.calendarView2)
    MaterialCalendarView calendarView;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.txtChangePicTV)
    TextView txtChangePicTv;
    @BindView(R.id.editaddress)
    TextView editaddress;
    @BindView(R.id.editstate)
    TextView editstate;
    @BindView(R.id.editpincode)
    TextView editpincode;
    @BindView(R.id.edPromocde)
    EditText edPromocde;
    @BindView(R.id.service_description)
    EditText servicedescription;
    @BindView(R.id.radioGroupFlexWeek)
    RadioGroup radioGroupFlexWeek;
    @BindView(R.id.radioGroupHistorical)
    RadioGroup radioGroupHistorical;
    @BindView(R.id.radioGroupInsurance)
    RadioGroup radioGroupInsurance;
    @BindView(R.id.radioGroupOwner)
    RadioGroup radioGroupOwner;
    @BindView(R.id.spinner)
    Spinner spin;
    @BindView(R.id.rounimageview1)
    ImageView round1image;
    @BindView(R.id.rounimageview2)
    ImageView round2image;
    @BindView(R.id.rounimageview3)
    ImageView round3image;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.changeLocationLy)
    LinearLayout changeLocationLy;


    String currentDate;
    String scatId;
    Bitmap mBitmap = null;
    Bitmap mBitmap2 = null;
    Bitmap mBitmap3 = null;
    String currentPhotoPath = "";
    // String[] countryNames={"India","China","Australia","Portugle","America","New Zealand"};
    ArrayList<String> countryNames = new ArrayList<String>();
    ArrayList<String> countryNames2 = new ArrayList<>();
    private ArrayList<ServicesSubCatData> mArrayList;
    LocationServicesRawList locationServicesRawList;
    String categoryId = "";
    String subCategoryId = "";
    String latitude = "";
    String longitude = "";

    int radIdFlexWeek = -1;
    int radIdHistoricalStructure = -1;
    int radIdInsurance = -1;
    int radIdOwner = -1;

    int flexWeeks = 1;
    String reqHistorical = "no";
    String reqInsurance = "no";
    String reqowner = "owner";
    String servicename = "";
    String serviceType = "";
    int locationId = 0;
    String locationTitle = "";
    String selCurrentDate = "";
    String state, city, zipcode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_repair);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            countryNames2 = getIntent().getStringArrayListExtra("LIST_ITEMS");
            mArrayList = (ArrayList<ServicesSubCatData>) getIntent().getSerializableExtra(Constants.LIST);
            scatId = getIntent().getStringExtra("serviceId");
            serviceType = getIntent().getStringExtra("serviceType");
            serviceType = serviceType + " Service";
        }
        setToolbarText(txtHeaderTV, serviceType);
        servicedescription.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.service_description) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        setDataOnScreen();
        executeLocationRequest();
        setUpViews();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.backRL, R.id.txtChangePicTV, R.id.btnSubmit, R.id.changeLocationLy, R.id.editpincode, R.id.editstate, R.id.editaddress})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.txtChangePicTV:
                performPicAdd();
                break;
            case R.id.btnSubmit:
                performCheck();
                break;
            case R.id.changeLocationLy:
                ChangePrimaryLocation();
                break;
            case R.id.editaddress:
                ChangePrimaryLocation();
                break;
            case R.id.editstate:
                ChangePrimaryLocation();
                break;
            case R.id.editpincode:
                ChangePrimaryLocation();
                break;
        }
    }

    private void ChangePrimaryLocation() {
        Intent mIntent = new Intent(mActivity, AddLocationsActivity.class);
        mIntent.putExtra("Check", "confirm");
        mIntent.putExtra("longitude", longitude);
        mIntent.putExtra("latitude", latitude);
        mIntent.putExtra("address", editaddress.getText().toString());
        mIntent.putExtra("state", state);
        mIntent.putExtra("city", city);
        mIntent.putExtra("zipcode", zipcode);
        startActivityForResult(mIntent, Constants.REQUEST_CODE);
    }

    private void performCheck() {
        btnSubmit.setClickable(false);
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSubmit.startAnimation(myAnim);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                btnSubmit.setClickable(true);
            }
        }, 2000);
    if(isValidate()){
        showProgressDialog(mActivity);
        executeApi();
    }
    }

    private boolean isValidate() {
        boolean flag = true;
        radIdFlexWeek = radioGroupFlexWeek.getCheckedRadioButtonId();
        radIdHistoricalStructure = radioGroupHistorical.getCheckedRadioButtonId();
        radIdInsurance = radioGroupInsurance.getCheckedRadioButtonId();
        radIdOwner = radioGroupOwner.getCheckedRadioButtonId();
        if ((radIdFlexWeek == -1)) {
            showAlertDialog(mActivity, "Please select how flexible are you ?");
            flag = false;
        } else if (servicedescription.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter description");
            flag = false;
        } else if (editpincode.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter your address");
            flag = false;
        } else if ((radIdHistoricalStructure == -1)) {
            showAlertDialog(mActivity, "Please select if the request is for Historical structure ?");
            flag = false;
        } else if ((radIdInsurance == -1)) {
            showAlertDialog(mActivity, "Please select if the request is covered by insurance claim ?");
            flag = false;
        } else if ((radIdOwner == -1)) {
            showAlertDialog(mActivity, "Please select " + getString(R.string.are_you_the_owner_or_an_authorized_representative_of_the_owner));
            flag = false;
        }

        return flag;
    }

    private void setUpViews() {

        try {
            calendarView.setOnDateChangedListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        spin.setOnItemSelectedListener(this);

        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), countryNames);
        spin.setAdapter(customAdapter);
        radioGroupFlexWeek.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
                    String s = rb.getText().toString();
                    flexWeeks = Integer.parseInt(s.replaceAll("[\\D]", ""));
                }

            }
        });

        //radioGroupHistorical
        radioGroupHistorical.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
                    reqHistorical = rb.getText().toString();

                    // Toast.makeText(mActivity, rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        //radioGroupInsurance
        radioGroupInsurance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
                    reqInsurance = rb.getText().toString();
                    // Toast.makeText(mActivity, rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        //radioGroupOwner
        radioGroupOwner.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
                    reqowner = rb.getText().toString();
                    // Toast.makeText(mActivity, rb.getText(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void setDataOnScreen() {
        for (int i = 0; i < countryNames2.size(); i++) {
            countryNames.add(countryNames2.get(i));
        }

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, +7);
        System.out.println("Date = " + cal.getTime());
        Date date = new Date();
        calendarView.setCurrentDate(cal.getTime());
        calendarView.setSelectedDate(cal.getTime());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateandTime = sdf.format(new Date(System.currentTimeMillis() + 86400 * 1000 * 7));
        Log.e(TAG, "**TIME**" + currentDateandTime);
        currentDate = currentDateandTime;
        calendarView.state().edit()
                .setMinimumDate(cal.getTime())
                //.setSaveCurrentPosition(true)
                .commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void performPicAdd() {
        if (checkPermission()) {
            if (round3image.getDrawable() == null) {
                onCameraClick();
            } else {
                showAlertDialog(mActivity, "You can only upload upto 3 images.");
            }
        } else {
            requestPermission();
        }
    }

    private void executeApi() {
        Log.e("Categorid", " client_id:" + getUserId() + " category_id:" + subCategoryId + " sub_categor_id:" + categoryId
                + " order_date:" + currentDate + " order_lat:" + String.format("%.7f", Double.parseDouble(latitude)) + " order_long:" + String.format("%.7f", Double.parseDouble(longitude)) + "faxble_weeks:" + flexWeeks +
                " order_details:" + servicedescription.getText().toString() + " address:" + editaddress.getText().toString() +
                " state:" + editstate.getText().toString() + " zip:" + editpincode.getText().toString() + " promocode:" + edPromocde.getText().toString() +
                " requested_for_historical_structure:" + reqHistorical + " request_covered_by_insurance_claim" + reqInsurance
                + " owner_or_authorized_representative_of_owner:" + reqowner);


        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        RequestBody client_id = RequestBody.create(MediaType.parse("multipart/form-data"), getUserId());
        RequestBody category_id = RequestBody.create(MediaType.parse("multipart/form-data"), subCategoryId);
        RequestBody subcategory_id = RequestBody.create(MediaType.parse("multipart/form-data"), categoryId);
        RequestBody order_date = RequestBody.create(MediaType.parse("multipart/form-data"), currentDate);
        RequestBody order_lat = RequestBody.create(MediaType.parse("multipart/form-data"), String.format("%.7f", Double.parseDouble(latitude)));
        RequestBody order_long = RequestBody.create(MediaType.parse("multipart/form-data"), String.format("%.7f", Double.parseDouble(longitude)));
        RequestBody faxble_week = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(flexWeeks));
        RequestBody order_details = RequestBody.create(MediaType.parse("multipart/form-data"), servicedescription.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("multipart/form-data"), editaddress.getText().toString());
        RequestBody state = RequestBody.create(MediaType.parse("multipart/form-data"), editstate.getText().toString());
        RequestBody zip = RequestBody.create(MediaType.parse("multipart/form-data"), editpincode.getText().toString());
        RequestBody promocode = RequestBody.create(MediaType.parse("multipart/form-data"), edPromocde.getText().toString());
        RequestBody requested_for_historical_structure = RequestBody.create(MediaType.parse("multipart/form-data"), reqHistorical.toLowerCase());
        RequestBody request_covered_by_insurance_claim = RequestBody.create(MediaType.parse("multipart/form-data"), reqInsurance.toLowerCase());
        RequestBody owner_or_authorized_representative_of_owner = RequestBody.create(MediaType.parse("multipart/form-data"), reqowner.toLowerCase());


        MultipartBody.Part orderimage1 = null;


        if (mBitmap != null) {
           RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mBitmap));
            orderimage1 = MultipartBody.Part.createFormData("orderimage[1]", getAlphaNumericString(), requestFile);
        } else {
            RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            orderimage1 = MultipartBody.Part.createFormData("orderimage[1]", "", ppostImage);
        }

        MultipartBody.Part orderimage2 = null;

        if (mBitmap2 != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mBitmap2));
            orderimage2 = MultipartBody.Part.createFormData("orderimage[2]", getAlphaNumericString(), requestFile);
        } else {
            RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            orderimage2 = MultipartBody.Part.createFormData("orderimage[2]", "", ppostImage);
        }


        MultipartBody.Part orderimage3 = null;

        if (mBitmap3 != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(mBitmap3));
            orderimage3 = MultipartBody.Part.createFormData("orderimage[3]", getAlphaNumericString(), requestFile);
        } else {
            RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            orderimage3= MultipartBody.Part.createFormData("orderimage[3]", "", ppostImage);
        }


        Call<AddTaskModel> call1 = mApiInterface.addPostDataRequest(client_id, category_id, subcategory_id, order_date, order_lat, order_long, faxble_week, order_details
                , address, state, zip, promocode, orderimage1, orderimage2, orderimage3, requested_for_historical_structure
                , request_covered_by_insurance_claim, owner_or_authorized_representative_of_owner);


        call1.enqueue(new Callback<AddTaskModel>() {
            @Override
            public void onResponse(Call<AddTaskModel> call, Response<AddTaskModel> response) {
                dismissProgressDialog();
                AddTaskModel mModel = response.body();
                assert mModel != null;
                if (mModel.getStatus() == 1) {

                    Intent intent = new Intent(mActivity, OrderSuccessful.class);
                    intent.putExtra("service_name", serviceType);
                    intent.putExtra("service_date", selCurrentDate);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddTaskModel> call, Throwable t) {
                dismissProgressDialog();
                showAlertDialog(mActivity, t.getMessage());


            }
        });
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLocationRequest() {

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUserLocationsRequest(getAuthKey(), mParam()).enqueue(new Callback<GetClientLocationModel>() {
            @Override
            public void onResponse(Call<GetClientLocationModel> call, Response<GetClientLocationModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                GetClientLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    try {
                        for (int i = 0; i <= mModel.getData().size(); i++) {
                            if (mModel.getData().get(i).getPrimaryValue() == 1) {
                                editaddress.setText(mModel.getData().get(i).getAddress());
                                editpincode.setText(mModel.getData().get(i).getZipCode());
                                editstate.setText(mModel.getData().get(i).getState());
                                latitude = mModel.getData().get(i).getLatitude();
                                longitude = mModel.getData().get(i).getLongitude();
                                locationId = mModel.getData().get(i).getId();
                                state = mModel.getData().get(i).getState();
                                city = mModel.getData().get(i).getLocationTitle();
                                zipcode =  mModel.getData().get(i).getZipCode().toString();
                                locationTitle = mModel.getData().get(i).getLocationTitle();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
//                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetClientLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]
                {writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //onSelectImageClick();
                    onCameraClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        executeLocationRequest();
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date1, boolean selected) {

        SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat format3 = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
        Date date = date1.getDate();
        currentDate = String.format(format2.format(date));
        selCurrentDate = String.format(format2.format(date));
        Log.e(TAG, "**current date**" + selCurrentDate);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        for (int i = 0; i <= mArrayList.size(); i++) {
            try {
                if (mArrayList.get(i).getName().equals(countryNames.get(position))) {
                    categoryId = String.valueOf(mArrayList.get(i).getId());
                    subCategoryId = String.valueOf(mArrayList.get(i).getScatId());
                    servicename = String.valueOf(mArrayList.get(i).getName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        View view1 = view.findViewById(R.id.viewr);
        TextView names = (TextView) view.findViewById(R.id.servicename);
        names.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_bottom, 0);
        view1.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void onCameraClick() {
        if (checkPermission()) {
            openCameraGalleryDialog();
        } else {
            requestPermission();
        }
    }

    private void openCameraGalleryDialog() {


        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {


                if (options[item].equals("Take Photo")) {
                    openCamera();

                } else if (options[item].equals("Choose from Gallery")) {
                    openGallery();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openCamera() {
        ImagePicker.with(this)
                .cameraOnly()    //User can only capture image using Camera
                .start();
    }

    private void openGallery() {
        ImagePicker.with(this)
                .galleryOnly()    //User can only capture image using Camera
                .start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri uri = data.getData();
            InputStream imageStream = null;
            try {
                imageStream = getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            showImage(selectedImage);
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show();
        } else {
        }
    }

    private void showImage(Bitmap selectedImage) {
        if (round1image.getDrawable() == null) {
            round1image.setImageBitmap(selectedImage);
            round1image.setVisibility(View.VISIBLE);
            mBitmap = selectedImage;
        } else if (round2image.getDrawable() == null) {
            round2image.setImageBitmap(selectedImage);
            round2image.setVisibility(View.VISIBLE);
            mBitmap2 = selectedImage;
        } else if (round3image.getDrawable() == null) {
            round3image.setImageBitmap(selectedImage);
            round3image.setVisibility(View.VISIBLE);
            mBitmap3 = selectedImage;
        }
    }
}