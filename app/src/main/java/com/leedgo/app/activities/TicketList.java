package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.leedgo.app.R;
import com.leedgo.app.adapters.RequestedHistoryAdapter;
import com.leedgo.app.adapters.TicketListAdapter;
import com.leedgo.app.models.RequestTaskModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketList extends BaseActivity {
Activity mActivity=TicketList.this;

    @BindView(R.id.imClose)
    ImageView imClose;
    @BindView(R.id.recyclerViewRv)
    RecyclerView recyclerView;
    ArrayList<RequestTaskModel.Data> mArrayList2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_list);
        ButterKnife.bind(this);

        executeCompletedApi();
        imClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCompletedApi() {

        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllTasks(mParam()).enqueue(new Callback<RequestTaskModel>() {
            @Override
            public void onResponse(Call<RequestTaskModel> call, Response<RequestTaskModel> response) {
                dismissProgressDialog();
                try {
                    Log.e(TAG, "**RESPONSE**" + mParam().toString() + ":::::" + response.body().toString());
                    RequestTaskModel mModel = null;
                    mModel = response.body();
                    if (mModel.getStatus() == 1) {
                        if (mArrayList2 != null) {
                            mArrayList2.clear();
                        }
                        mArrayList2 = (ArrayList<RequestTaskModel.Data>) mModel.getData();

                        setAdapter2(mArrayList2);
                    }




                    else {
                        showToast(mActivity, mModel.getMessage());
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<RequestTaskModel> call, Throwable t) {
                dismissProgressDialog();
                showToast(mActivity, t.getMessage());
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter2(ArrayList<RequestTaskModel.Data> data) {
        try {


            LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
            TicketListAdapter mAdapter = new TicketListAdapter(mActivity, (ArrayList<RequestTaskModel.Data>) data);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(mAdapter);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }


}