package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.leedgo.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TicketSubmitted extends AppCompatActivity {
    @BindView(R.id.btnContinueB)
    Button btContinue;
    @BindView(R.id.order_text)
    TextView txOrder;
    @BindView(R.id.TV1)
    TextView TV1;
    @BindView(R.id.imgLikeIV)
    ImageView imgLikeIV;

    String serviceType="";


    Activity mActivity=TicketSubmitted.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_submitted);
        ButterKnife.bind(this);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                imgLikeIV.setVisibility(View.VISIBLE);
                final Animation myAnim0 = AnimationUtils.loadAnimation(mActivity, R.anim.image_anim_mod);
                imgLikeIV.startAnimation(myAnim0);

                final Handler handlerTV = new Handler();
                handlerTV.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TV1.setVisibility(View.VISIBLE);
                        txOrder.setVisibility(View.VISIBLE);
                        btContinue.setVisibility(View.VISIBLE);
                        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.image_anim_mod);
                        TV1.startAnimation(myAnim);
                        txOrder.startAnimation(myAnim);
                        btContinue.startAnimation(myAnim);
                    }
                }, 1500);
            }
        }, 200);


    }
    @OnClick({R.id.btnContinueB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinueB:
                Goto();
                break;
            default:
                break;
        }
    }
    private void Goto() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btContinue.startAnimation(myAnim);
        Intent intent=new Intent(mActivity,CuctomerTicketListMain.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


}