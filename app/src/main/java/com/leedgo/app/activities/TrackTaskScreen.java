package com.leedgo.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.leedgo.app.R;
import com.leedgo.app.models.BidListModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackTaskScreen extends BaseActivity {

    Activity mActivity = TrackTaskScreen.this;

    @BindView(R.id.llyBack)
    LinearLayout llyBack;

    @BindView(R.id.txServiceName)
    TextView txServiceName;
    @BindView(R.id.txServiceNameType)
    TextView txServiceNameType;
    @BindView(R.id.imServiceImage)
    ImageView imServiceImage;
    @BindView(R.id.imTask1)
    ImageView imTask1;
    @BindView(R.id.imTask2)
    ImageView imTask2;
    @BindView(R.id.imtask3)
    ImageView imTask3;
    @BindView(R.id.imtask4)
    ImageView imTask4;
    @BindView(R.id.viewtask1)
    View viewTask1;
    @BindView(R.id.viewtask2)
    View viewTask2;
    @BindView(R.id.viewtask3)
    View viewTask3;
    @BindView(R.id.txMainTask1)
    TextView txMainTask1;
    @BindView(R.id.txMainTask2)
    TextView txMainTask2;
    @BindView(R.id.txMainTask3)
    TextView txMainTask3;
    @BindView(R.id.txMainTask4)
    TextView txMainTask4;
    @BindView(R.id.txMainSubTask1)
    TextView txMainSubTask1;
    @BindView(R.id.txMainSubTask2)
    TextView txMainSubTask2;
    @BindView(R.id.txMainSubTask3)
    TextView txMainSubTask3;
    @BindView(R.id.txMainSubTask4)
    TextView txMainSubTask4;
    @BindView(R.id.txWorkLocation)
    TextView txWorkLocation;
    @BindView(R.id.txWeeks)
    TextView txWeeks;
    @BindView(R.id.txWorkDescription)
    TextView txWorkDescription;
    @BindView(R.id.txViewBids)
    TextView txViewBids;
    @BindView(R.id.txtProDetailsTV)
    TextView txtProDetailsTV;
    @BindView(R.id.imYesHistorical)
    ImageView imYesHistorical;
    @BindView(R.id.imNoHistorical)
    ImageView imNoHistorical;
    @BindView(R.id.imYesInsurance)
    ImageView imYesInsurance;
    @BindView(R.id.imNoInsurance)
    ImageView imNoInsurance;
    @BindView(R.id.imYesRepresentative)
    ImageView imYesRepresentative;
    @BindView(R.id.imNoRepresentative)
    ImageView imNoRepresentative;
    String orderStatus, paymentStatus, serviceName, categoryImage, subCategoryName, workLocation, flexibleWeeks,
            workDescription, isHistoricalStructure, isInsuranceClaim, isRepresentative, taskId;

    String proId, proName, proImage, proEmail, proPhone, proTime, proRating;
    int val = 0;
    SharedPreferences prefv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_task_screen);

        ButterKnife.bind(this);

        prefv = getSharedPreferences("pref", Context.MODE_PRIVATE);

        if (getIntent() != null) {
            orderStatus = getIntent().getStringExtra("orderStatus");
            paymentStatus = getIntent().getStringExtra("paymentStatus");
            serviceName = getIntent().getStringExtra("serviceName");
            categoryImage = getIntent().getStringExtra("categoryImage");
            subCategoryName = getIntent().getStringExtra("subCategoryName");
            workLocation = getIntent().getStringExtra("workLocation");
            flexibleWeeks = getIntent().getStringExtra("flexibleWeeks");
            workDescription = getIntent().getStringExtra("workDescription");
            isHistoricalStructure = getIntent().getStringExtra("isHistoricalStructure");
            isInsuranceClaim = getIntent().getStringExtra("isInsuranceClaim");
            isRepresentative = getIntent().getStringExtra("isRepresentative");
            taskId = getIntent().getStringExtra("taskId");
            proName = getIntent().getStringExtra("proName");
            proImage = getIntent().getStringExtra("proImage");
            proEmail = getIntent().getStringExtra("proEmail");
            proPhone = getIntent().getStringExtra("proPhone");
            proTime = getIntent().getStringExtra("proTime");
            if(getIntent().getStringExtra("proId")!=null) {
                proId = getIntent().getStringExtra("proId");
            }
            if(getIntent().getStringExtra("proRating")!=null) {
                proRating = getIntent().getStringExtra("proRating");
            }
        }

        if (orderStatus.equals("Complete")) {
            txtProDetailsTV.setVisibility(View.VISIBLE);
            txViewBids.setVisibility(View.GONE);
            Glide.with(mActivity)
                    .load(R.drawable.ic_green_radio_btn)
                    .into(imTask4);
            txMainTask4.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
            txMainSubTask4.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
        } else if (orderStatus.equals("Pending")) {
            txViewBids.setVisibility(View.GONE);
            txtProDetailsTV.setVisibility(View.VISIBLE);
        }

        setUpViewS();
        setClicks();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        String message = prefv.getString(taskId, "");
        if (message.equals("bidSuccess")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_green_check_new)
                    .into(imTask2);
            txMainTask2.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlack));
            txMainSubTask2.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlack));
            Glide.with(mActivity)
                    .load(R.drawable.ic_green_radio_btn)
                    .into(imTask3);
        }
    }

    private void setClicks() {
        llyBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txViewBids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeApi();
            }
        });

        txtProDetailsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openProDetailsActivity();
            }
        });
    }

    public void openProDetailsActivity() {
        Intent mIntent = new Intent(this, ProDetailsActivity.class);
        mIntent.putExtra("proId", proId);
        mIntent.putExtra("proName", proName);
        mIntent.putExtra("proImage", proImage);
        mIntent.putExtra("proPhone", proPhone);
        mIntent.putExtra("proEmail", proEmail);
        mIntent.putExtra("proTime", proTime);
        mIntent.putExtra("proRating", proRating);
        startActivity(mIntent);
    }

    private void setUpViewS() {
        //  try {
        if (!categoryImage.equals("")) {
            Glide.with(mActivity)
                    .load(categoryImage)
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(imServiceImage);
        } else {
            // holder.imgItemIV.setImageResource(R.drawable.ic_pp);
            SharedPreferences preference = mActivity.getSharedPreferences("LeedGo", Context.MODE_PRIVATE);

            Set<String> set = preference.getStringSet("myKey", null);
            List<String> sample = new ArrayList<String>(set);
            Glide.with(mActivity)
                    .load(sample.get(new Random().nextInt(sample.size())))
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(imServiceImage);

        }

        txServiceName.setText(serviceName);
        txServiceNameType.setText(subCategoryName);
        try {
            val = Integer.parseInt(paymentStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (paymentStatus != null && val == 0) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_uncheck_green_radio_btn)
                    .into(imTask4);
            txMainTask4.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
            txMainSubTask4.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));

        } else {
            Glide.with(mActivity)
                    .load(R.drawable.ic_green_radio_btn)
                    .into(imTask4);
            txMainTask4.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlack));
            txMainSubTask4.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlack));

        }

        isVlidate();

        txWorkLocation.setText(workLocation);
        txWeeks.setText(flexibleWeeks);
        txWorkDescription.setText(workDescription);


        if (isHistoricalStructure.trim().equals("no")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_uncheck_green_radio_btn)
                    .into(imYesHistorical);


        } else {
            Glide.with(mActivity)
                    .load(R.drawable.ic_uncheck_green_radio_btn)
                    .into(imNoHistorical);
        }
        if (isRepresentative.trim().equals("representative")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_uncheck_green_radio_btn)
                    .into(imNoRepresentative);
        } else {
            Glide.with(mActivity)
                    .load(R.drawable.ic_uncheck_green_radio_btn)
                    .into(imYesRepresentative);
        }
        if (isInsuranceClaim.trim().equals("no")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_uncheck_green_radio_btn)
                    .into(imYesInsurance);
        } else {
            Glide.with(mActivity)
                    .load(R.drawable.ic_uncheck_green_radio_btn)
                    .into(imNoInsurance);
        }
    }

    private void isVlidate() {
        switch (orderStatus) {
            case "Not Assigned":
                perfrom1();
                break;
            case "Pending":
                perfrom2();
                break;
            case "Complete":
                perfrom3();
                break;

        }
    }

    private void perfrom3() {
        Glide.with(mActivity)
                .load(R.drawable.ic_green_radio_btn)
                .into(imTask4);
        txMainTask4.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
        txMainSubTask4.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
    }


    private void perfrom1() {
        Glide.with(mActivity)
                .load(R.drawable.ic_green_radio_btn)
                .into(imTask2);
        txMainTask2.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
        txMainSubTask2.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
        Glide.with(mActivity)
                .load(R.drawable.ic_uncheck_green_radio_btn)
                .into(imTask3);
        txMainTask3.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
        txMainSubTask3.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
    }

    private void perfrom2() {
        Glide.with(mActivity)
                .load(R.drawable.ic_green_radio_btn)
                .into(imTask3);
        txMainTask3.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
        txMainSubTask3.setTextColor(ContextCompat.getColor(mActivity, R.color.colorTextHint));
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamnewUpdate() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        mMap.put("task_id", taskId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.showBidList(mParamnewUpdate()).enqueue(new Callback<BidListModel>() {
            @Override
            public void onResponse(Call<BidListModel> call, Response<BidListModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                BidListModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    // showToast(mActivity, mModel.getMessage());
                    //finish();
                    Intent intent = new Intent(mActivity, ViewBidsUserActivity.class);
                    intent.putExtra("serviceName", serviceName);
                    intent.putExtra("categoryImage", categoryImage);
                    intent.putExtra("subCategoryName", subCategoryName);
                    intent.putExtra("workLocation", workLocation);
                    intent.putExtra("taskId", taskId);
                    startActivity(intent);
                } else {
                    showAlertDialog(mActivity, "No bids received yet!");
                }
            }

            @Override
            public void onFailure(Call<BidListModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


}