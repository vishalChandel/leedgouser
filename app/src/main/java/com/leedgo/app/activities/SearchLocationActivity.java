package com.leedgo.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgo.app.R;
import com.leedgo.app.adapters.SearchLocationAdapter;
import com.leedgo.app.interfaces.LocationInterface;
import com.leedgo.app.models.LocationChangeModel;
import com.leedgo.app.models.ServiceAreaModel;
import com.leedgo.app.models.locationlatlong.Location;
import com.leedgo.app.models.locationlatlong.PlaceLatLongModel;
import com.leedgo.app.models.locations.EditLocationModel;
import com.leedgo.app.models.locations.PredictionsItem;
import com.leedgo.app.models.locations.SearchLocationModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchLocationActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = SearchLocationActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SearchLocationActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.closeRL)
    RelativeLayout closeRL;
    String state = "";
    String zipcode = "";
    String client_location_id;
    String client_id, address2, latitude2, longitude2, location_title, state2, zip_code2;
    //  LocationChangeModel locationChangeModel;
    /*
     * Initialize...
     * */
    ArrayList<PredictionsItem> mLocationArrayLists = new ArrayList<>();


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);

        ButterKnife.bind(this);
        searchWatchListner();
    }

    private void searchWatchListner() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    closeRL.setVisibility(View.VISIBLE);
                    getSearchData();
                } else {
                    try {
                        mLocationArrayLists.clear();
                        setAdapter();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    closeRL.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getSearchData();
                    return true;
                }
                return false;
            }
        });

    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.backRL, R.id.closeRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.closeRL:
                performCloseClick();
                break;
        }
    }

    private void performCloseClick() {
        editSearchET.setText("");
        mLocationArrayLists.clear();
        setAdapter();
    }

    private void getSearchData() {
        if (isNetworkAvailable(mActivity))
            executeSearchApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeSearchApi() {
        mLocationArrayLists.clear();
        // showProgressDialog(mActivity);
        String mSearhText = editSearchET.getText().toString().trim();
        String mApiUrl = Constants.GOOGLE_PLACES_SEARCH + "input=" + mSearhText + "&radius=15000" + "&key=" + getString(R.string.google_places_key);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getSearchPlacesRequest(mApiUrl).enqueue(new Callback<SearchLocationModel>() {
            @Override
            public void onResponse(Call<SearchLocationModel> call, Response<SearchLocationModel> response) {
                dismissProgressDialog();
                if (response.code() == 200) {
                    if (response.body().getPredictions().size() > 0) {
                        mLocationArrayLists.addAll(response.body().getPredictions());
                        setAdapter();
                    } else {
                        showToast(mActivity, getString(R.string.city_not_found));
                    }
                } else if (response.code() == 500) {
                    showToast(mActivity, getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<SearchLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    public void setAdapter() {
        SearchLocationAdapter mSearchLocationAdapter = new SearchLocationAdapter(mActivity, mLocationArrayLists, mLocationInterface);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerViewRV.setLayoutManager(mLayoutManager);
        mRecyclerViewRV.setAdapter(mSearchLocationAdapter);
    }


    LocationInterface mLocationInterface = new LocationInterface() {
        @Override
        public void getSelectedLocation(PredictionsItem mModel) {
            executePlaceLatLongApi(mModel.getPlaceId(), mModel.getDescription(), mModel.getStructuredFormatting().getMainText());
        }
    };

    private void executePlaceLatLongApi(String placeId, String address, String city) {
        showProgressDialog(mActivity);
        String mApiUrl = Constants.GOOGLE_PLACES_LAT_LONG + placeId + "&key=" + getString(R.string.google_places_key);
        Log.e(TAG, "**Google Places Api**" + mApiUrl);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getLatLongFromPlaceId(mApiUrl).enqueue(new Callback<PlaceLatLongModel>() {
            @Override
            public void onResponse(Call<PlaceLatLongModel> call, Response<PlaceLatLongModel> response) {
                dismissProgressDialog();
                if (response.code() == 200) {
                    if (response.body().getResult().getGeometry() != null) {
                        if (response.body().getResult().getGeometry().getLocation() != null) {
                            Location mModel = response.body().getResult().getGeometry().getLocation();
                            ServiceAreaModel mServiceAreaModel = new ServiceAreaModel();
                            mServiceAreaModel.setAddress(address);
                            mServiceAreaModel.setCity(city);
                            mServiceAreaModel.setLatitude("" + mModel.getLat());
                            mServiceAreaModel.setLongitude("" + mModel.getLng());
                            final Geocoder gcd = new Geocoder(getApplicationContext());
                            try {
                                List<Address> addresses = gcd.getFromLocation(mModel.getLat(), mModel.getLng(), 10);
                                for (Address address : addresses) {
                                    if(address.getLocality()!=null && address.getPostalCode()!=null){
                                        state=address.getAdminArea();
                                        zipcode=address.getPostalCode();
                                        Log.d("Locality",address.getAdminArea());
                                        Log.d("PostalCode",address.getPostalCode());
                                        break;
                                    }
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            if (response.body().getResult().getFormatted_address().contains(",")) {
                                String mArray[] = response.body().getResult().getFormatted_address().split(" ");
                                Log.e(TAG, "onResponse: " + response.body().getResult());
                                if (mArray.length >= 2) {
//                                    String state = "";
//                                    String zipcode = "";
                                    try {
                                        String stateZipCode = mArray[mArray.length - 2];
                                        String stateZipArray[] = stateZipCode.split(", ");
                                        boolean result = stateZipArray[1].matches("[0-9]");
//                                        if (result == true) {
//                                            state = stateZipArray[1];
//                                            zipcode = stateZipArray[2];
//                                        } else {
//                                            state = stateZipArray[1];
//                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    mServiceAreaModel.setState(state);
                                    mServiceAreaModel.setZipcode(zipcode);
                                    Intent mIntent = new Intent();
                                    mIntent.putExtra(Constants.MODEL, mServiceAreaModel);
                                    setResult(Constants.REQUEST_CODE, mIntent);
                                    finish();

                                } else {
                                    showToast(mActivity, getString(R.string.state_and_zip));
                                }
                            } else {
                                showToast(mActivity, getString(R.string.state_and_zip));
                            }


                        }
                    }
                } else if (response.code() == 500) {
                    dismissProgressDialog();
                    showToast(mActivity, getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<PlaceLatLongModel> call, Throwable t) {
                dismissProgressDialog();
                showToast(mActivity, getString(R.string.some_thing_went_wrong));
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private Map<String, String> mParamEdit() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", client_location_id);
        mMap.put("client_id", getUserId());
        mMap.put("address", address2);
        mMap.put("latitude", latitude2);
        mMap.put("longitude", longitude2);
        mMap.put("location_title", location_title);
        mMap.put("state", state2);
        mMap.put("zip_code", zip_code2);

        return mMap;
    }

    private void executeEditLocApi() {

        Log.e("Dataq2", "**RESPONSE**" + mParamEdit().toString());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeEditLocationData(getAuthKey(), mParamEdit()).enqueue(new Callback<EditLocationModel>() {
            @Override
            public void onResponse(Call<EditLocationModel> call, Response<EditLocationModel> response) {
                EditLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    finish();
                    Log.e("Test", "Success");
                } else {
                    try {
                        showAlertDialog(mActivity, response.body().getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<EditLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }

}