package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leedgo.app.R;
import com.leedgo.app.adapters.ServicesAdapter2;
import com.leedgo.app.adapters.ServicesSingleAdapter;
import com.leedgo.app.models.ServicesDataModel;
import com.leedgo.app.models.ServicesModel;
import com.leedgo.app.models.ServicesSubCatData;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeeSingleService extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SeeSingleService.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity =SeeSingleService.this;



    /*
     * Widgets
     * */
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.backRL)
    RelativeLayout backRl;

    ArrayList<ServicesDataModel> mServicesArrayList = new ArrayList<>();
    ArrayList<ServicesSubCatData> mSelectedServicesAL = new ArrayList<>();
    /*
     * Initialize...
     * */
    String strDeviceToken = "";
    String serviceType="";
    String serviceImage="";
    String serviceSingle="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, getString(R.string.select_a_service));
        if (getIntent()!=null)
        {
            serviceType=getIntent().getStringExtra("serviceType");
            serviceImage=getIntent().getStringExtra("serviceImage");
            try {
                serviceSingle=getIntent().getStringExtra("serviceSingle");
                if (serviceSingle.equals("yes")) {
                    setToolbarText(txtHeaderTV, serviceType);
                }
                else
                {
                    setToolbarText(txtHeaderTV,getString(R.string.select_a_service));
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            serviceSingle=getIntent().getStringExtra("serviceSingle");
            mSelectedServicesAL = (ArrayList<ServicesSubCatData>) getIntent().getSerializableExtra(Constants.LIST);
        }
        getServicesData();

        backRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private void getServicesData() {
        if (isNetworkAvailable(mActivity))
            executeServicesRequest();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }
    private void executeServicesRequest() {
        mServicesArrayList.clear();
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getServicesRequest().enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ServicesModel mModel = response.body();
                if (mModel.getStatus() == 1) {


                                mServicesArrayList.addAll(response.body().getData());

                            setAdapter();



                  //  if (response.body().getData().get())

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }
    private void setAdapter() {

        ServicesSingleAdapter mSearchLocationAdapter = new ServicesSingleAdapter(mActivity,mServicesArrayList,mSelectedServicesAL,serviceType,serviceImage);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerViewRV.setLayoutManager(mLayoutManager);
        mRecyclerViewRV.setAdapter(mSearchLocationAdapter);
    }

}