package com.leedgo.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.models.RoomCreateModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.LeedgoPrefrences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskDetailsActivity extends BaseActivity {

    Activity mActivity = TaskDetailsActivity.this;

    @BindView(R.id.llyBack)
    LinearLayout llyBack;

    @BindView(R.id.txtUserNameTV)
    TextView txtUserNameTV;

    @BindView(R.id.txtEmailTV)
    TextView txtEmailTV;

    @BindView(R.id.imageIV)
    ImageView imageIV;

    @BindView(R.id.txtMessageTV)
    TextView txtMessageTV;

    @BindView(R.id.imServiceImage)
    ImageView imServiceImage;

    @BindView(R.id.txServiceName)
    TextView txServiceName;

    @BindView(R.id.txServiceNameType)
    TextView txServiceNameType;

    @BindView(R.id.txDateTV)
    TextView txDateTV;

    String providerID = "", provider_name = "", provider_image = " ", provider_email = "",
            service_name = "", sub_category_name = "", work_date = "", category_image = "";

    String formattedDate;

    SharedPreferences prefv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);

        ButterKnife.bind(this);

        prefv = getSharedPreferences("pref", Context.MODE_PRIVATE);

        if (getIntent() != null) {
            providerID = getIntent().getStringExtra("providerID");
            provider_name = getIntent().getStringExtra("provider_name");
            provider_image = getIntent().getStringExtra("provider_image");
            provider_email = getIntent().getStringExtra("provider_email");
            service_name = getIntent().getStringExtra("service_name");
            sub_category_name = getIntent().getStringExtra("sub_category_name");
            work_date = getIntent().getStringExtra("work_date");
            category_image = getIntent().getStringExtra("category_image");

            Glide.with(mActivity).load(category_image).into(imServiceImage);

            if (!provider_image.equals("")) {
                Glide.with(mActivity).load(provider_image).into(imageIV);
            } else {
                imageIV.setImageResource(R.drawable.ic_person);
            }
        }

        setClicks();
        setDataOnWidgets();
    }

    private void setDataOnWidgets() {
        txtUserNameTV.setText(provider_name);
        txtEmailTV.setText(provider_email);
        txServiceName.setText(service_name);
        txServiceNameType.setText(sub_category_name);
        txDateTV.setText(work_date);
    }

    private void setClicks() {
        llyBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtMessageTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeMessage();
            }
        });
    }

    /*
     * Execute create room api
     * */
    private Map<String, String> mMessageParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", LeedgoPrefrences.readString(mActivity, LeedgoPrefrences.USER_ID, ""));
        mMap.put("provider_id", providerID);
        Log.e("Room", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeMessage() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createRoom(mMessageParam()).enqueue(new Callback<RoomCreateModel>() {
            @Override
            public void onResponse(Call<RoomCreateModel> call, Response<RoomCreateModel> response) {
                dismissProgressDialog();

                RoomCreateModel mModel = response.body();

                if (mModel.getStatus() == 1) {

                    Intent intent = new Intent(mActivity, ChatActivity.class);
                    intent.putExtra("room_id", mModel.getRoom_id());
                    intent.putExtra("name", provider_name);
                    mActivity.startActivity(intent);

                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RoomCreateModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(String.valueOf(mActivity), "**ERROR**" + t.getMessage());
            }
        });
    }
}