package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.models.CardData;
import com.leedgo.app.models.CardDetailModel;
import com.leedgo.app.models.CheckModel;
import com.leedgo.app.models.PaymentModel;
import com.leedgo.app.models.PaymentSendModel;
import com.leedgo.app.models.StripeAccountModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity1 extends BaseActivity {

    Activity mActivity = PaymentActivity1.this;

    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.btnPay)
    Button btnPay;
    @BindView(R.id.txEdit)
    TextView txEdit;
    @BindView(R.id.txServiceName)
    TextView txServiceName;
    @BindView(R.id.txServiceId)
    TextView txServiceId;
    @BindView(R.id.txServiceDate)
    TextView getTxServiceDate;
    @BindView(R.id.imServiceImage)
    ImageView imageView;
    @BindView(R.id.txServicePrice)
    TextView txServicePrice;
    @BindView(R.id.txCardNo)
    TextView txCardno;

    String card_id = null;
    String update_cardornot;
    String card_type;
    String cardyear;
    String cardmonth;
    String cardholdername;
    String cardnumber;
    String stripe_token;
    String serviceTime;
    String serviceId;
    String serviceName;
    String servicePrice;
    String cardNO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment1);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, "Payment");
        if (getIntent() != null) {
            stripe_token = getIntent().getStringExtra(Constants.TOKEN);
            cardNO = getIntent().getStringExtra(Constants.CARD_NO);
            Log.i("StripToken",stripe_token);
            String mLastFourDigits = null;
            if(cardNO.length()>4) {
                mLastFourDigits = cardNO.substring(cardNO.length() - 4);
                txCardno.setText("**** **** **** " + mLastFourDigits);
            }else {
                txCardno.setText("**** **** **** " + cardNO);
            }

        }
        executePaymentapi();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamnewUpdate() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executePaymentapi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executePaymentStatus(mParamnewUpdate()).enqueue(new Callback<PaymentModel>() {
            @Override
            public void onResponse(Call<PaymentModel> call, Response<PaymentModel> response) {
                dismissProgressDialog();
                PaymentModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    dismissProgressDialog();
                    serviceName = mModel.getData().getCategory_name();
                    serviceTime = mModel.getData().getTask_complete_date();
                    serviceId = mModel.getData().getUnique_task_id();
                    Log.e(TAG, "TASK_ID::" + serviceId);
                    servicePrice = mModel.getData().getAssigned_amount();
                    txServiceName.setText(serviceName);
                    txServicePrice.setText("$ " + servicePrice + ".00");
                    //  btnPay.setText("Pay " + "$" + servicePrice);
                    txServiceId.setText("Service ID: " + serviceId);
                    Glide.with(mActivity).load(mModel.getData().getCategory_image()).placeholder(R.drawable.ic_pp).into(imageView);
                    try {
                        Calendar calendar = Calendar.getInstance();
                        TimeZone tz = TimeZone.getDefault();
                        calendar.setTimeInMillis(Long.parseLong(serviceTime) * 1000);
                        calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                        Date currenTimeZone = (Date) calendar.getTime();
                        getTxServiceDate.setText("Completed on: " + sdf.format(currenTimeZone));
                    } catch (Exception e) {
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<PaymentModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @OnClick({R.id.backRL, R.id.txEdit, R.id.btnPay})
    public void onViewCLicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                finish();
                break;
            case R.id.btnPay:
                performPay();
                break;
            case R.id.txEdit:
                performSwitch();
                break;


        }
    }

    private void performSwitch() {
        txEdit.setClickable(false);

        if (txEdit.getText().equals("Edit")) {
            Intent mIntent = new Intent(mActivity, PaymentActivity2.class);
            mIntent.putExtra("card_id", card_id);
            mIntent.putExtra("update_cardornot", "update");
            mIntent.putExtra("card_type", card_type);
            mIntent.putExtra("cardyear", cardyear);
            mIntent.putExtra("cardmonth", cardmonth);
            mIntent.putExtra("cardholdername", cardholdername);
            mIntent.putExtra("cardnumber", cardnumber);
            startActivity(mIntent);
        } else {
            startActivity(new Intent(this, PaymentActivity2.class)
                    .putExtra("update_cardornot", "add"));
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                txEdit.setClickable(true);
            }
        }, 1500);

    }

    private void performPay() {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        btnPay.startAnimation(myAnim);
        executeSendPaymentApi();

    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString()+"::"+getAuthKey());
        return mMap;
    }

    private void executeCardRequest() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getClientDetailsCard(getAuthKey(), mParam()).enqueue(new Callback<CardDetailModel>() {
            @Override
            public void onResponse(Call<CardDetailModel> call, Response<CardDetailModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                CardDetailModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    for (int i = 0; i <= mModel.getData().size(); i++) {
                        if (mModel.getData().get(i).getIsPrimary().equals(1)) {
                            card_id = mModel.getData().get(i).getCard_id();
                            update_cardornot = "update";
                            card_type = mModel.getData().get(i).getBrand().toLowerCase();
                            cardyear = mModel.getData().get(i).getExp_year();
                            cardmonth = mModel.getData().get(i).getExp_month();
                            cardholdername = mModel.getData().get(i).getCard_holder_name();
                            cardnumber = mModel.getData().get(i).getCard_last4();
                            // txCardno.setText("**** **** ****" + cardnumber);

                        }
                    }
//                    txCardno.setText("**** **** ****" + cardNO);
                    if (card_id == null) {
                        txEdit.setText("Add");
                    } else {
                        txEdit.setText("Edit");

                    }
                } else {
                    txEdit.setText("Add");
                    showAlertDialog(mActivity, "No cards added");
                }
            }

            @Override
            public void onFailure(Call<CardDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private Map<String, String> mParamP() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("unique_task_id", serviceId);
        mMap.put("user_id", getUserId());
        mMap.put("stripeToken", stripe_token);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSendPaymentApi() {
          showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.sendPayment(mParamP()).enqueue(new Callback<PaymentSendModel>() {
            @Override
            public void onResponse(Call<PaymentSendModel> call, Response<PaymentSendModel> response) {
                dismissProgressDialog();
                CheckModel model;
                PaymentSendModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    startActivity(new Intent(PaymentActivity1.this, PaymentSuccessfulActivity.class));
                    finish();
                }
                else {
                    showToast(mActivity,"Payment failed.");
                }
            }

            @Override
            public void onFailure(Call<PaymentSendModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


}