package com.leedgo.app.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.leedgo.app.R;
import com.leedgo.app.fonts.EditTextMedium;
import com.leedgo.app.models.GetClientLocationModel;
import com.leedgo.app.models.profile.ProfileModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.ImageUtil;
import com.leedgo.app.utils.LeedgoPrefrences;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    /**
     * Getting the Current Class Name
     */
    String TAG = EditProfileActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = EditProfileActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.imgProfileCIV)
    CircleImageView imgProfileCIV;
    @BindView(R.id.txtChangePicTV)
    TextView txtChangePicTV;
    @BindView(R.id.txtLocationsTV)
    TextView txtLocationsTV;
    @BindView(R.id.editNameET)
    EditText editNameET;
    @BindView(R.id.txtEmailTV)
    TextView txtEmailTV;
    @BindView(R.id.editPhoneET)
    EditText edPhoneEt;

    @BindView(R.id.btnSaveB)
    Button btnSaveB;
    @BindView(R.id.txtEditPwdTV)
    TextView txtEditPwdTV;


    /*
     * Initialize Objects...
     * */
    Bitmap mBitmap = null;
    ProfileModel mProfileModel;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, getString(R.string.edit_profile_your));
        setDataOnWidgets();
        getProfileData();

    }


    @Override
    protected void onResume() {
        super.onResume();
        executeLocationRequest();

    }

    private void getProfileData() {
        if (isNetworkAvailable(mActivity))
            executeProfileDataApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeProfileDataApi() {
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUserDataRequest(getAuthKey(), mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                try {
                    dismissProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                mProfileModel = response.body();
                if (mProfileModel.getStatus() == 1) {
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_ID, mProfileModel.getData().getId());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_NAME, mProfileModel.getData().getName());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_EMAIL, mProfileModel.getData().getEmail());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_PROFILE_PIC, mProfileModel.getData().getProfilePic());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.CELL_NO, mProfileModel.getData().getCellno());
                    setDataOnWidgets();

                } else {
                    showToast(mActivity, mProfileModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setDataOnWidgets() {
        if (getUserProfilePic() != null) {
            Glide.with(mActivity)
                    .asBitmap()
                    .load(getUserProfilePic())
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            if (resource != null) {
                                imgProfileCIV.setImageBitmap(resource);
                                mBitmap = resource;
                            }
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                        }
                    });
        } else {
            imgProfileCIV.setImageResource(R.drawable.ic_person);
        }
        if (getUserEmail() != null) {
            txtEmailTV.setText(getUserEmail());
        }
        if (getUserName() != null) {
            editNameET.setText(getUserName());
            editNameET.setSelection(getUserName().length());
        }
        if (getCellno() != null) {
            edPhoneEt.setText(getCellno());
        }

    }


    /*
     * Widgets Click Listener
     * */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.backRL, R.id.txtChangePicTV, R.id.txtLocationsTV, R.id.txtEditPwdTV, R.id.btnSaveB, R.id.imgProfileCIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.txtChangePicTV:
                performChangeProfileClick();
                break;
            case R.id.txtLocationsTV:
                performLocationClick();
                break;

            case R.id.txtEditPwdTV:
                performEditPwdClick();
                break;
            case R.id.btnSaveB:
                performSaveClick();
                break;
            case R.id.imgProfileCIV:
                performChangeProfileClick();
                break;
        }
    }

    private void performLocationClick() {
        LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.EDITORADDLOCATION, "add");
        startActivity(new Intent(mActivity, AddLocationsActivity.class));
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void performChangeProfileClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private void performEditPwdClick() {
        startActivity(new Intent(mActivity, ChangePasswordActivity.class));
    }

    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_name));
            flag = false;
        } else if (edPhoneEt.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter phone number");
            flag = false;
        } else if (edPhoneEt.getText().toString().length() < 10 || edPhoneEt.getText().toString().length() > 10) {
            showAlertDialog(mActivity, "Please enter valid phone number");
            flag = false;
        } else if (txtLocationsTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_your_locations));
            flag = false;
        }
        return flag;
    }


    private void performSaveClick() {

        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSaveB.startAnimation(myAnim);

        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeUpdateProfileData();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }


    /*
     * Execute api update profile
     * */
    private Map<String, String> mUpdateProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", getUserId());
        mMap.put("name", editNameET.getText().toString().trim());
        mMap.put("email", getUserEmail());
        mMap.put("latitude", "");
        mMap.put("longitude", "");
        mMap.put("address", txtLocationsTV.getText().toString().trim());
        mMap.put("profile_image", ImageUtil.convert(mBitmap));
        mMap.put("cellno", edPhoneEt.getText().toString().trim());
        mMap.put("location_id", "");
        mMap.put("state", "");
        mMap.put("location_title", "");
        mMap.put("zip_code", "");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeUpdateProfileData() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.updateUserProfileRequest(getAuthKey(), mUpdateProfileParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                // dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + getAuthKey() + "::::::::::::" + getUserId());
                mProfileModel = response.body();
                if (mProfileModel.getStatus() == 1) {
                    showToast(mActivity, mProfileModel.getMessage().toString());
                    // LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_ID, mProfileModel.getData().getId());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_NAME, editNameET.getText().toString().trim());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_EMAIL, txtEmailTV.getText().toString().trim());

                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.CELL_NO, edPhoneEt.getText().toString().trim());


                    // setDataOnWidgets();
                    finish();
                } else {
                    showAlertDialog(mActivity, mProfileModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Update Profile Picture
     * */
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermission() {
        requestPermissions(new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(50, 50)
                .setRequestedSize(120, 120)
                .setMultiTouchEnabled(false)
                .start(mActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    Log.e(TAG, "onActivityResult: " + result.getUri());
                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 40, out);
                    imgProfileCIV.setImageBitmap(selectedImage);
                    mBitmap = selectedImage;

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamk() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLocationRequest() {
//       showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUserLocationsRequest(getAuthKey(), mParamk()).enqueue(new Callback<GetClientLocationModel>() {
            @Override
            public void onResponse(Call<GetClientLocationModel> call, Response<GetClientLocationModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                GetClientLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    try {
                        txtLocationsTV.setText(mModel.getData().get(mModel.getData().size() - 1).getAddress());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    //  showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetClientLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


}