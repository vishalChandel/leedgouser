package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.leedgo.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentSuccessfulActivity extends BaseActivity {

    @BindView(R.id.btnContinueB)
    Button btnContinueB;
    @BindView(R.id.txDashboard)
    TextView txDashboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_successful);
        ButterKnife.bind(this);
    }

    @SuppressLint("NonConstantResourceId")
    @OnClick({R.id.btnContinueB,R.id.txDashboard})
    public  void  onViewClicked(View view)
    {
        switch (view.getId())
        {
            case  R.id.btnContinueB:
                performClick();
                finish();
            case  R.id.txDashboard:
                performClick2();
                finish();
        }
    }

    private void performClick2() {
        Intent intent=new Intent(this,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    private void performClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        btnContinueB.startAnimation(myAnim);
        Intent intent=new Intent(this,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        performClick();
    }
}