package com.leedgo.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leedgo.app.R;
import com.leedgo.app.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OpenLinksActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = OpenLinksActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = OpenLinksActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.mWebViewWV)
    WebView mWebViewWV;
    @BindView(R.id.txtTextTV)
    TextView txtTextTV;
    @BindView(R.id.txtTextTV2)
    TextView txtTextTV2;
    @BindView(R.id.txtTextTV3)
    TextView txtTextTV3;
    @BindView(R.id.txtTextTV4)
    TextView txtTextTV4;
    @BindView(R.id.txtTextTV5)
    TextView txtTextTV5;
    /*
    * Initialize objects...
    * */
    String mLinkType = "";


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_links);
        ButterKnife.bind(this);
        getIntentData();
    }



    private void getIntentData() {
        if (getIntent() != null) {
            mLinkType = getIntent().getStringExtra(Constants.LINK_TYPE);
            if (mLinkType.equals(Constants.PRIVACY_POLICY)) {
                txtHeaderTV.setText(getString(R.string.privacy_policy));
                txtTextTV.setText(getString(R.string.term_conditions_text_new1));
                txtTextTV2.setText(getString(R.string.term_conditions_text_new2));
                txtTextTV3.setText(getString(R.string.term_conditions_text_new20));
                txtTextTV4.setText(getString(R.string.term_conditions_text_new3));
                txtTextTV5.setText(getString(R.string.term_conditions_text_new30));
            } else if (mLinkType.equals(Constants.TERMS_SERVICES)) {
                txtHeaderTV.setText(getString(R.string.terms_of_services));
                txtTextTV.setText(getString(R.string.term_conditions_text_new1));
                txtTextTV2.setText(getString(R.string.term_conditions_text_new2));
                txtTextTV3.setText(getString(R.string.term_conditions_text_new20));
                txtTextTV4.setText(getString(R.string.term_conditions_text_new3));
                txtTextTV5.setText(getString(R.string.term_conditions_text_new30));
            } else if (mLinkType.equals(Constants.DONT_SELL_DATA)) {
                txtHeaderTV.setText(getString(R.string.dont_sell_my_data));
                txtTextTV.setText(getString(R.string.term_conditions_text_new1));
                txtTextTV2.setText(getString(R.string.term_conditions_text_new2));
                txtTextTV3.setText(getString(R.string.term_conditions_text_new20));
                txtTextTV4.setText(getString(R.string.term_conditions_text_new3));
                txtTextTV5.setText(getString(R.string.term_conditions_text_new30));
            } else if (mLinkType.equals(Constants.ABOUT_US)) {
                txtHeaderTV.setText(getString(R.string.about_leedgo));
                txtTextTV.setText(getString(R.string.term_conditions_text_new1));
                txtTextTV2.setText(getString(R.string.term_conditions_text_new2));
                txtTextTV3.setText(getString(R.string.term_conditions_text_new20));
                txtTextTV4.setText(getString(R.string.term_conditions_text_new3));
                txtTextTV5.setText(getString(R.string.term_conditions_text_new30));
            }
        }
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.backRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }
}