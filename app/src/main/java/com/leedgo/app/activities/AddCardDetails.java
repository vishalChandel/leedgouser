package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.leedgo.app.R;
import com.leedgo.app.models.CheckModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;
import com.leedgo.app.utils.LeedgoPrefrences;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCardDetails extends BaseActivity {
    Activity mActivity = AddCardDetails.this;

    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.textAddedorNot)
    TextView txAddedornot;
    @BindView(R.id.textAddedorNotSub)
    TextView txAddedornotSUb;
    @BindView(R.id.llyVisa)
    LinearLayout llyVisa;
    @BindView(R.id.imVisaChecked)
    ImageView imVisaChecked;
    @BindView(R.id.llyMasterCard)
    LinearLayout llyMastercard;
    @BindView(R.id.imMasterCardChecked)
    ImageView imMasterCardChecked;
    @BindView(R.id.llyPayPal)
    LinearLayout llyPayPal;
    @BindView(R.id.imPayPalChecked)
    ImageView imPayPalChecked;
    @BindView(R.id.edCardHoldername)
    EditText edCardHolderName;
    @BindView(R.id.txCardHoldername)
    TextView txCardHolderName;
    @BindView(R.id.edCardNumber)
    EditText edCardNumber;
    @BindView(R.id.txCardNumber)
    TextView txCardNumber;
    @BindView(R.id.edCvvnumber)
    EditText edCvvNumber;
    @BindView(R.id.txExpirationDate)
    TextView txExpirationDate;
    @BindView(R.id.checkboxSaveCardInfo)
    CheckBox checkBoxSaveCardInfo;
    @BindView(R.id.txTerms)
    TextView txTerms;
    @BindView(R.id.btnSaveB2)
    Button btnSave;
    SingleDateAndTimePickerDialog singleDateAndTimePickerDialog;

    String cardNumber = "", cvc = "", mCardHolderName="";
    String expirayYear = "";
    String expiraydate = "";
    String tokenId = "";
    String expMonth;
    Card card;
    String update_cardornot;
    String cardBrand, cardYear, cardMonth, cardHoldername, cardLstnumber, updateCardId;
    int mYear, mMonth, mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card_details);
        ButterKnife.bind(this);

        setToolbarText(txtHeaderTV, "Add Payment");

        if (LeedgoPrefrences.readString(mActivity, LeedgoPrefrences.CARD_ADD_STATUS, "").equals("true")) {
            txAddedornot.setVisibility(View.GONE);
            txAddedornotSUb.setVisibility(View.GONE);
            txTerms.setVisibility(View.GONE);

        } else {
            txAddedornot.setVisibility(View.VISIBLE);
            txAddedornotSUb.setVisibility(View.VISIBLE);
            txTerms.setVisibility(View.VISIBLE);
            String text = "<font color=#989898 >By Saving a credit card , you agree to our </font> <font color=#069E7C><b>Terms and Privacy Policy</b></font>  <font color=#989898> ,and if you use it to pay for a service you have requested, you authorize your credit card to be charged on a recurring basis untill you cancel, which you can do any time. ";
            txTerms.setText(Html.fromHtml(text));
        }

        if (getIntent() != null) {
            cardYear = getIntent().getStringExtra("cardyear");
            cardMonth = getIntent().getStringExtra("cardmonth");
            if (getIntent().getStringExtra("cardmonth") != null) {
                txtHeaderTV.setText("Edit Card Details");
                txAddedornot.setVisibility(View.GONE);
                txAddedornotSUb.setVisibility(View.GONE);
            }

            cardHoldername = getIntent().getStringExtra("cardholdername");
            cardLstnumber = getIntent().getStringExtra("cardnumber");
            update_cardornot = getIntent().getStringExtra("update_cardornot");
            cardBrand = getIntent().getStringExtra("card_type");
            updateCardId = getIntent().getStringExtra("card_id");

            if (cardYear != null && cardMonth != null) {
                expirayYear = cardYear;
                expMonth = cardMonth;
            }
            // Toast.makeText(mActivity, ""+cardBrand, Toast.LENGTH_SHORT).show();
            if (cardBrand != null) {
                if (cardBrand.equals("visa")) {
                    VisaClick();
                } else {
                    MasterClick();
                }
            } else {
                VisaClick();
            }

            if (cardHoldername != null && cardHoldername.length() > 1) {
                edCardHolderName.setText(cardHoldername);
                edCardHolderName.setVisibility(View.VISIBLE);
                txCardHolderName.setVisibility(View.GONE);
            } else {
                edCardHolderName.setVisibility(View.VISIBLE);
                txCardHolderName.setVisibility(View.GONE);
            }
            if (cardLstnumber != null) {
                edCardNumber.setVisibility(View.GONE);
                txCardNumber.setText("**** **** **** " + cardLstnumber);
                txCardNumber.setVisibility(View.VISIBLE);
                txExpirationDate.setText(cardMonth + "/" + cardYear);
                edCvvNumber.setText("***");
                edCvvNumber.setFocusable(false);
                edCvvNumber.setClickable(false);

            }

        }


        edCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    String d = editable.toString();
                    char firstChar = d.charAt(0);
                    if (firstChar == '4') {
                        VisaClick();
                    } else if (firstChar == '5') {
                        MasterClick();
                    } else {
                        PayPalClick();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @OnClick({R.id.backRL, R.id.llyVisa, R.id.llyMasterCard, R.id.llyPayPal, R.id.txExpirationDate, R.id.btnSaveB2, R.id.txTerms})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.llyVisa:
                VisaClick();
                break;
            case R.id.llyMasterCard:
                MasterClick();
                break;
            case R.id.llyPayPal:
                PayPalClick();
                break;
            case R.id.txExpirationDate:
                txExpirationDate();
                break;
            case R.id.btnSaveB2:
                GetCardCheck();
                break;
            case R.id.txTerms:
                performTermServicesClick();
                break;
        }
    }

    private void performTermServicesClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.TERMS_SERVICES);
        startActivity(mIntent);
    }


    private void GetCardCheck() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSave.startAnimation(myAnim);
        if (getIntent().getStringExtra("cardmonth") != null) {
            executeUpdate();
        } else {
            GetCardData();
        }

    }


    private void GetCardData() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                saveCard();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }

        }
    }

    //validations

    private boolean isValidate() {
        boolean flag = true;
        if (edCardHolderName.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter Card Holder Name");
            flag = false;
        } else if (edCardNumber.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter Card number");
            flag = false;
        } else if (edCardNumber.getText().toString().trim().length() < 15) {
            showAlertDialog(mActivity, "Please enter valid Card number");
            flag = false;
        } else if (edCvvNumber.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter CVV number");
            flag = false;
        } else if (edCvvNumber.getText().toString().trim().length() < 3) {
            showAlertDialog(mActivity, "Please enter valid CVV number");
            flag = false;
        } else if (txExpirationDate.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter card expiration date");
            flag = false;
        } else if (!checkBoxSaveCardInfo.isChecked()) {
            showAlertDialog(mActivity, "Please select the checkbox to add a card");
            flag = false;
        }
        return flag;
    }


    private boolean isValidateU() {
        boolean flag = true;
        if (edCardHolderName.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter Card Holder Name");
            flag = false;
        } else if (txCardNumber.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter Card number");
            flag = false;
        } else if (txCardNumber.getText().toString().trim().length() < 15) {
            showAlertDialog(mActivity, "Please enter valid Card number");
            flag = false;
        } else if (edCvvNumber.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter Cvv number");
            flag = false;
        } else if (edCvvNumber.getText().toString().trim().length() < 3) {
            showAlertDialog(mActivity, "Please enter valid CVV number");
            flag = false;
        } else if (txExpirationDate.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter card expiration date");
            flag = false;
        } else if (!checkBoxSaveCardInfo.isChecked()) {
            showAlertDialog(mActivity, "Please select the checkbox to add a card");
            flag = false;
        }
        return flag;
    }


    private void saveCard() {
        cardNumber = edCardNumber.getText().toString().trim();
        cvc = edCvvNumber.getText().toString().trim();
        mCardHolderName=edCardHolderName.getText().toString();
        card = new Card(cardNumber, Integer.valueOf(expiraydate), Integer.valueOf(expirayYear), cvc,mCardHolderName,"","","","","","","");
        if (card == null) {
            showAlertDialog(mActivity, getString(R.string.The_card_invalid));
        } else {
            if (!card.validateCard()) {
            // Do not continue token creation.
                showAlertDialog(mActivity, getString(R.string.The_card_invalid));
            } else {
                CreateToken(card);
            }
        }
    }

    /*
     * To Get Token
     **/

    private void CreateToken(Card card) {
        showProgressDialog(mActivity);
        Stripe stripe = null;
        try {
            stripe = new Stripe(Constants.publishable_key);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }

        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        // Send token to your server
                        Log.e("Stripe Token", token.getId());
                        tokenId = token.getId();
                        saveData();
                    }

                    public void onError(Exception error) {
                        String rr = error.toString();
                        // Show localized error message
                        showAlertDialog(mActivity, rr);
                        dismissProgressDialog();

                    }
                }
        );
    }

    private void executeUpdate() {
        if (isValidateU()) {
            showProgressDialog(mActivity);
            executeUpdateApi();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamnewUpdate() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("card_id", updateCardId);
        mMap.put("client_id", getUserId());
        mMap.put("exp_year", expirayYear);
        mMap.put("exp_month", expMonth);
        mMap.put("name", edCardHolderName.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeUpdateApi() {
        Log.e("Checkl", mParamnewUpdate().toString());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.updateCard(getAuthKey(), mParamnewUpdate()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                CheckModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("token", tokenId);
        mMap.put("client_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void saveData() {
        Log.e("Checkl", mParam().toString() + "::::::" + getAuthKey());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addCard(getAuthKey(), mParam()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                CheckModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }

    @SuppressLint("ResourceAsColor")
    private void txExpirationDate() {
        try {
            InputMethodManager manager
                    = (InputMethodManager)
                    getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            manager
                    .hideSoftInputFromWindow(
                            txExpirationDate.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Get Current Date
        final Calendar c = Calendar.getInstance();

        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        MonthYearPickerDialog pd = new MonthYearPickerDialog();
        pd.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
//
                SimpleDateFormat DateFor12 = new SimpleDateFormat("MM/yyyy");
                SimpleDateFormat DateFor13 = new SimpleDateFormat("yyyy");
                SimpleDateFormat DateFor14 = new SimpleDateFormat("MM");
                String stringDate = DateFor12.format(calendar.getTime());
                txExpirationDate.setText(stringDate);
                expiraydate = DateFor14.format(calendar.getTime());
                expirayYear = DateFor13.format(calendar.getTime());

            }
        });
        pd.show(getSupportFragmentManager(), "MonthYearPickerDialog");
    }


    private void MasterClick() {
        imMasterCardChecked.setVisibility(View.VISIBLE);
        llyMastercard.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext_red));
        imPayPalChecked.setVisibility(View.INVISIBLE);
        llyPayPal.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_card_unsel));
        imVisaChecked.setVisibility(View.INVISIBLE);
        llyVisa.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_card_unsel));
    }

    private void PayPalClick() {
        imMasterCardChecked.setVisibility(View.INVISIBLE);
        llyMastercard.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_card_unsel));
        imPayPalChecked.setVisibility(View.VISIBLE);
        llyPayPal.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext_red));
        imVisaChecked.setVisibility(View.INVISIBLE);
        llyVisa.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_card_unsel));
    }

    private void VisaClick() {
        imMasterCardChecked.setVisibility(View.INVISIBLE);
        llyMastercard.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_card_unsel));
        imPayPalChecked.setVisibility(View.INVISIBLE);
        llyPayPal.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_card_unsel));
        imVisaChecked.setVisibility(View.VISIBLE);
        llyVisa.setBackgroundDrawable(ContextCompat.getDrawable(mActivity, R.drawable.bg_edittext_red));

    }

    // Return true if the card number is valid
    public static boolean validitychk(long cnumber) {
        return (thesize(cnumber) >= 13 && thesize(cnumber) <= 16) && (prefixmatch(cnumber, 4)
                || prefixmatch(cnumber, 5) || prefixmatch(cnumber, 37) || prefixmatch(cnumber, 6))
                && ((sumdoubleeven(cnumber) + sumodd(cnumber)) % 10 == 0);
    }

    // Get the result from Step 2
    public static int sumdoubleeven(long cnumber) {
        int sum = 0;
        String num = cnumber + "";
        for (int i = thesize(cnumber) - 2; i >= 0; i -= 2)
            sum += getDigit(Integer.parseInt(num.charAt(i) + "") * 2);
        return sum;
    }

    // Return this cnumber if it is a single digit, otherwise,
    // return the sum of the two digits
    public static int getDigit(int cnumber) {
        if (cnumber < 9)
            return cnumber;
        return cnumber / 10 + cnumber % 10;
    }

    // Return sum of odd-place digits in cnumber
    public static int sumodd(long cnumber) {
        int sum = 0;
        String num = cnumber + "";
        for (int i = thesize(cnumber) - 1; i >= 0; i -= 2)
            sum += Integer.parseInt(num.charAt(i) + "");
        return sum;
    }

    // Return true if the digit d is a prefix for cnumber
    public static boolean prefixmatch(long cnumber, int d) {
        return getprefx(cnumber, thesize(d)) == d;
    }

    // Return the number of digits in d
    public static int thesize(long d) {
        String num = d + "";
        return num.length();
    }

    // Return the first k number of digits from
    // number. If the number of digits in number
    // is less than k, return number.
    public static long getprefx(long cnumber, int k) {
        if (thesize(cnumber) > k) {
            String num = cnumber + "";
            return Long.parseLong(num.substring(0, k));
        }
        return cnumber;
    }
}
