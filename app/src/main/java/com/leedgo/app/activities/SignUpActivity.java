package com.leedgo.app.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.leedgo.app.R;
import com.leedgo.app.models.SignInModel;
import com.leedgo.app.models.SignUpModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.Constants;
import com.leedgo.app.utils.LeedgoPrefrences;

import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.editFullNameET)
    EditText editFullNameET;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPhoneET)
    EditText editPhoneEt;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.editConfirmPasswordET)
    EditText editConfirmPasswordET;
    @BindView(R.id.btnGetStartedB)
    Button btnGetStartedB;
    @BindView(R.id.btnGoogleB)
    Button btnGoogleB;
    @BindView(R.id.btnFacebookB)
    Button btnFacebookB;
    @BindView(R.id.txtSignInTV)
    TextView txtSignInTV;
    @BindView(R.id.txtTermsServicesTV)
    TextView txtTermsServicesTV;
    @BindView(R.id.txtPrivacyPolicyTV)
    TextView txtPrivacyPolicyTV;
    @BindView(R.id.txtDontSellTV)
    TextView txtDontSellTV;

    /*
     * Initialize...
     * */
    String strDeviceToken = "";
    /*
     * Activity Override method
     * #onActivityCreated
     * */
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 1;
    String google_id,gooogle_name,google_email,google_image;
    private String fbEmail, fbLastName, fbFirstName, fbId, userName, fbSocialUserserName;
    String FacebookProfilePicture = "";
    String fb_Username = "";
    String facebook_id = "";
    String fb_email = "";
    private URL fbProfilePicture;
    CallbackManager mCallbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        getDeviceToken();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString( R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


    }

    private void getDeviceToken() {

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {

                    if (!task.isSuccessful()) {
                        //handle token error
                        Log.e(TAG, "**Get Instance Failed**", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    strDeviceToken = task.getResult();
                    Log.e(TAG, "**Push Token**" + strDeviceToken);

                });
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.btnGetStartedB, R.id.btnGoogleB, R.id.btnFacebookB, R.id.txtSignInTV,
            R.id.txtTermsServicesTV,R.id.txtPrivacyPolicyTV,R.id.txtDontSellTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnGetStartedB:
                performGetStartedClick();
                break;
            case R.id.btnGoogleB:
                performGoogleLoginClick();
                break;
            case R.id.btnFacebookB:
                performFacebookLoginClick();
                break;
            case R.id.txtSignInTV:
                performSignUpClick();
                break;
            case R.id.txtTermsServicesTV:
                performTermServicesClick();
                break;
            case R.id.txtPrivacyPolicyTV:
                performPrivacyPolicyClick();
                break;
            case R.id.txtDontSellTV:
                performDontSellClick();
                break;
        }
    }

    private void performGetStartedClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnGetStartedB.startAnimation(myAnim);

        if (isValidate()){
            if (isNetworkAvailable(mActivity))
                executeSignUpApi();
            else
                showToast(mActivity,getString(R.string.internet_connection_error));
        }
    }



    private void performGoogleLoginClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnGoogleB.startAnimation(myAnim);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void performFacebookLoginClick() {
//        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
//        btnFacebookB.startAnimation(myAnim);
        performFbLogin();
    }

    private void performSignUpClick() {
        startActivity(new Intent(mActivity, SignInActivity.class));
        finish();
    }

    private void performTermServicesClick() {
        Intent mIntent = new Intent(mActivity,OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE,Constants.TERMS_SERVICES);
        startActivity(mIntent);
    }

    private void performPrivacyPolicyClick() {
        Intent mIntent = new Intent(mActivity,OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE,Constants.PRIVACY_POLICY);
        startActivity(mIntent);
    }

    private void performDontSellClick() {
        Intent mIntent = new Intent(mActivity,OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE,Constants.DONT_SELL_DATA);
        startActivity(mIntent);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity, SignInActivity.class));
        finish();
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editFullNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_fullname));
            flag = false;
        }else if (editEmailET.getText().toString().trim().equals("")){
            showAlertDialog(mActivity, getString(R.string.please_enter_your_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity,getString(R.string.please_enter_your_valid_email_address));
            flag = false;
        }
        else if (editPhoneEt.getText().toString().trim().equals("")){
            showAlertDialog(mActivity, "Please enter phone number");
            flag = false;
        } else if (editPhoneEt.getText().toString().length()<10||editPhoneEt.getText().toString().length()> 10) {
            showAlertDialog(mActivity,"Please enter valid phone number");
            flag = false;
        }

        else if (editPasswordET.getText().toString().trim().equals("")){
            showAlertDialog(mActivity,getString(R.string.please_enter_password));
            flag = false;
        }  else if (editPasswordET.getText().toString().trim().length()<8){
            showAlertDialog(mActivity,"Password should contain minimum 8 characters");
            flag = false;
        }
        else if (editConfirmPasswordET.getText().toString().trim().equals("")){
            showAlertDialog(mActivity,getString(R.string.please_enter_confirm_password));
            flag = false;
        }else if (!editPasswordET.getText().toString().trim().equals(editConfirmPasswordET.getText().toString().trim())){
            showAlertDialog(mActivity,"Passwords are not matching. Please try again.");
            flag = false;
        }
        return flag;
    }



    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("name", editFullNameET.getText().toString().trim());
        mMap.put("email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("cellno",editPhoneEt.getText().toString().trim());
        mMap.put("device_token", strDeviceToken);
        mMap.put("device_type", "ANDROID");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeSignUpApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signUpRequest(mParam()).enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignUpModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.IS_LOGIN, true);
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_ID, mModel.getId());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.AUTH_KEY, mModel.getAuthKey());

                    LeedgoPrefrences.writeString(mActivity,LeedgoPrefrences.CELL_NO,editPhoneEt.getText().toString().trim());
                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,@Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mCallbackManager!=null){
            Log.e("Check", "2");
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignIn.getClient(mActivity,
                    new GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN).build()
            ).signOut();
            Log.e("Check","3");
            Task <GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {

                Log.e("Check","4");
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                if (result.isSuccess()) {
// Google Sign In was successful, save Token and a state then authenticate with Firebase
                    GoogleSignInAccount account1 = result.getSignInAccount();
                    gooogle_name = account1.getDisplayName();
                    google_email = account1.getEmail();
                    google_id=account1.getId();
                    Uri photoUri = account1.getPhotoUrl();
                    google_image = photoUri.toString();
                    Log.e("Check","40"+account1.getId()+" token:"+account1.getIdToken());
                    GoogleSignInAccount account = task.getResult( ApiException.class);
                    //  Toast.makeText(mActivity, "Successssghsvs ", Toast.LENGTH_SHORT).show();
                    // firebaseAuthWithGoogle(account);
                    executeGoogleSignInApi();
                }
                else {
                    Log.e("Check","41");
                }
            } catch (ApiException e) {

            }
        }
    }

    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", google_email);
        mMap.put("username", gooogle_name);
        mMap.put("google_id", google_id);
        mMap.put("google_image",google_image);
        mMap.put("device_token", strDeviceToken);
        mMap.put("device_type", "ANDROID");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }
    private void executeGoogleSignInApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signInGoogleRequest(mParams2()).enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignInModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, "Signin Successfully");
                    LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.IS_LOGIN, true);
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_ID, mModel.getData().getId());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.AUTH_KEY, mModel.getData().getAuthKey());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_NAME, mModel.getData().getName());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_EMAIL, mModel.getData().getEmail());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_PROFILE_PIC, mModel.getData().getProfilePic());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LATITUDE, mModel.getData().getLatitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LONGITUDE, mModel.getData().getLongitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_LOCATIONS, mModel.getData().getLongitude());
                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else{
                    showAlertDialog(mActivity,mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    //fb
    //fb Login
    private void performFbLogin() {
//        loginButton.performClick();
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            FacebookSdk.sdkInitialize(getApplicationContext());
            //FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
            LoginManager.getInstance().logOut();
            AppEventsLogger.activateApp(getApplication());
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
            mCallbackManager = CallbackManager.Factory.create();

            loginWithFacebook();
        }

    }


    private void loginWithFacebook() {

        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"));
            LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d("onSuccess: ", loginResult.getAccessToken().getToken());
                    Log.e("fook", "2");
                    getFacebookData(loginResult);
                }

                @Override
                public void onCancel() {
                    Log.e("fook", "3");
                    // Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Log.e("fook", "4");
                 //   Toast.makeText(mActivity, error.toString(), Toast.LENGTH_SHORT).show();
                    Log.e("error", "is" + error.toString());
                }

            });
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    private void getFacebookData(LoginResult loginResult) {
        showProgressDialog(mActivity);
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                dismissProgressDialog();
                try {

                    if (object.has("id")) {
                        fbId = object.getString("id");
                        Log.e("LoginActivity", "id" + fbId);

                    }
                    //check permission first userName
                    if (object.has("first_name")) {
                        fbFirstName = object.getString("first_name");
                        Log.e("LoginActivity", "first_name" + fbFirstName);

                    }
                    //check permisson last userName
                    if (object.has("last_name")) {
                        fbLastName = object.getString("last_name");
                        Log.e("LoginActivity", "last_name" + fbLastName);
                    }
                    //check permisson email
                    if (object.has("email")) {
                        fbEmail = object.getString("email");
                        Log.e("LoginActivity", "email" + fbEmail);
                    }

                    fbSocialUserserName = fbFirstName + " " + fbLastName;

                    JSONObject jsonObject = new JSONObject(object.getString("picture"));
                    if (jsonObject != null) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        Log.e("Loginactivity", "json oject get picture" + dataObject);
                        fbProfilePicture = new URL("https://graph.facebook.com/" + fbId + "/picture?width=500&height=500");
                        Log.e("LoginActivity", "json object=>" + object.toString());
                    }

                    fb_Username = fbSocialUserserName;
                    fb_email = fbEmail;
                    facebook_id = fbId;

                    if (fbProfilePicture != null) {
                        FacebookProfilePicture = String.valueOf(fbProfilePicture);

                    } else {
                        FacebookProfilePicture = "";
                    }


                    executeLoginWithFbApi();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle bundle = new Bundle();
        Log.e("LoginActivity", "bundle set");
        bundle.putString("fields", "id, first_name, last_name,email,picture,gender,location");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();

    }

    private Map<String, String> mParams3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("username", fb_Username);
        mMap.put("email", fb_email);
        mMap.put("facebook_id", fbId);
        mMap.put("device_type",  "android");
        mMap.put("device_token",strDeviceToken);
        mMap.put("facebook_image",FacebookProfilePicture);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLoginWithFbApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.facebookRequest(mParams3()).enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignInModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, "Signin Successfully");
                    LeedgoPrefrences.writeBoolean(mActivity, LeedgoPrefrences.IS_LOGIN, true);
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_ID, mModel.getData().getId());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.AUTH_KEY, mModel.getData().getAuthKey());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_NAME, mModel.getData().getName());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_EMAIL, mModel.getData().getEmail());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_PROFILE_PIC, mModel.getData().getProfilePic());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LATITUDE, mModel.getData().getLatitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.LONGITUDE, mModel.getData().getLongitude());
                    LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.USER_LOCATIONS, mModel.getData().getLongitude());
                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

}