package com.leedgo.app.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leedgo.app.R;
import com.leedgo.app.adapters.EditPaymentAdapter;
import com.leedgo.app.interfaces.PrimaryCardInterface;
import com.leedgo.app.models.CardData;
import com.leedgo.app.models.CardDetailModel;
import com.leedgo.app.models.UpdatePriLocationModel;
import com.leedgo.app.models.locations.DeleteLocationModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.LeedgoPrefrences;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPaymentActivity extends BaseActivity {

    Activity mActivity = EditPaymentActivity.this;

    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView recyclerView;
    @BindView(R.id.locationsLL)
    LinearLayout llyLocation;
    @BindView(R.id.btnSaveB)
    Button btnSave;

    EditPaymentAdapter mSearchLocationAdapter;

    LinearLayout lly;
    LinearLayoutManager mLayoutManager;
    String card_id;
    ArrayList<CardData> data;
    String mgetCardId = "";
    Boolean isPrimaryByDefault = false;

    PrimaryCardInterface mPrimaryCardInterface = new PrimaryCardInterface() {

        @Override
        public void getPrimaryCard(CardData mModel) {
            mgetCardId = mModel.getCard_id();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_payment);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, "Choose a Payment Method");
        Log.e("authKey", getAuthKey());
    }

    @Override
    public void onResume() {
        super.onResume();
        getUserLocationData();
    }


    @OnClick({R.id.backRL, R.id.locationsLL, R.id.btnSaveB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.locationsLL:
                addCard();
                break;
            case R.id.btnSaveB:
                performSaveClick();
                break;

        }
    }

    private void addCard() {
        Intent intent = new Intent(mActivity, AddCardDetails.class);
        startActivity(intent);
    }

    private void performSaveClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSave.startAnimation(myAnim);

            if (isNetworkAvailable(mActivity)) {
                try {
                    if (mgetCardId.length() > 0) {
                        executePrimaryCardRequest();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }


    private void getUserLocationData() {
        if (isNetworkAvailable(mActivity))
            executeCardRequest();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCardRequest() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getClientDetailsCard(getAuthKey(), mParam()).enqueue(new Callback<CardDetailModel>() {
            @Override
            public void onResponse(Call<CardDetailModel> call, Response<CardDetailModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                CardDetailModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    data = (ArrayList<CardData>) response.body().getData();

                    if (data != null && data.size() > 0) {
                        LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.CARD_ADD_STATUS, "true");
                    } else {
                        LeedgoPrefrences.writeString(mActivity, LeedgoPrefrences.CARD_ADD_STATUS, "false");
                    }

                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).getIsPrimary().equals("1")) {
                            Collections.swap(data, i, 0);
                        } else {

                        }
                    }
                    if (!isPrimaryByDefault) {
                        if (data.size() == 1) {
                            mgetCardId = data.get(0).getCard_id();
                            executeDefaultPrimaryCardRequest();
                            isPrimaryByDefault = true;
                        }
                    }
                    setAdapter(data);

                } else {
                    try {
                        data.clear();
                        isPrimaryByDefault = false;
                        mSearchLocationAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    showAlertDialog(mActivity, "No cards added");
                }
            }

            @Override
            public void onFailure(Call<CardDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    public void setAdapter(ArrayList<CardData> data) {
        mLayoutManager = new LinearLayoutManager(mActivity);
        mSearchLocationAdapter = new EditPaymentAdapter(mActivity, data, mPrimaryCardInterface, new EditPaymentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int positon, CardData item, View view) {
                switch (view.getId()) {
                    case R.id.imgItemDeleteIV:
                        View layout = mLayoutManager.findViewByPosition(positon);
                        DelCard(item, positon);
                        layout.findViewById(R.id.imgItemDeleteIV).setClickable(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                layout.findViewById(R.id.imgItemDeleteIV).setClickable(true);
                            }
                        }, 1500);

                        break;
                    case R.id.imgItemEditIV:
                        View layout2 = mLayoutManager.findViewByPosition(positon);
                        TextView txTitle = layout2.findViewById(R.id.itemCityNameTV);
                        String title = txTitle.getText().toString().trim();
                        EditCard(item, positon, title);
                        layout2.findViewById(R.id.imgItemEditIV).setClickable(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                layout2.findViewById(R.id.imgItemEditIV).setClickable(true);
                            }
                        }, 1500);

                        break;
                }
            }
        });

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mSearchLocationAdapter);
    }

    private void performClick(CardData item) {
        Intent mIntent = new Intent(mActivity, AddCardDetails.class);
        mIntent.putExtra("card_id", item.getCard_id());
        mIntent.putExtra("update_cardornot", "update");
        mIntent.putExtra("card_type", item.getBrand().toLowerCase());
        mIntent.putExtra("cardyear", item.getExp_year());
        mIntent.putExtra("cardmonth", item.getExp_month());
        mIntent.putExtra("cardholdername", item.getCard_holder_name());
        mIntent.putExtra("cardnumber", item.getCard_last4());
        startActivity(mIntent);
    }


    private void DelCard(CardData item, int positon) {
        String message = "delete card";
        String type = "delete";
        card_id = String.valueOf(item.getId());
        funDelalertDialog(message, type, positon, item);

    }

    private void EditCard(CardData item, int positon, String title) {
        String message = "edit card";
        String type = "edit";
        card_id = String.valueOf(item.getId());
        funDelalertDialog(message, type, positon, item);

    }

    void funDelalertDialog(String message, String type, int positon, CardData item) {


        new IOSDialog.Builder(mActivity)
                .setMessage("Are you sure you want to " + message + " ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (type.equals("delete")) {
                            executeDeleteapi();

                        } else {
                            performClick(item);

                        }
                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private Map<String, String> mParamDel() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", card_id);
        return mMap;
    }

    private void executeDeleteapi() {
        //showProgressDialog(mActivity);
        Log.e("", "1" + mParam());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeDeleteCard(getAuthKey(), mParamDel()).enqueue(new Callback<DeleteLocationModel>() {
            @Override
            public void onResponse(Call<DeleteLocationModel> call, Response<DeleteLocationModel> response) {
                dismissProgressDialog();
                Log.e("Dataq", "**RESPONSE**" + response.body().toString());
                card_id = "";
                DeleteLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    getUserLocationData();

                } else {
                    showAlertDialog(mActivity, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DeleteLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }

    private Map<String, String> mUpdateParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("card_id", "" + mgetCardId);
        mMap.put("client_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executePrimaryCardRequest() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addPrimaryCard(getAuthKey(), mUpdateParam()).enqueue(new Callback<UpdatePriLocationModel>() {
            @Override
            public void onResponse(Call<UpdatePriLocationModel> call, Response<UpdatePriLocationModel> response) {
                dismissProgressDialog();
                UpdatePriLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    getUserLocationData();
                } else {
                    if (mgetCardId != null && mgetCardId.length() > 1) {
                        showToast(mActivity, mModel.getMessage());
                    } else {
                        showToast(mActivity, "Please add card as primary ");
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdatePriLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }

    private void executeDefaultPrimaryCardRequest() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.addPrimaryCard(getAuthKey(), mUpdateParam()).enqueue(new Callback<UpdatePriLocationModel>() {
            @Override
            public void onResponse(Call<UpdatePriLocationModel> call, Response<UpdatePriLocationModel> response) {
                dismissProgressDialog();
                UpdatePriLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                } else {
                    if (mgetCardId != null && mgetCardId.length() > 1) {
                        showToast(mActivity, mModel.getMessage());
                    } else {
                        showToast(mActivity, "Please add card as primary ");
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdatePriLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }
}