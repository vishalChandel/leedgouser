package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.leedgo.app.R;

public class AddLocationConfirm extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location_confirm);
    }
}