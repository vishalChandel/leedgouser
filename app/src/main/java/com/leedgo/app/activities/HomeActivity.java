package com.leedgo.app.activities;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.fragments.HomeFragment;
import com.leedgo.app.fragments.InboxFragment;
import com.leedgo.app.fragments.LocationsFragment;
import com.leedgo.app.fragments.RequestHistoryFragment;
import com.leedgo.app.fragments.RequestedTaskFragment;
import com.leedgo.app.models.CheckModel;
import com.leedgo.app.models.OrderPaymentStatusModel;
import com.leedgo.app.models.PaymentModel;
import com.leedgo.app.models.StripeAccountModel;
import com.leedgo.app.models.profile.ProfileModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.LeedgoPrefrences;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeActivity.this.getClass().getSimpleName();


    /**
     * Current Activity Instance
     */
    Activity mActivity = HomeActivity.this;
    /*
     * Widgets
     * */
    @BindView(R.id.menuRL)
    RelativeLayout menuRL;
    @BindView(R.id.notificationRL)
    RelativeLayout notificationRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.containerFL)
    FrameLayout containerFL;
    @BindView(R.id.tab1LL)
    LinearLayout tab1LL;
    @BindView(R.id.tab1IV)
    ImageView tab1IV;
    @BindView(R.id.tab2LL)
    LinearLayout tab2LL;
    @BindView(R.id.tab2IV)
    ImageView tab2IV;
    @BindView(R.id.tab3LL)
    LinearLayout tab3LL;
    @BindView(R.id.tab3IV)
    ImageView tab3IV;
    @BindView(R.id.tab4LL)
    LinearLayout tab4LL;
    @BindView(R.id.tab4IV)
    ImageView tab4IV;
    @BindView(R.id.imgNotificationBadgeIV)
    ImageView imgNotificationBadgeIV;
    @BindView(R.id.tabCenterLL)
    RelativeLayout tabCenterLL;
    String formattedDate;

    /*
     * Initialize...
     * */
    private long mHomeClick = 0;
    private long mReqHistoryClick = 0;
    private long mReqEdTaskClick = 0;
    private long mInboxClick = 0;
    private long mLocationClick = 0;
    Dialog alertDialog;
    String providerId = "";
    String clickType = "";
    ProfileModel mProfileModel;

    private long mTabClick1 = 0, mTabClick2 = 0, mTabClick3 = 0, mTabClick4 = 0, mTabClick5 = 0;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        performTab1Click();
        checkStatusApi();
    }

    public void checkStatusApi() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                executePaymentapi();
            }
        }, 2000);

    }


    /*
     * Execute api
     * */
    private Map<String, String> mParamnewUpdate() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executePaymentapi() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executePaymentStatus(mParamnewUpdate()).enqueue(new Callback<PaymentModel>() {
            @Override
            public void onResponse(Call<PaymentModel> call, Response<PaymentModel> response) {
                dismissProgressDialog();
                PaymentModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    providerId = mModel.getData().getAssign_user();
                    showAlertPDialog(mActivity, mModel.getData().getCategory_image(), mModel.getData().getCategory_name(),
                            mModel.getData().getSubcategory_name(), mModel.getData().getTask_complete_date(), mModel.getData().getUnique_task_id(),
                            mModel.getData().getAssigned_amount(), mModel.getData().getId());
                } else {
                }
            }

            @Override
            public void onFailure(Call<PaymentModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     *
     * Error Alert Dialog
     * */
    @SuppressLint("SetTextI18n")
    public void showAlertPDialog(Activity mActivity, String category_image, String category_name, String subcategory_name, String task_complete_date, String unique_task_id, String amount, String providerId) {
        alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert_payment);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txServiceName = alertDialog.findViewById(R.id.txServiceName);
        TextView txServiceType = alertDialog.findViewById(R.id.txServiceNameType);
        TextView txPaymentMessage = alertDialog.findViewById(R.id.txPaymentMessage);
        TextView txLater = alertDialog.findViewById(R.id.txLater);
        Button btnPay = alertDialog.findViewById(R.id.btnPay);
        ImageView imServiceImage = alertDialog.findViewById(R.id.imServiceImage);
        TextView txdialogHeading = alertDialog.findViewById(R.id.txdialogHeading);
        Glide.with(this).load(category_image).placeholder(R.drawable.ic_pp).into(imServiceImage);
        txServiceName.setText(category_name);
        long coment_time = Long.parseLong(task_complete_date);
        timeChange(coment_time);
        txServiceType.setText(subcategory_name);
        txPaymentMessage.setText(getString(R.string.paymentpending1) + " " + unique_task_id + " " + getString(R.string.paymentpending2));
        // txPaymentMessage.setText(strMessage);
        txLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To check provider Account added or not
                executeCheckStripeStatusApi(subcategory_name, unique_task_id, formattedDate, category_image, amount, providerId);
//                startActivity(new Intent(HomeActivity.this, PaymentActivity1.class)

                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    void timeChange(long coment_time) {
        Date date = new java.util.Date(coment_time * 1000L);

        // the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());
        formattedDate = sdf.format(date);
    }

    private Map<String, String> mParam2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", providerId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCheckStripeStatusApi(String subcategory_name, String unique_task_id, String formattedDate, String category_image, String amount, String providerId) {
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.providerStripeAccount(mParam2()).enqueue(new Callback<StripeAccountModel>() {
            @Override
            public void onResponse(Call<StripeAccountModel> call, Response<StripeAccountModel> response) {
                CheckModel model;
                StripeAccountModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    startActivity(new Intent(HomeActivity.this, PaymentActivity2.class)
                            .putExtra("serviceName", subcategory_name)
                            .putExtra("serviceId", unique_task_id)
                            .putExtra("serviceTime", formattedDate)
                            .putExtra("serviceImage", category_image)
                            .putExtra("servicePrice", amount)

                    );

                } else {
                    showAlertDialog(mActivity, getString(R.string.you_cant_proceed_to_checkout));
                }
            }

            @Override
            public void onFailure(Call<StripeAccountModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.menuRL, R.id.notificationRL, R.id.tab1LL, R.id.tab2LL, R.id.tab3LL, R.id.tab4LL, R.id.tabCenterLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.menuRL:
                performMenuClick();
                break;
            case R.id.notificationRL:
                performNotificationClick();
                break;
            case R.id.tab1LL:
                checkStatusApi();

                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mTabClick1 < 1500) {
                    return;
                }
                mTabClick1 = SystemClock.elapsedRealtime();

                performTab1Click();
                break;
            case R.id.tab2LL:
                checkStatusApi();

                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mTabClick2 < 1500) {
                    return;
                }
                mTabClick2 = SystemClock.elapsedRealtime();

                performTab2Click();
                break;
            case R.id.tab3LL:
                checkStatusApi();

                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mTabClick3 < 1500) {
                    return;
                }
                mTabClick3 = SystemClock.elapsedRealtime();

                performTab3Click();
                break;
            case R.id.tab4LL:
                checkStatusApi();

                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mTabClick4 < 1500) {
                    return;
                }
                mTabClick4 = SystemClock.elapsedRealtime();

                performTab4Click();
                break;
            case R.id.tabCenterLL:
                checkStatusApi();

                // Preventing multiple clicks, using threshold of 1 second
                if (SystemClock.elapsedRealtime() - mTabClick5 < 1500) {
                    return;
                }
                mTabClick5 = SystemClock.elapsedRealtime();

                performCenterTabClick();
                break;
        }
    }

    private void dismisss() {
        try {
            alertDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void performMenuClick() {
        dismisss();
        startActivity(new Intent(mActivity, LeftMenuActivity.class));
    }

    private void performNotificationClick() {
        dismisss();
        startActivity(new Intent(mActivity, NotificationsActivity.class));
    }

    private void performTab1Click() {

        tab1IV.setImageResource(R.drawable.ic_home_select);
        tab2IV.setImageResource(R.drawable.ic_order_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchFragment(mActivity, new HomeFragment(), false, null);
        setHomeToolbar();
    }

    private void performTab2Click() {
        dismisss();
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_order_select);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchFragment(mActivity, new RequestHistoryFragment(), false, null);
        setRequestHistoryToolbar();
    }

    private void performTab3Click() {
        dismisss();
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_order_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_select);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchFragment(mActivity, new InboxFragment(), false, null);
        setInboxToolbar();
    }

    private void performTab4Click() {
        dismisss();
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_order_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_select);
        switchFragment(mActivity, new LocationsFragment(), false, null);
        setLocationToolbar();
    }

    private void performCenterTabClick() {
        dismisss();
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_order_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchFragment(mActivity, new RequestedTaskFragment(), false, null);
        setRequestedTaskToolbar();
    }

    /********
     *Replace Fragment In Activity
     **********/
    public void switchFragment(Activity activity, Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerFL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }

    private void setHomeToolbar() {
        // Preventing multiple clicks, using threshold of 1.5 second
        if (SystemClock.elapsedRealtime() - mHomeClick < 1500) {
            return;
        }
        mHomeClick = SystemClock.elapsedRealtime();
        menuRL.setVisibility(View.VISIBLE);
        txtHeaderTV.setVisibility(View.VISIBLE);
        txtHeaderTV.setText(getString(R.string.leedgo));
        notificationRL.setVisibility(View.VISIBLE);
    }

    private void setRequestHistoryToolbar() {
        // Preventing multiple clicks, using threshold of 1.5 second
        if (SystemClock.elapsedRealtime() - mReqHistoryClick < 1500) {
            return;
        }
        mReqHistoryClick = SystemClock.elapsedRealtime();
        menuRL.setVisibility(View.GONE);
        txtHeaderTV.setVisibility(View.VISIBLE);
        txtHeaderTV.setText(getString(R.string.request_history));
        notificationRL.setVisibility(View.GONE);
    }

    private void setRequestedTaskToolbar() {
        // Preventing multiple clicks, using threshold of 1.5 second
        if (SystemClock.elapsedRealtime() - mReqEdTaskClick < 1500) {
            return;
        }
        mReqEdTaskClick = SystemClock.elapsedRealtime();
        menuRL.setVisibility(View.GONE);
        txtHeaderTV.setVisibility(View.VISIBLE);
        txtHeaderTV.setText(getString(R.string.requested_task));
        notificationRL.setVisibility(View.GONE);
    }

    private void setInboxToolbar() {
        // Preventing multiple clicks, using threshold of 1.5 second
        if (SystemClock.elapsedRealtime() - mInboxClick < 1500) {
            return;
        }
        mInboxClick = SystemClock.elapsedRealtime();
        menuRL.setVisibility(View.GONE);
        txtHeaderTV.setVisibility(View.VISIBLE);
        txtHeaderTV.setText(getString(R.string.inbox));
        notificationRL.setVisibility(View.GONE);
    }

    private void setLocationToolbar() {
        // Preventing multiple clicks, using threshold of 1.5 second
        if (SystemClock.elapsedRealtime() - mLocationClick < 1500) {
            return;
        }
        mLocationClick = SystemClock.elapsedRealtime();
        menuRL.setVisibility(View.GONE);
        txtHeaderTV.setVisibility(View.VISIBLE);
        txtHeaderTV.setText(getString(R.string.location));
        notificationRL.setVisibility(View.GONE);
    }


    @Override
    protected void onPause() {
        super.onPause();
        dismisss();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("id", getUserId());
        Log.e(TAG, "**PARAM**" + mMap.toString() + getAuthKey());
        return mMap;
    }

    private void executeProfileDataApi() {
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUserDataRequest(getAuthKey(), mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                try {
                    dismissProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("getAuthKey()", getAuthKey());
                mProfileModel = response.body();
                if (mProfileModel.getStatus() == 1) {
                    if (mProfileModel.getUnread_notification().equals("0")) {
                        imgNotificationBadgeIV.setVisibility(View.GONE);
                    } else {
                        imgNotificationBadgeIV.setVisibility(View.VISIBLE);
                    }
                } else {
                    showToast(mActivity, mProfileModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        executeProfileDataApi();
    }
}