package com.leedgo.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.leedgo.app.R;
import com.leedgo.app.models.RoomCreateModel;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;
import com.leedgo.app.utils.LeedgoPrefrences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProDetailsActivity extends BaseActivity {

    Activity mActivity = ProDetailsActivity.this;

    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.imgProviderIV)
    CircleImageView imgProviderIV;
    @BindView(R.id.txProviderName)
    TextView txProviderName;
    @BindView(R.id.txtPhoneNumberTV)
    TextView txtPhoneNumberTV;
    @BindView(R.id.txProviderProfessional)
    TextView txProviderProfessional;
    @BindView(R.id.txtEmailTV)
    TextView txtEmailTV;
    @BindView(R.id.imgChatIV)
    ImageView imgChatIV;
    @BindView(R.id.txtRatingTV)
    TextView txtRatingTV;

    String proId, proName, proImage, proEmail, proPhone, proTime, proRoom, proRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pro_details);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            proId = getIntent().getStringExtra("proId");
            proName = getIntent().getStringExtra("proName");
            proImage = getIntent().getStringExtra("proImage");
            proEmail = getIntent().getStringExtra("proEmail");
            proPhone = getIntent().getStringExtra("proPhone");
            proTime = getIntent().getStringExtra("proTime");
            proRating = getIntent().getStringExtra("proRating");
            setDataViews();
        }
        executeMessage();
    }

    @OnClick({R.id.backRL, R.id.imgChatIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.imgChatIV:
               openChatActivity();
                break;
        }
    }

    private void openChatActivity() {
        Intent intent = new Intent(mActivity, ChatActivity.class);
        intent.putExtra("room_id", proRoom);
        intent.putExtra("name", proName);
        mActivity.startActivity(intent);
    }


    private void setDataViews() {
        txtEmailTV.setText(proEmail);
        txtPhoneNumberTV.setText(proPhone);
        txProviderName.setText(proName);
        txProviderProfessional.setText("Professional Since " + proTime);
        txtRatingTV.setText(proRating);
        Glide.with(this)
                .load(proImage)
                .into(imgProviderIV);
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", getUserId());
        mMap.put("provider_id", proId);
        Log.e("CreateRoomParrams",getUserId()+"::"+proId);
        return mMap;
    }

    private void executeMessage() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createRoom(mParam()).enqueue(new Callback<RoomCreateModel>() {
            @Override
            public void onResponse(Call<RoomCreateModel> call, Response<RoomCreateModel> response) {

                RoomCreateModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    proRoom=mModel.getRoom_id();
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RoomCreateModel> call, Throwable t) {

                Log.e(String.valueOf(mActivity), "**ERROR**" + t.getMessage());
            }
        });
    }
}