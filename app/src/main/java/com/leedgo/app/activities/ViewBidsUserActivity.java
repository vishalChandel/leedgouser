package com.leedgo.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.leedgo.app.R;
import com.leedgo.app.adapters.ViewBidsAdapter;
import com.leedgo.app.adapters.ViewBidsAdapter2;
import com.leedgo.app.interfaces.BidsInteface;
import com.leedgo.app.interfaces.PrimaryLocationInterface;
import com.leedgo.app.models.BidListModel;
import com.leedgo.app.models.CheckModel;
import com.leedgo.app.models.GetClientLocationData;
import com.leedgo.app.retrofit.ApiClient;
import com.leedgo.app.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewBidsUserActivity extends BaseActivity {
    Activity mActivity=ViewBidsUserActivity.this;

    @BindView(R.id.llyBack)
    LinearLayout llyBack;

    @BindView(R.id.txServiceName)
    TextView txServiceName;
    @BindView(R.id.txServiceNameType)
    TextView txServiceNameType;
    @BindView(R.id.imServiceImage)
    ImageView imServiceImage;
    @BindView(R.id.txWorkLocation)
    TextView txWorkLocation;
    @BindView(R.id.viewBidsRecycler)
    RecyclerView viewBidsRecycler;
    @BindView(R.id.viewSelectBidsRecycler)
    RecyclerView viewBidsSelectRecycler;
    @BindView(R.id.txClearAll)
    TextView txClearAll;
    @BindView(R.id.txSelectBid)
    TextView txSelectBid;

    String serviceName,categoryImage,subCategoryName,workLocation,taskId;
    String bidId;

    LinearLayoutManager mLayoutManager,mLayoutManager2;
    boolean check=true;
    String bidSuccessfull="";
    SharedPreferences prefv;
    String bidUser="";

    BidsInteface mInterface = new BidsInteface() {
        @Override
        public void getSelectedPosition(BidListModel.Data mModel) {
          bidId=mModel.getBid_id();
          bidUser=mModel.getProvider_name();
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_bids_user);
        ButterKnife.bind(this);
        prefv=getSharedPreferences("pref", Context.MODE_PRIVATE);
        setUpViews();

    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.txClearAll, R.id.txSelectBid,R.id.llyBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txClearAll:
                executeClearApi();
                break;
            case R.id.txSelectBid:
                performClick();
                break;
            case R.id.llyBack:
               // finish();
                backClickF();
                break;
        }
    }

    private void backClickF() {

        prefv.edit().putString(taskId,bidSuccessfull).commit();
       finish();
    }

    private void performClick() {

        if (check)
        {
            txSelectBid.setText("Accept Bid");
            viewBidsRecycler.setVisibility(View.GONE);
            viewBidsSelectRecycler.setVisibility(View.VISIBLE);

            check=false;
        }
        else {
            if (bidId!=null) {
                executeSubmiBidApi();
            }
        }
    }


    private void setUpViews() {
        if (getIntent()!=null)
        {
            serviceName=getIntent().getStringExtra("serviceName");
            categoryImage=getIntent().getStringExtra("categoryImage");
            subCategoryName=getIntent().getStringExtra("subCategoryName");
            workLocation=getIntent().getStringExtra("workLocation");
            taskId=getIntent().getStringExtra("taskId");
            txServiceName.setText(serviceName);
            txServiceNameType.setText(subCategoryName);
            txWorkLocation.setText(workLocation);
            if (!categoryImage.equals("")) {
                Glide.with(mActivity)
                        .load(categoryImage)
                        .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                        .into(imServiceImage);
            } else {
                SharedPreferences preference=mActivity.getSharedPreferences("LeedGo", Context.MODE_PRIVATE);

                Set<String> set=preference.getStringSet("myKey",null);
                List<String> sample=new ArrayList<String>(set);
                Glide.with(mActivity)
                        .load(sample.get(new Random().nextInt(sample.size())))
                        .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                        .into(imServiceImage);

            }
            //Toast.makeText(mActivity, ""+taskId, Toast.LENGTH_SHORT).show();
            executeApi();
        }


    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamnewUpdate() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        mMap.put("task_id", taskId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.showBidList( mParamnewUpdate()).enqueue(new Callback<BidListModel>() {
            @Override
            public void onResponse(Call<BidListModel> call, Response<BidListModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                BidListModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    //finish();
                    setAdapter(mModel.getData());
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<BidListModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter(List<BidListModel.Data> data) {
        try {

            mLayoutManager = new LinearLayoutManager(mActivity);
            ViewBidsAdapter viewBids=new ViewBidsAdapter(mActivity, (ArrayList<BidListModel.Data>) data,mInterface);
            viewBidsRecycler.setLayoutManager(mLayoutManager);
            viewBidsRecycler.setAdapter(viewBids);

            mLayoutManager2 = new LinearLayoutManager(mActivity);
            ViewBidsAdapter2 viewBids2=new ViewBidsAdapter2(mActivity, (ArrayList<BidListModel.Data>) data,mInterface);
            viewBidsSelectRecycler.setLayoutManager(mLayoutManager2);
            viewBidsSelectRecycler.setAdapter(viewBids2);


        }
        catch (Exception e)
        {
         e.printStackTrace();
        }
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("task_id", taskId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeClearApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.clearBidList( mParams()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                CheckModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
    /*
     * Execute api
     * */
    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getUserId());
        mMap.put("bid_id", bidId);
        mMap.put("task_id", taskId);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSubmiBidApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.acceptBid( mParams2()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                CheckModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    bidSuccessfull="bidSuccess";
                    prefv.edit().putString(taskId,bidSuccessfull).commit();
                    showAlertDialog2(mActivity,getString(R.string.bidsubmit)+bidUser+getString(R.string.bidsubmit2));
                    //finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    public void showAlertDialog2(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                       finish();
                    }
                }, 500);

            }
        });
        alertDialog.show();
    }

}