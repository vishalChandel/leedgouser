package com.leedgo.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

import com.leedgo.app.R;
import com.leedgo.app.utils.Constants;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.InstanceIdResult;

public class SplashActivity extends BaseActivity {

    /**
     * Splash Close Time
     */
    public final int SPLASH_TIME_OUT = 1500;

    /**
     * Getting the Current Class Name
     */
    String TAG = SplashActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SplashActivity.this;

    boolean isNotification = false;
    String notification_type = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        hideStatusBar();

        setNotifications();
    }

    private void setNotifications() {
        if (getIntent().getExtras() != null) {

            notification_type = String.valueOf(getIntent().getExtras().get("type"));

            if (notification_type != null) {
                isNotification = true;

                if (notification_type.equalsIgnoreCase("new_bid_reminder") ||
                        notification_type.equalsIgnoreCase("request_submitted") ||
                        notification_type.equalsIgnoreCase("received_bid") ||
                        notification_type.equalsIgnoreCase("bid_approved") ||
                        notification_type.equalsIgnoreCase("task_date_scheduled") ||
                        notification_type.equalsIgnoreCase("task_complete") ||
                        notification_type.equalsIgnoreCase("chat") ||
                        notification_type.equalsIgnoreCase("unassigned_task") ||
                        notification_type.equalsIgnoreCase("pending_bid_status") ||
                        notification_type.equalsIgnoreCase("request_start_day") ||
                        notification_type.equalsIgnoreCase("order")) {
                    Intent mIntent = new Intent(this, NotificationsActivity.class);
                    mIntent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
                    startActivity(mIntent);
                    finish();
                } else {
                    setUpSplash();
                }
            }

//            for (String key : getIntent().getExtras().keySet()) {
//                Object value = getIntent().getExtras().get(key);
//                Log.d(TAG, "Key: " + key + " Value: " + value);
//
//                if (key.equalsIgnoreCase("type")) {
//                    Log.d(TAG, "****value****" + String.valueOf(value));
//                }
//                if (value != null && value.equals("normal")) {
//                    isNotification = true;
//
//                    Toast.makeText(mActivity, String.valueOf(value), Toast.LENGTH_SHORT).show();
//                    Intent mIntent = new Intent(this, NotificationsActivity.class);
//                    mIntent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
//                    startActivity(mIntent);
//                    finish();
//                }
//            }

            if (!isNotification) {
                setUpSplash();
            }
        } else {
            setUpSplash();
        }
    }

    private void setUpSplash() {
        printHashKey(mActivity);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (IsLogin()) {
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    startActivity(mIntent);
                    finish();
                } else {
                    if (IsIntroScreens()) {
                        startActivity(new Intent(mActivity, SignInActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(mActivity, IntroActivity.class));
                        finish();
                    }
                }

            }
        }, SPLASH_TIME_OUT);
    }

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("HashKey",  hashKey);
            }
        } catch (Exception e) {
            Log.e("TAG", "printHashKey()", e);
        }
    }
}