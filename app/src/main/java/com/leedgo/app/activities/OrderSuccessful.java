package com.leedgo.app.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.leedgo.app.R;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderSuccessful extends BaseActivity {

    @BindView(R.id.btnContinueB)
    Button btContinue;
    @BindView(R.id.order_text)
    TextView txOrder;

    String serviceType="";
    String serviceDate="";


    Activity mActivity=OrderSuccessful.this;



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_successful);
        ButterKnife.bind(this);
        if (getIntent()!=null)
        {
            serviceType=getIntent().getStringExtra("service_name");
            serviceDate=getIntent().getStringExtra("service_date");
        }

        setData();

    }



    @OnClick({R.id.btnContinueB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinueB:
                Goto();
                break;
            default:
                break;
        }
    }


    private void setData() {

        Date date1 = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate= formatter.format(date1);

        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
        String var = dateFormat.format(date);

        txOrder.setText("You have requested for "+serviceType +" service on "+serviceDate+" "+var+". "+getString(R.string.order_successful));
       // txOrder.setText("You have requested for "+serviceType +" service on "+serviceDate+" "+getString(R.string.order_successful));


    }

    private void Goto() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btContinue.startAnimation(myAnim);


        Intent intent=new Intent(mActivity,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Goto();
    }
}